# BookAround - Share your books!

BookAround is a book sharing application for all bibliophiles. With BookAround you can:

* Share your book copies with nearby book lovers!
* Find who else is sharing books in your zone!
* Arrange for lending, and get directly in touch with other people!

![Gallery](https://bitbucket.org/not-our-users/book-around/raw/6bdc2262f624c93a5741a1e3b97cc8e5374768b2/sample-pictures/ba_gallery.PNG =250x)
![Book](https://bitbucket.org/not-our-users/book-around/raw/6bdc2262f624c93a5741a1e3b97cc8e5374768b2/sample-pictures/ba_book.PNG =250x)
![Copy](https://bitbucket.org/not-our-users/book-around/raw/6bdc2262f624c93a5741a1e3b97cc8e5374768b2/sample-pictures/ba_copy.PNG =250x)
![Chat](https://bitbucket.org/not-our-users/book-around/raw/6bdc2262f624c93a5741a1e3b97cc8e5374768b2/sample-pictures/ba_chat.PNG =250x)
![Lending](https://bitbucket.org/not-our-users/book-around/raw/6bdc2262f624c93a5741a1e3b97cc8e5374768b2/sample-pictures/ba_lending.PNG =250x)

## Disclaimer

This app was developed by our team of four people during an introductory course to Android programming at the Polytechnic University of Turin, A.Y. 2017/18.

You can freely browse, extend, modify or use however you want our work, but keep in mind that this app will no longer be maintained. As such, we cannot guarantee the database's uptime, nor the validity of API keys. For example, the Facebook and Twitter keys have been invalidated and their login option is no longer available. Also it's very likely that the code contains a certain degree of bugs which we haven't squashed yet.

Among the different techonologies adopted, we used:

* Firebase as our backend
* Google Books API for book related searches
* RxJava (and the RxKotlin extensions) to track live mutable data

## Authors

* Giulio Bonetto
      * Bitbucket: https://bitbucket.org/giuliobonetto/
* Alessio Giuseppe Calì
      * LinkedIn: https://www.linkedin.com/in/alessio-giuseppe-cali/
* Alessandro Mascherin
      * LinkedIn https://www.linkedin.com/in/amascherin/
* Ema Srdoc
      * LinkedIn: https://www.linkedin.com/in/ema-srdoc-93b44a87/