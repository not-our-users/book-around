package it.polito.students.mad.group5.bookaround.utils

import android.content.Context
import com.google.firebase.database.FirebaseDatabase
import io.reactivex.rxkotlin.subscribeBy
import it.polito.students.mad.group5.bookaround.models.Copy
import it.polito.students.mad.group5.bookaround.models.TransactionStatus
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Created by Alessio on 26/05/2018.
 */
@RunWith(MockitoJUnitRunner::class)
class FirebaseUtilsKtTest {

    companion object {
        @BeforeClass @JvmStatic fun setUp() {
            FirebaseDatabase.getInstance().setPersistenceEnabled(false)
        }
        @AfterClass @JvmStatic fun tearDown() {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        }
    }

    @Mock lateinit var mContext : Context

    val TEST_COPY_ID = "TEST_COPY"
    val TEST_TRANSACTION_ID = "TEST_TRANSACTION"
    val TEST_REVIEW_ID = "TEST_REVIEW1"
    val TEST_SCORE_USER = "TEST_USER2"

    /* TEST COPY VALUES */
    val TEST_COPY_COND = Copy.Condition.MINT
    val TEST_COPY_DETAILS = "Very nice book!"
    val TEST_COPY_API_ID = "9CJWTbd-RYoC"
    val TEST_COPY_ISBN = "9781781101582"
    val TEST_COPY_LAT = 0.0
    val TEST_COPY_LON = 0.0
    val TEST_COPY_OWNER = "TEST_USER1"
    val TEST_COPY_STATUS = Copy.Status.NOT_BORROWED

    /* TEST TRANSACTION VALUES */
    val TEST_TRANS_BFLAG = true
    val TEST_TRANS_BORROWER = "TEST_USER2"
    val TEST_TRANS_LENDER= "TEST_USER1"
    val TEST_TRANS_COPY = "TEST_COPY"
    val TEST_TRANS_DATE_ISSUED: Long = 1527262127524
    val TEST_TRANS_LFLAG = false
    val TEST_TRANS_STATUS = TransactionStatus.OPEN

    /* TEST REVIEW VALUES */
    val TEST_REVIEW_AUTHOR = "TEST_USER1"
    val TEST_REVIEW_SUBJECT = "TEST_USER2"
    val TEST_REVIEW_VOTE = 5
    val TEST_REVIEW_TRANSACTION = "TEST_TRANSACTION"
    val TEST_REVIEW_TEXT = "Good guy, real good"

    /* TEST SCORE */
    val TEST_SCORE = 2.5f

    /* TEST USER TRANSACTIONS */
    val TEST_USER = "TEST_USER1"
    val TEST_LENDER_TRNSACTION = "TEST_TRANSACTION"
    val TEST_BORROWER_TRANSACTION = "TEST_TRANSACTION2"

    /* TEST USER REVIEWS */
    val TEST_REVIEW_COUNT = 1
    val TEST_EXPECTED_REVIEW_ID = "TEST_REVIEW2"

    @Test
    fun testDownloadCopy() {
        val latch = CountDownLatch(1)
        val disposable = downloadCopy(TEST_COPY_ID).subscribeBy(
                onNext = {
                    it.apply {
                        assertEquals(TEST_COPY_ID, copyId)
                        assertEquals(TEST_COPY_COND, condition)
                        assertEquals(TEST_COPY_DETAILS, details)
                        assertEquals(TEST_COPY_API_ID, googleApiId)
                        assertEquals(TEST_COPY_ISBN, isbn)
                        assertEquals(TEST_COPY_LAT, latitude, 0.001)
                        assertEquals(TEST_COPY_LON, longitude, 0.001)
                        assertEquals(TEST_COPY_OWNER, ownerId)
                        assertEquals(TEST_COPY_STATUS, status)
                    }
                    latch.countDown()
                }
        )

        latch.await(10, TimeUnit.SECONDS)
        disposable.dispose()
    }

    @Test
    fun testDownloadTransaction() {
        val latch = CountDownLatch(1)
        val disposable = downloadTransaction(TEST_TRANSACTION_ID).subscribeBy(
                onNext = {
                    it.apply{
                        assertEquals(TEST_TRANSACTION_ID, transactionId)

                        assertNotNull(borrower)
                        assertEquals(TEST_TRANS_BORROWER, borrower?.userId)
                        assertNotNull(lender)
                        assertEquals(TEST_TRANS_LENDER, lender?.userId)
                        assertNotNull(copy)
                        assertEquals(TEST_TRANS_COPY, copy?.copyId)

                        assertEquals(TEST_TRANS_LENDER, lenderId)
                        assertEquals(TEST_TRANS_BORROWER, borrowerId)
                        assertEquals(TEST_TRANS_LFLAG, lenderApprovalFlag)
                        assertEquals(TEST_TRANS_BFLAG, borrowerApprovalFlag)
                        assertEquals(TEST_TRANS_COPY, copyId)
                        assertEquals(TEST_TRANS_DATE_ISSUED, dateIssued)
                        assertEquals(TEST_TRANS_STATUS, status)
                    }
                    latch.countDown()
                }
        )

        latch.await(10, TimeUnit.SECONDS)
        disposable.dispose()
    }

    @Test
    fun testDownloadReview() {
        val latch = CountDownLatch(1)
        val disposable = downloadReview(TEST_REVIEW_SUBJECT, TEST_REVIEW_ID).subscribeBy(
                onNext = {
                    it.apply {
                        assertEquals(TEST_REVIEW_ID, reviewId)

                        assertNotNull(subject)
                        assertNotNull(authorId)
                        assertNotNull(transaction)

                        assertEquals(TEST_REVIEW_SUBJECT, subject?.userId)
                        assertEquals(TEST_REVIEW_AUTHOR, author?.userId)
                        assertEquals(TEST_REVIEW_TRANSACTION, transaction?.transactionId)

                        assertEquals(TEST_REVIEW_TEXT, text)
                        assertEquals(TEST_REVIEW_VOTE, vote)
                    }
                    latch.countDown()
                }
        )

        latch.await(10, TimeUnit.SECONDS)
        disposable.dispose()
    }

    @Test
    fun testDownloadUserScore() {
        val latch = CountDownLatch(1)
        val disposable = downloadUserScore(TEST_SCORE_USER).subscribeBy(
                onNext = {
                    assertEquals(TEST_SCORE, it)
                    latch.countDown()
                }
        )

        latch.await(10, TimeUnit.SECONDS)
        disposable.dispose()
    }

    @Test
    fun testDownloadUserTransactions() {
        val latch = CountDownLatch(2)
        val lenderDisposable = downloadUserTransactions(TEST_USER, true).subscribeBy (
            onNext = {
                assertEquals(1, it.size)
                assertEquals(TEST_LENDER_TRNSACTION, it[0].transactionId)
                latch.countDown()
            }
        )
        val borrowerDisposable = downloadUserTransactions(TEST_USER, false).subscribeBy(
                onNext = {
                    assertEquals(1, it.size)
                    assertEquals(TEST_BORROWER_TRANSACTION, it[0].transactionId)
                    latch.countDown()
                }
        )

        latch.await(10, TimeUnit.SECONDS)
        lenderDisposable.dispose()
        borrowerDisposable.dispose()
    }

    @Test
    fun testDownloadOpenClosedTransactions() {
        val latch = CountDownLatch(1)
        val transactionsDisposable = downloadOpenClosedTransactions(TEST_USER)
                .subscribeBy(
                        onNext = {
                            assertNotNull(it[true])
                            assertNotNull(it[false])
                            assertEquals(1, it[true]?.size)
                            assertEquals(1, it[false]?.size)
                            assertEquals(TEST_LENDER_TRNSACTION, it[true]?.get(0)?.transactionId)
                            assertEquals(TEST_BORROWER_TRANSACTION, it[false]?.get(0)?.transactionId)
                            latch.countDown()
                        }
                )

        latch.await(10, TimeUnit.SECONDS)
        transactionsDisposable.dispose()
    }

    @Test
    fun testDownloadUserReviews() {
        val latch = CountDownLatch(1)
        val reviewsDisposable = downloadUserReviews(TEST_REVIEW_SUBJECT, TEST_REVIEW_COUNT).subscribeBy {
            assertEquals(TEST_REVIEW_COUNT, it.size)
            assertEquals(TEST_EXPECTED_REVIEW_ID, it[0].reviewId)
            latch.countDown()
        }

        latch.await(10, TimeUnit.SECONDS)
        reviewsDisposable.dispose()
    }

    @Test
    fun testDownloadLastNearbyCopies() {
        val latch = CountDownLatch(1)
        val copiesDisposable = downloadNewestNearbyCopies(0.0, 0.0). subscribeBy {
            // TODO Actual test logic? Sorry just doing informal testing here
            latch.countDown()
        }

        latch.await(10, TimeUnit.SECONDS)
        copiesDisposable.dispose()
    }
}