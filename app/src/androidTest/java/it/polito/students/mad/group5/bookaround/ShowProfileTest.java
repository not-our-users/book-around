package it.polito.students.mad.group5.bookaround;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.polito.students.mad.group5.bookaround.activities.ShowProfileActivity;
import it.polito.students.mad.group5.bookaround.models.User;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alessio on 20/03/2018.
 */

@RunWith(AndroidJUnit4.class)
public class ShowProfileTest {

    private static final User TEST_USER = new User(
        "0",
        "John",
        "john.doe@someprovider.com",
        "Hi my name is John and I do stuff for a living.",
        "New York",
            3,
            0,
            0
    );

    private static final User MOCK_RETURN_USER = new User(
            "0",
            "Mario",
            "mario.bianchi@pastapizzamandolino.it",
            "Hi-ah it's-a me a stereotype",
            "Roma",
            3,
            0,
            0
    );

    /**
     * This is used to modify how the activity to test is launched, by putting some extras
     * in the launch intent.
     */
    @Rule
    public ActivityTestRule<ShowProfileActivity> rule =
        new ActivityTestRule<ShowProfileActivity>(ShowProfileActivity.class, false, true) {
            @Override
            protected Intent getActivityIntent() {
                Intent intent = new Intent();
                intent.putExtra(InstrumentationRegistry.getTargetContext().getString(R.string.show_user_key), TEST_USER);
                return intent;
            }
        };

    /**
     * Test persistence of data after orientation change.
     */
    @Test
    public void testOrientationChange() {
        ShowProfileActivity activity = rule.getActivity();

        TextView nameView = activity.findViewById(R.id.show_name_view);
        TextView emailView = activity.findViewById(R.id.show_email_view);
        TextView bioView = activity.findViewById(R.id.show_bio_view);

        // Forcefully request an orientation change
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        assertEquals(TEST_USER.getName() ,nameView.getText());
        assertEquals(TEST_USER.getEmail(), emailView.getText());
        assertEquals(TEST_USER.getBio(), bioView.getText());

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        assertEquals(TEST_USER.getName() ,nameView.getText());
        assertEquals(TEST_USER.getEmail(), emailView.getText());
        assertEquals(TEST_USER.getBio(), bioView.getText());
    }

    /*
        This is no longer valid after updating ShowProfile with authentication

    // Test that returned user data after calling EditProfileActivity is correctly acquired.

    @Test
    public void testOnActivityResult() {
        ShowProfileActivity activity = rule.getActivity();

        // Mock up an ActivityResult:
        Intent returnIntent = new Intent();
        returnIntent.putExtra(activity.getString(R.string.show_user_key), MOCK_RETURN_USER);
        Instrumentation.ActivityResult activityResult = new Instrumentation.ActivityResult(Activity.RESULT_OK, returnIntent);

        // Create an ActivityMonitor that catch EditProfileActivity and return mock ActivityResult:
        Instrumentation.ActivityMonitor activityMonitor =
                InstrumentationRegistry.getInstrumentation()
                                       .addMonitor(
                                               EditProfileActivity.class.getName(),
                                               activityResult,
                                               true
                                       );

        // Simulate a button click that start ChildActivity for result:
        final ImageButton button = activity.findViewById(R.id.edit_button);
        activity.runOnUiThread(button::performClick);

        // Wait for the ActivityMonitor to be hit, Instrumentation will then return the mock ActivityResult:
        InstrumentationRegistry.getInstrumentation()
                               .waitForMonitorWithTimeout(activityMonitor, 1000);

        // Check that the returned values are now correct
        TextView nameView = activity.findViewById(R.id.show_name_view);
        TextView emailView = activity.findViewById(R.id.show_email_view);
        TextView bioView = activity.findViewById(R.id.show_bio_view);

        assertEquals(MOCK_RETURN_USER.getName() ,nameView.getText());
        assertEquals(MOCK_RETURN_USER.getEmail(), emailView.getText());
        assertEquals(MOCK_RETURN_USER.getBio(), bioView.getText());
    }
    */
}
