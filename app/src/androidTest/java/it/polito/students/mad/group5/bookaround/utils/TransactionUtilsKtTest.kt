package it.polito.students.mad.group5.bookaround.utils

import android.content.Context
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import it.polito.students.mad.group5.bookaround.models.LendingTransaction
import it.polito.students.mad.group5.bookaround.models.Review
import it.polito.students.mad.group5.bookaround.models.TransactionStatus
import org.junit.AfterClass
import org.junit.Assert.*
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(MockitoJUnitRunner::class)
class TransactionUtilsKtTest {

    @Mock
    lateinit var  mContext:Context


    val mCopyId = "-LAO1RRULOU8wyl1p7S2"
    val mLenderId = "Ux9uGg95jwa7gSFv8H05AxImnLH3"
    val reviewText = "paperino è meglio di topolino"
    //var mTransactionId:String? = null

    companion object {
        val email = "suvari@gifto12.com"
        val pwd = "qwerty1234"
        var mTransactionId:String?=null

        @BeforeClass @JvmStatic
        fun setUp(){
            val countDownLatch = CountDownLatch(1)
            val auth:FirebaseAuth?= FirebaseAuth.getInstance()
            auth?.signInWithEmailAndPassword(email,pwd)?.addOnCompleteListener{
                countDownLatch.countDown()
            }?:countDownLatch.countDown()
            countDownLatch.await(10, TimeUnit.SECONDS)

        }

        @AfterClass @JvmStatic
        fun tearDown(){
           FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(mTransactionId).setValue(null)
           FirebaseAuth.getInstance()?.signOut()
        }
    }




    @Test
    fun createTransaction() {
        val countDownLatch = CountDownLatch(1)
        val (task,mTransactionId) = createTransaction(mLenderId,mCopyId)
        val mBorrowerId = FirebaseAuth.getInstance().uid
        TransactionUtilsKtTest.mTransactionId=mTransactionId
        task?.addOnCompleteListener {
            countDownLatch.countDown()
        } ?: countDownLatch.countDown()

        countDownLatch.await(10, TimeUnit.SECONDS)

        val ref=FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(mTransactionId)
        ref.addValueEventListener { onDataChange {
            ref.removeEventListener(this)
            assertNotNull(it)
            val trans = it?.getValue(LendingTransaction::class.java)
            assertNotNull(trans)
            trans?.apply {
                assertEquals(mTransactionId,transactionId)
                assertEquals(mBorrowerId,borrowerId)
                assertEquals(mLenderId,lenderId)
                assertEquals(mCopyId,copyId)
                assertEquals(TransactionStatus.OPEN,status)
                assertEquals(true,borrowerApprovalFlag)
                assertEquals(false,lenderApprovalFlag)
                assertEquals(null,borrowerReviewId)
                assertEquals(null,lenderReviewId)
            }
        } }
    }

    @Test
    fun lenderApproveOrDenyTransaction() {
        lenderApproveOrDenyTransaction(TransactionUtilsKtTest.mTransactionId!!, true)
        val ref=FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(mTransactionId)
        ref.addValueEventListener {
            onDataChange{
                ref.removeEventListener(this)
                assertNotNull(it)
                val trans = it?.getValue(LendingTransaction::class.java)
                assertNotNull(trans)
                trans?.apply {
                    assertEquals(TransactionStatus.APPROVED,status)
                    assertFalse(lenderApprovalFlag)
                    assertFalse(borrowerApprovalFlag)
                }
            }
        }
    }

    @Test
    fun writeReview() {
        val ref=FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(mTransactionId)
        val countDownLatch = CountDownLatch(1)
        ref.addValueEventListener {
            onDataChange{
                ref.removeEventListener(this)
                assertNotNull(it)
                val trans = it?.getValue(LendingTransaction::class.java)
                val (task,mReviewId) = writeReview(trans?.transactionId!!, trans.borrowerId!!, trans.lenderId!!,reviewText,5)
                task?.addOnCompleteListener {
                    countDownLatch.countDown()
                } ?: countDownLatch.countDown()
                countDownLatch.await(10, TimeUnit.SECONDS)

                val mref=FirebaseDatabase.getInstance().getReference(Constants.DBKEY_REVIEWS).child(mReviewId)
                mref.addValueEventListener {
                    onDataChange {
                        mref.removeEventListener(this)
                        assertNotNull(it)
                        val review = it?.getValue(Review::class.java)
                        assertNotNull(review)
                        assertEquals(trans.transactionId,review?.transactionId)
                        assertEquals(trans.borrowerId,   review?.authorId)
                        assertEquals(trans.lenderId,     review?.subjectId)
                        assertEquals(reviewText,         review?.text)
                        assertEquals(5,          review?.vote)
                    }
                }
            }
        }
    }
}