package it.polito.students.mad.group5.bookaround;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.junit.Rule;
import org.junit.Test;

import it.polito.students.mad.group5.bookaround.activities.ShowCopyActivity;
import it.polito.students.mad.group5.bookaround.models.Copy;

/**
 * Created by Alessio on 17/04/2018.
 */
public class ShowCopyActivityTest {

    private final static String TEST_COPY_ID = "-LAAB8YCsSTGZIm0321t";
    private final static String TAG = "ShowCopyActivityTest";

    @Rule
    public final ActivityTestRule<ShowCopyActivity> rule = new ActivityTestRule<>(
            ShowCopyActivity.class, false, false);


    @Test
    public void testGeneralActivity() {
        DatabaseReference copyRef = FirebaseDatabase.getInstance().getReference("copies").child(TEST_COPY_ID);
        copyRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Copy copy = dataSnapshot.getValue(Copy.class);
                Intent intent = new Intent(InstrumentationRegistry.getTargetContext(), ShowCopyActivity.class);
                intent.putExtra(ShowCopyActivity.COPY_KEY, copy);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                InstrumentationRegistry.getInstrumentation().getTargetContext()
                        .startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

        while(rule.getActivity() == null || !rule.getActivity().isFinishing()) {
            try { Thread.sleep(1000); }
            catch (InterruptedException ie) { /* Who cares */ }
        }
    }
}