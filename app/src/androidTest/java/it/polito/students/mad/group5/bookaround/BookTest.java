package it.polito.students.mad.group5.bookaround;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.polito.students.mad.group5.bookaround.models.Book;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by Alessio on 14/04/2018.
 */
@RunWith(AndroidJUnit4.class)
public class BookTest {

    private Book testedBook;

    private static final String TEST_ISBN = "0062651234";
    private static final String TEST_TITLE = "Blood, Sweat, and Pixels";
    private static final String TEST_PUBLISHER = "Harper Paperbacks";
    private static final String TEST_API_ID = "KHVJMQAACAAJ";
    private static final String[] TEST_AUTHORS = { "Jason Schreier"  };
    private static final int TEST_YEAR = 2017;

    @Before
    public void setUp() throws Exception {
       // testedBook = new Book(TEST_ISBN);
    }

    @Test
    @SmallTest
    public void getIsbn() {
        assertEquals("ISBNs differ", TEST_ISBN, testedBook.getIsbn());
    }

    @Test
    @SmallTest
    public void getTitle() {
        assertEquals("Titles differ", TEST_TITLE, testedBook.getTitle());
    }

    @Test
    @SmallTest
    public void getPublisher() {
        assertEquals("Publishers differ", TEST_PUBLISHER, testedBook.getPublisher());
    }

    @Test
    @SmallTest
    public void getGoogleApiId() {
        assertEquals("Api Ids differ", TEST_API_ID, testedBook.getGoogleApiId());
    }

    @Test
    @SmallTest
    public void getAuthors() {
        assertArrayEquals("Authors differ", TEST_AUTHORS, testedBook.getAuthors().toArray());
    }

    @Test
    @SmallTest
    public void getEditionYear() {
        assertEquals("Edition years differ", TEST_YEAR, testedBook.getEditionYear());
    }
}