package it.polito.students.mad.group5.bookaround;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.polito.students.mad.group5.bookaround.activities.BarcodeReaderActivity;

import static org.junit.Assert.*;

/**
 * Created by Alessio on 14/04/2018.
 */
@RunWith(AndroidJUnit4.class)
public class BarcodeReaderActivityTest {

    // https://it.wikipedia.org/wiki/ISBN#/media/File:EAN-13-ISBN-13.svg
    private static final String TEST_ISBN10 = "316148410X";
    private static final String TEST_ISBN13 = "9783161484100";

    @Rule
    public ActivityTestRule<BarcodeReaderActivity> rule =
            new ActivityTestRule<>(BarcodeReaderActivity.class, false, true);

    @Test
    public void testBarcode() {
        // Wait for UI thread to become Idle (i.e. after saveButton is pressed and
        // finish() is invoked.

        while(!rule.getActivity().isFinishing()) {
            try { Thread.sleep(1000); }
            catch (InterruptedException ie) { /* Who cares */ }
        }

        // Retrieve activity result and check that it matches the changes.
        Instrumentation.ActivityResult result = rule.getActivityResult();
        Intent data = result.getResultData();
        assertEquals(Activity.RESULT_OK, result.getResultCode());
        assertNotNull(data);

        Bundle extras = data.getExtras();
        assertNotNull(extras);
        assertTrue(extras.containsKey(rule.getActivity().getString(R.string.read_barcode_key)));

        String readBarcode = extras.getString(rule.getActivity().getString(R.string.read_barcode_key));
        assertTrue(TEST_ISBN10.equals(readBarcode) || TEST_ISBN13.equals(readBarcode));
    }

}