package it.polito.students.mad.group5.bookaround;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.ImageButton;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import it.polito.students.mad.group5.bookaround.activities.EditProfileActivity;
import it.polito.students.mad.group5.bookaround.models.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Alessio on 20/03/2018.
 */

@RunWith(AndroidJUnit4.class)
public class EditProfileTest {

    private static final User TEST_USER = new User(
            "0",
            "John",
            "john.doe@someprovider.com",
            "Hi my name is John and I do stuff for a living.",
            "New York",
            40.730610,
            -73.935242,
            3
    );

    private static final String TEST_USERNAME = "Mario";
    private static final String TEST_EMAIL = "mario.bianchi@pastapizzamandolino.it";
    private static final String TEST_BIO = "Hi-ah it's-a me a stereotype";

    @Rule
    public ActivityTestRule<EditProfileActivity> rule =
            new ActivityTestRule<EditProfileActivity>(EditProfileActivity.class, false, true) {
                @Override
                protected Intent getActivityIntent() {
                    Intent intent = new Intent();
                    intent.putExtra(InstrumentationRegistry.getTargetContext().getString(R.string.edit_user_key), TEST_USER);
                    return intent;
                }
            };

    /**
     * Test that user data from the callee is acquired correctly.
     */
    @Test
    public void testUserReceived() {
        EditProfileActivity activity = rule.getActivity();

        final TextView nameView = activity.findViewById(R.id.edit_name_view);
        final TextView emailView = activity.findViewById(R.id.edit_email_view);
        final TextView bioView = activity.findViewById(R.id.edit_bio_view);

        assertEquals(TEST_USER.getName(), nameView.getText().toString());
        assertEquals(TEST_USER.getEmail(), emailView.getText().toString());
        assertEquals(TEST_USER.getBio(), bioView.getText().toString());
    }

    /**
     * Test that the activity correctly stores and returns changes to the user data.
     */
    @Test
    public void testUserChange() {
        EditProfileActivity activity = rule.getActivity();

        final TextView nameView = activity.findViewById(R.id.edit_name_view);
        final TextView emailView = activity.findViewById(R.id.edit_email_view);
        final TextView bioView = activity.findViewById(R.id.edit_bio_view);

        final ImageButton saveButton = activity.findViewById(R.menu.menu_save);

        // Simulate edits of the input fields and button press
        activity.runOnUiThread(() -> {
                nameView.setText(TEST_USER.getName());
                emailView.setText(TEST_USER.getEmail());
                bioView.setText(TEST_USER.getBio());
                saveButton.performClick();
        });

        // Wait for UI thread to become Idle (i.e. after saveButton is pressed and
        // finish() is invoked.
        InstrumentationRegistry.getInstrumentation().waitForIdleSync();

        // Retrieve activity result and check that it matches the changes.
        Instrumentation.ActivityResult result = rule.getActivityResult();
        assertEquals(Activity.RESULT_OK, result.getResultCode());

        Bundle extras = result.getResultData().getExtras();
        assertNotNull(extras);

        assertTrue(extras.containsKey(activity.getString(R.string.edit_user_key)));
        Object userObject = extras.getSerializable(activity.getString(R.string.edit_user_key));
        assertTrue(userObject instanceof User);
        User user = (User)userObject;

        assertEquals(TEST_USERNAME, user.getName());
        assertEquals(TEST_EMAIL, user.getEmail());
        assertEquals(TEST_BIO, user.getBio());
    }
}
