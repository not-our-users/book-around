package it.polito.students.mad.group5.bookaround.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.models.Chat;
import it.polito.students.mad.group5.bookaround.models.Message;
import it.polito.students.mad.group5.bookaround.utils.Constants;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentSendMessageDialog#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSendMessageDialog extends DialogFragment {

    private Disposable chatDisposable = null;
    private String copyOwnerChatId = null;
    private String newChatId = null;

    //Interface that needs to be implemented in order to get the result from this fragment
    public interface SendMessageDialogListener {
        void onCancelClicked();
        void onSendClicked(String newChatId);
    }

    private SendMessageDialogListener mListener;
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (FragmentSendMessageDialog.SendMessageDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @param title The desired title for this fragment
     * @param targetUserId The user recipient of the message
     *
     * @return A new instance of fragment FragmentSendMessageDialog.
     */
    public static FragmentSendMessageDialog newInstance(int title, String targetUserId) {
        FragmentSendMessageDialog fragment = new FragmentSendMessageDialog();
        Bundle args = new Bundle();
        args.putInt("title", title);
        args.putString("targetUserId", targetUserId);
        fragment.setArguments(args);
        return fragment;
    }


    private void startListeningForDialogs(String targetUserId) {
        chatDisposable =  FirebaseUtils.downloadUserChats()
                .doOnNext(chats -> {
                    // Filter out all empty chats
                    for (Chat chat : chats) {
                        for(IUser user : chat.getUsers()){
                            if(targetUserId.equals(user.getId())){
                                copyOwnerChatId=chat.getId();
                                chatDisposable.dispose();
                                return;
                            }
                        }
                    }
                })
                .doOnError(this::onError)
                .subscribe();
    }

    @Override
    public @NonNull Dialog onCreateDialog(Bundle savedInstanceState){
        assert getArguments() != null;
        int title = getArguments().getInt("title");
        final String targetUserId = getArguments().getString("targetUserId");
        startListeningForDialogs(targetUserId);
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        @SuppressLint("InflateParams")  // This is legit for AlertDialog views -> https://possiblemobile.com/2013/05/layout-inflation-as-intended/
        final View rootDialogView = inflater.inflate(R.layout.fragment_send_message_dialog,null);

        return new AlertDialog.Builder(getActivity())
                //.setIcon(R.drawable.alert_dialog_icon)
                .setTitle(title)
                .setView(rootDialogView)
                //In the send button the fragment creates a new chat in firebase and populates it with a transaction
                .setPositiveButton("send",
                        (dialog, whichButton) -> {
                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference ref = database.getReference(Constants.DBKEY_MEMBERS).push();
                            newChatId = ref.getKey();
                            ref.runTransaction(new Transaction.Handler() {
                                @Override
                                public Transaction.Result doTransaction(MutableData mutableData) {
                                    if(copyOwnerChatId!=null) {
                                        return Transaction.abort();
                                    }
                                    else{
                                        //DatabaseReference newChat = ref.push();
                                        chatDisposable.dispose();
                                        Map<String, Boolean> users = new HashMap<>();
                                        users.put(targetUserId, true);
                                        users.put(FirebaseAuth.getInstance().getUid(), true);
                                        mutableData.setValue(users);
                                        return Transaction.success(mutableData);
                                    }
                                }

                                @Override
                                public void onComplete(DatabaseError databaseError, boolean transactionCompleted, DataSnapshot dataSnapshot) {
                                    if(databaseError == null){
                                        //No error, we can now send the message and open the chat activity. If the transaction was aborted with no error, that means that there is already a
                                        // chat between the users, so we can just open it.
                                        if(transactionCompleted){
                                            DatabaseReference messageRef = database.getReference().child(Constants.DBKEY_MESSAGES).child(newChatId).push();
                                            EditText editText = rootDialogView.findViewById(R.id.NewMessage);
                                            Message mess = new Message( messageRef.getKey(),
                                                    editText.getText().toString(),
                                                    String.valueOf(System.currentTimeMillis()),
                                                    FirebaseAuth.getInstance().getUid());
                                            messageRef.setValue(mess);
                                        }
                                        mListener.onSendClicked(newChatId);
                                    }
                                    else {
                                        Log.d("FragmentSendMessage",databaseError.getDetails());
                                    }
                                }
                            });

                        }
                )
                .setNegativeButton("cancel",
                        (dialog, whichButton) -> {
                            //user clicked cancel
                            chatDisposable.dispose();
                            mListener.onCancelClicked();
                        }
                )
                .create();

    }

    private void onError(Throwable t) {
        Activity context = getActivity();
        if (context != null) {
            Toast.makeText(context.getApplicationContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        }
        Log.e("FragmentSendMessage", t.getMessage());

    }
}
