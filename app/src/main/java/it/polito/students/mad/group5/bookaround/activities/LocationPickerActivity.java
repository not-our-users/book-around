package it.polito.students.mad.group5.bookaround.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import it.polito.students.mad.group5.bookaround.R;

public class LocationPickerActivity extends FragmentActivity implements OnMapReadyCallback, SearchView.OnQueryTextListener {

    private static final String TAG = "LocationPickerActivity";

    public static final String ADDRESS_TOWN_KEY = "AddressTown";
    public static final String ADDRESS_LAT_KEY = "AddressLatitude";
    public static final String ADDRESS_LON_KEY = "AddressLongitude";

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private static final int MAP_ZOOM = 15;

    private Location lastUserLocation = null;   // TODO wait. Why is this never used?
    private FusedLocationProviderClient locationProviderClient;
    private GoogleMap mMap;
    private GeocoderTask geocoderTask = null;
    private GeocoderReverseTask geocoderReverseTask = null;

    private AddressAdapter addressAdapter = new AddressAdapter();

    private Marker lastMarker = null;

    // Handles position updates and gets the latest value into lastUserLocation
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            lastUserLocation = locationResult.getLastLocation();
        }
    };

    // ViewHolder for address search results
    private class AddressViewHolder extends RecyclerView.ViewHolder {

        private View view;
        private Address currentAddress = null;

        public AddressViewHolder(View itemView) {
            super(itemView);
            this.view = itemView;
        }

        public void setData(Address data) {
            currentAddress = data;
            TextView addressView = view.findViewById(R.id.address_view);
            addressView.setText(currentAddress.getAddressLine(0));
        }

        public void moveMapToCurrentAddress() {
            if (currentAddress == null) { return; }
            moveMapTo(currentAddress);
        }
    }

    // RecyclerAdapter for addresses searched using Geocoder
    private class AddressAdapter extends RecyclerView.Adapter<AddressViewHolder> {

        private List<Address> addressList = new ArrayList<>();

        @NonNull
        @Override
        public AddressViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.location_search_list_item, parent, false);
            AddressViewHolder holder = new AddressViewHolder(view);
            view.setOnClickListener(v -> holder.moveMapToCurrentAddress());
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull AddressViewHolder holder, int position) {
            holder.setData(addressList.get(position));
        }

        @Override
        public int getItemCount() {
            return addressList.size();
        }

        public void setItems(Collection<Address> items) {
            addressList.clear();
            RecyclerView searchView = findViewById(R.id.search_results);

            if (items.size() == 0) { searchView.setVisibility(View.GONE); }
            else {
                addressList.addAll(items);
                searchView.setVisibility(View.VISIBLE);
            }

            notifyDataSetChanged();
        }
    }

    // Queries the Geocoder task to get results based on user's input
    private static class GeocoderTask extends AsyncTask<String, Address, List<Address>> {

        private final static String TAG = "GeocoderTask";

        private WeakReference<LocationPickerActivity> context;
        private Geocoder geocoder;  // Provides access to a REST API too lookup locations based on names or coordinates

        private GeocoderTask(LocationPickerActivity context) {
            this.context = new WeakReference<>(context);
            geocoder = new Geocoder(context);
        }

        @Override
        protected List<Address> doInBackground(String... queries) {
            if (queries.length == 0) { return Collections.emptyList(); }

            try {
                return geocoder.getFromLocationName(queries[0], 5);
            }
            catch (IOException ioe) {
                context.get().runOnUiThread(() -> Toast.makeText(
                        context.get(),
                        context.get().getString(R.string.no_internet_connection),
                        Toast.LENGTH_LONG
                    ).show()
                );
            }
            catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }

            return Collections.emptyList();
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {
            super.onPostExecute(addresses);
            context.get().addressAdapter.setItems(addresses);
        }
    }

    // Does a reverse lookup for an Address based on latitude and longitude
    private static class GeocoderReverseTask extends AsyncTask<Double, Address, Address> {

        private static final String TAG = "GeocoderReverseTask";

        private WeakReference<LocationPickerActivity> context;
        private Geocoder geocoder;  // Provides access to a REST API too lookup locations based on names or coordinates

        private GeocoderReverseTask(LocationPickerActivity context) {
            this.context = new WeakReference<>(context);
            geocoder = new Geocoder(context);
        }

        @Override
        protected Address doInBackground(Double... coords) {
            if (coords.length < 2) { return null; }
            else {
                try {
                    List<Address> result = geocoder.getFromLocation(coords[0], coords[1], 1);

                    if (result.size() == 0) { return null; }
                    else { return result.get(0); }
                }
                catch (IOException ioe) {
                    context.get().runOnUiThread(() -> Toast.makeText(
                            context.get(),
                            context.get().getString(R.string.no_internet_connection),
                            Toast.LENGTH_LONG
                        ).show()
                    );
                }
                catch (Exception e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            super.onPostExecute(address);
            if (address != null) {
                context.get().moveMapTo(address);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);

        locationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        SearchView searchView = findViewById(R.id.location_search_view);
        searchView.setOnQueryTextListener(this);

        RecyclerView searchResultView = findViewById(R.id.search_results);
        searchResultView.setAdapter(addressAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        searchResultView.setLayoutManager(layoutManager);
        searchResultView.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        updateSearchResult(newText);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        updateSearchResult(query);
        return true;
    }

    // Performs an asynchronous lookup for the user's search
    private void updateSearchResult(String query) {
        if (query.length() == 0) {
            addressAdapter.setItems(Collections.emptyList());
            return;
        }
        if (geocoderTask != null) { geocoderTask.cancel(true); }
        geocoderTask = new GeocoderTask(this);
        geocoderTask.execute(query);
    }

    @Override
    protected void onPause() {
        super.onPause();    // Disable all location updates
        if (locationProviderClient != null) { locationProviderClient.removeLocationUpdates(locationCallback); }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (geocoderTask != null) { geocoderTask.cancel(true); }
        if (geocoderReverseTask != null) { geocoderReverseTask.cancel(true); }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnMapClickListener(latLng -> {
            if (latLng != null) { findLocation(latLng); }
        });

        enableMyLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                enableMyLocation();
            }
        }
    }

    // Check (and ask) the user for location permissions. If granted, the 'My Location' button is enabled
    // and the activity starts listening for location updates
    private void enableMyLocation() {
        if (
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{ android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION },
                    REQUEST_LOCATION_PERMISSION
            );
        }
        else {
            mMap.setMyLocationEnabled(true);
            locationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                if (location != null) {
                    mMap.moveCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            new LatLng(location.getLatitude(), location.getLongitude()),
                            MAP_ZOOM
                        )
                    );
                    lastUserLocation = location;
                }
            });

            View myLocationButton = findViewById(R.id.my_location_button);
            myLocationButton.setVisibility(View.VISIBLE);

            // Schedule location updates every minute with balanced power consumption
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(6000); // Each minute
            locationRequest.setFastestInterval(6000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, getMainLooper());
        }
    }

    // Given an address found using Geocoder, update the current Marker and move the map
    // to the new location
    private void moveMapTo(Address address) {
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
        if (lastMarker == null) {
            lastMarker = mMap.addMarker(
                new MarkerOptions()
                    .position(latLng)
                    .title(address.getLocality())
                    .snippet(address.getAddressLine(0))
            );
        }
        else {
            lastMarker.setPosition(latLng);
            lastMarker.setTitle(address.getLocality());
            lastMarker.setSnippet(address.getAddressLine(0));
        }

        mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                        latLng,
                        MAP_ZOOM
                )
        );
    }

    // Confirm location and return it according to data stored inside the Marker
    public void returnResult(View v) {
        Intent returnData = new Intent();
        if (lastMarker != null) {
            returnData.putExtra(ADDRESS_LAT_KEY, lastMarker.getPosition().latitude);
            returnData.putExtra(ADDRESS_LON_KEY, lastMarker.getPosition().longitude);
            returnData.putExtra(ADDRESS_TOWN_KEY, lastMarker.getTitle());
            setResult(RESULT_OK, returnData);
        }
        else { setResult(RESULT_CANCELED); }
        finish();
    }

    // Take the last user location and perform a reverse lookup on Geocoder to get the city's name.
    public void findMyLocation(View v) {
        if (
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            locationProviderClient.getLastLocation().addOnSuccessListener(location -> {
                lastUserLocation = location;
                if (location == null) {
                    if (isLocationEnabled()) {
                        Toast.makeText(this, getString(R.string.error_location), Toast.LENGTH_LONG).show();
                    }
                    else {
                        promptEnableLocation();
                    }
                }
                else {
                    findLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            });
        }
    }

    public void findLocation(@NonNull LatLng latLng) {
        if (geocoderReverseTask != null) {
            geocoderReverseTask.cancel(true);
        }
        geocoderReverseTask = new GeocoderReverseTask(this);
        geocoderReverseTask.execute(latLng.latitude, latLng.longitude);
    }

    /**
     * Checks if at least one location provider is enabled. If neither the GPS nor the Network
     * provider are enabled, it returns false.
     */
    private boolean isLocationEnabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled;
        boolean network_enabled;

        if (lm == null) {
            Log.wtf(TAG, "Somehow, LOCATION_SERVICE doesn't exist.");
            return false;
        }

        gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        return gps_enabled || network_enabled;
    }

    /**
     * Shows a dialog asking the user to enable the location service.
     */
    private void promptEnableLocation() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.gps_network_not_enabled));

        dialog.setPositiveButton(getString(R.string.open_location_settings), (clickedDialog, which) -> {
            Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(myIntent);
        });
        dialog.setNegativeButton(getString(R.string.cancel), (clickedDialog, which) -> {});

        dialog.show();
    }
}
