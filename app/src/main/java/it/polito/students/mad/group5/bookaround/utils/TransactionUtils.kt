package it.polito.students.mad.group5.bookaround.utils

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import it.polito.students.mad.group5.bookaround.R.mipmap.transaction
import it.polito.students.mad.group5.bookaround.models.LendingTransaction
import it.polito.students.mad.group5.bookaround.models.Review
import it.polito.students.mad.group5.bookaround.models.TransactionStatus
import it.polito.students.mad.group5.bookaround.models.Copy

data class CreateTransactionResult (val task:Task<Void>?, val transactionId: String?, val transaction:LendingTransaction?)
fun createTransaction(lenderId:String, copyId:String ): CreateTransactionResult {

    val ref = FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS)
    val transactionRef = ref.push()
    val borrowerId = FirebaseAuth.getInstance().uid?: throw SecurityException("User must be authenticated")

    val transaction = LendingTransaction(transactionRef.key, borrowerId,lenderId,copyId)
    return CreateTransactionResult(transactionRef.setValue(transaction),transactionRef.key,transaction)
}

/**
 * @param transactionId The transactionID received by the lender
 * @param lenderResponse True:transaction approved, False:transaction rejected
 */
fun lenderApproveOrDenyTransaction (transactionId:String, lenderResponse:Boolean){
    val ref = FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(transactionId)

    ref.runTransaction{
        doTransaction {
            val trans:LendingTransaction? = it?.getValue(LendingTransaction::class.java)

            trans?.apply {
                if(borrowerApprovalFlag&&!lenderApprovalFlag&&status==TransactionStatus.OPEN){
                    status= if(lenderResponse) TransactionStatus.APPROVED else TransactionStatus.REJECTED
                    borrowerApprovalFlag=false
                    it.value=this
                    return@doTransaction Transaction.success(it)
                }
                else
                    return@doTransaction Transaction.abort()
            }
            Transaction.abort()
        }
        onComplete { databaseError, success, dataSnapshot ->
            if(success) {
                if (lenderResponse) {   // Update book status in case of acceptance
                    val transaction = dataSnapshot?.getValue(LendingTransaction::class.java)
                    transaction?.copyId?.let {
                        getCopiesRef().child(it).child(Constants.DBKEY_COPY_STATUS).setValue(Copy.Status.BORROWED)
                    }
                }
            }
            else {
                Log.d("LenderAcceptTransaction",databaseError.toString())
            }
        }
    }
}

fun confirmBookExchange (transactionId:String, date:Long?){
    val ref = FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(transactionId)
    val uid = FirebaseAuth.getInstance().uid?:throw SecurityException()
    ref.runTransaction{
        doTransaction {
            val trans:LendingTransaction? = it?.getValue(LendingTransaction::class.java)

            trans?.apply {
                if(status==TransactionStatus.APPROVED){

                    if(uid == borrowerId){
                        borrowerApprovalFlag=true
                    }
                    else if(uid==lenderId){
                        lenderApprovalFlag=true
                        expireDate=date
                    }
                    else
                        return@doTransaction Transaction.abort()

                    if(borrowerApprovalFlag&&lenderApprovalFlag){
                        status=TransactionStatus.ACTIVE
                        borrowerApprovalFlag=false
                        lenderApprovalFlag=false
                    }
                    it.value=this
                    return@doTransaction Transaction.success(it)

                }
                else
                    return@doTransaction Transaction.abort()
            }
            Transaction.abort()
        }
        onComplete { databaseError, b, dataSnapshot ->
            if(!b) Log.d("LenderAcceptTransaction",databaseError.toString())
        }
    }
}

fun confirmBookRestitution (transactionId:String){
    val ref = FirebaseDatabase.getInstance().getReference(Constants.DBKEY_TRANSACTIONS).child(transactionId)
    val uid = FirebaseAuth.getInstance().uid?:throw SecurityException()
    ref.runTransaction{
        doTransaction {
            val trans:LendingTransaction? = it?.getValue(LendingTransaction::class.java)

            trans?.apply {
                if(status==TransactionStatus.ACTIVE){

                    if(uid==lenderId){
                        lenderApprovalFlag=true
                    }
                    else
                        return@doTransaction Transaction.abort()

                    if(lenderApprovalFlag){
                        status=TransactionStatus.CLOSED
                        dateClosed=System.currentTimeMillis()
                        getCopiesRef().child(trans.copyId)
                                .child(Constants.DBKEY_COPY_STATUS)
                                .setValue(Copy.Status.NOT_BORROWED)
                    }
                    it.value=this
                    return@doTransaction Transaction.success(it)

                }
                else
                    return@doTransaction Transaction.abort()
            }
            Transaction.abort()
        }
        onComplete { databaseError, b, dataSnapshot ->
            if (!b) {
                Log.d("LenderAcceptTransaction",databaseError.toString())
            }
        }
    }
}

data class WriteReviewResult(val task:Task<Void>?, val reviewId: String?)
fun writeReview(transactionId: String, authorId:String, reviewedId:String, reviewText: String, reviewVote: Int)
        :WriteReviewResult
{
    val ref = FirebaseDatabase.getInstance().getReference(Constants.DBKEY_REVIEWS).child(reviewedId)
    val reviewRef = ref.push()
    val review = Review(reviewRef.key,transactionId,authorId,reviewedId,reviewVote,reviewText, System.currentTimeMillis())
    return WriteReviewResult(reviewRef.setValue(review),reviewRef.key)
}