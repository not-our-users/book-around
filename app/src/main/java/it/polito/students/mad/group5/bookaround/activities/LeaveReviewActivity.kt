package it.polito.students.mad.group5.bookaround.activities

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer
import it.polito.students.mad.group5.bookaround.models.Copy
import it.polito.students.mad.group5.bookaround.models.LendingTransaction
import it.polito.students.mad.group5.bookaround.models.User
import it.polito.students.mad.group5.bookaround.utils.*
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadUser

class LeaveReviewActivity : AppCompatActivity() {

    private lateinit var mFilterFragment: FragmentFilterDrawer
    private lateinit var drawer: DrawerLayout
    private var isFiltersView = false
    private lateinit var transaction: LendingTransaction
    private var sharedCopy: Copy? = null
    private lateinit var userId: String
    private var isOwner = false

    private lateinit var status: TextView

    companion object {
        const val TRANSACTION_KEY: String = "TRANSACTION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leave_review)

        userId = FirebaseAuth.getInstance().uid ?: throw SecurityException("User must be authenticated!")

        transaction = intent?.getSerializableExtra(TRANSACTION_KEY) as? LendingTransaction
                ?: run { this@LeaveReviewActivity.finish(); return@onCreate }

        if (userId != transaction.lenderId && userId != transaction.borrowerId) {
            throw SecurityException("User is not part of this transaction!")
        }

        isOwner = transaction.lenderId == userId
        val ratedUserId = if (isOwner) transaction.borrowerId else transaction.lenderId
        downloadUser(ratedUserId).toLiveData().observe(
                { this.lifecycle },
                {
                    it?.value?.let { setUserToReview(it) }
                    it?.exception?.printStackTrace()
                }
        )

        downloadCopy(transaction.copyId).toLiveData().observe(
                { lifecycle },
                {
                    it?.value?.let { setCopy(it) }
                    it?.exception?.printStackTrace()
                }
        )

        setUpDrawerAndToolbar()
        setTitle(R.string.title_activity_leave_review)


        val ratingBar= findViewById<RatingBar>(R.id.stars_rating)
        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, _ ->
            if(rating<1.0f)
                ratingBar.rating = 1.0f
        }

        findViewById<FloatingActionButton>(R.id.floatingActionButton).setOnClickListener(this@LeaveReviewActivity::publishReview)
        mFilterFragment = supportFragmentManager.findFragmentById(R.id.filter_drwr_fragment) as FragmentFilterDrawer
    }


    private fun setCopy(copy: Copy) {
        val thumbImg = findViewById<ImageView>(R.id.copy_thumb_img)
        val pictureToLoad = copy.pictureList?.let {
            if (it.size > 0) it[0] else null
        }
        val loader = pictureToLoad?.let { Picasso.get().load(it) } ?: Picasso.get().load(R.mipmap.not_found)
        loader.placeholder(R.mipmap.not_found).fit().into(thumbImg)
        copy.bookData.subscribe(
                { findViewById<TextView>(R.id.book_title).text = it.title },
                { it.printStackTrace() }
        )
    }

    private fun setUserToReview(u: User) {
        findViewById<TextView>(R.id.user_name).text = u.name
        u.profilePicBitmap?.let{ findViewById<ImageView>(R.id.reviewed_thumb_img)?.setImageBitmap(it) }
    }

    fun publishReview(v:View){
        val ratingBar= findViewById<RatingBar>(R.id.stars_rating)
        val rating = Math.round(ratingBar.rating)
        val comment = findViewById<EditText>(R.id.review_text).text.toString()

        val dialog = ProgressDialog.show(this, "", getString(R.string.wait_dialog), true)

        writeReview(
            transaction.transactionId,
            if (isOwner) transaction.lenderId else transaction.borrowerId,
            if (isOwner) transaction.borrowerId else transaction.lenderId,
            comment,
            rating
        )
        .apply {
            task
            ?.addOnCompleteListener({
                dialog.dismiss()
                if (it.isSuccessful) {
                    getTransactionRef()
                            .child(transaction.transactionId)
                            .child(
                                    if (isOwner) Constants.DBKEY_TRANS_LENDER_REVIEW
                                    else Constants.DBKEY_TRANS_BORROWER_REVIEW
                            )
                            .setValue(reviewId!!)

                    finish()
                }
                else {
                    it.exception?.let {
                        Toast.makeText(this@LeaveReviewActivity, getString(R.string.internal_error), Toast.LENGTH_LONG).show()
                        it.printStackTrace()
                    }
                }
            })
            ?: run {
                dialog.dismiss()
                Toast.makeText(this@LeaveReviewActivity, getString(R.string.internal_error), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun setUpDrawerAndToolbar() {
        // Setting the drawer
        val drawerFragment = supportFragmentManager.findFragmentById(R.id.nav_drwr_fragment)
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        // Setting the toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        val toggle = ActionBarDrawerToggle(this,drawer,toolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
    }

    //START SEARCH CLASSES

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_search, menu)
        //getMenuInflater().inflate(R.menu.menu_filters, menu);
        val searchItem = menu.findItem(R.id.mi_search)
        val sv = searchItem.actionView as android.support.v7.widget.SearchView
        sv.setIconifiedByDefault(false)
        val fv = findViewById<View>(R.id.filters_frame)
        sv.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            internal var filtersMenuItem = menu.findItem(R.id.btn_advanced_search)
            override fun onViewDetachedFromWindow(arg0: View) {
                filtersMenuItem.isVisible = false
                if (isFiltersView) {
                    val animate = TranslateAnimation(
                            0f,
                            0f,
                            0f,
                            (-fv.height).toFloat())
                    animate.duration = 500
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate)
                    fv.visibility = View.INVISIBLE
                    isFiltersView = false
                }
            }


            override fun onViewAttachedToWindow(arg0: View) {
                filtersMenuItem.isVisible = true
            }
        })

        sv.setOnQueryTextListener(object : android.support.v7.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (isFiltersView) {
                    val animate = TranslateAnimation(
                            0f,
                            0f,
                            0f,
                            (-fv.height).toFloat())
                    animate.duration = 500
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate)
                    fv.visibility = View.INVISIBLE
                    isFiltersView = false
                }
                mFilterFragment.StartSearchActivity(query)
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })

        val advS = findViewById<Button>(R.id.searchButton)
        advS.setOnClickListener { view ->
            val s = sv.query.toString()

            mFilterFragment.StartSearchActivity(s)

            val animate = TranslateAnimation(
                    0f,
                    0f,
                    0f,
                    (-fv.height).toFloat())
            animate.duration = 500
            //animate.setFillAfter(false);
            fv.startAnimation(animate)
            fv.visibility = View.INVISIBLE
            isFiltersView = false
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        when (id) {
            R.id.btn_advanced_search -> {
                val fv = findViewById<View>(R.id.filters_frame)
                if (!isFiltersView) {
                    fv.visibility = View.VISIBLE
                    val animate = TranslateAnimation(
                            0f,
                            0f,
                            (-fv.height).toFloat(),
                            0f)
                    animate.duration = 500
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate)
                    isFiltersView = true
                } else {
                    val animate = TranslateAnimation(
                            0f,
                            0f,
                            0f,
                            (-fv.height).toFloat())
                    animate.duration = 500
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate)
                    fv.visibility = View.INVISIBLE
                    isFiltersView = false
                }
            }
            else -> {
            }
        }

        return super.onOptionsItemSelected(item)
    }

    //END SEARCH CLASSES
}
