package it.polito.students.mad.group5.bookaround.models;

import android.content.Context;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import io.reactivex.Single;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.utils.BookQueryBuilder;

/**
 * Created by Alessio on 09/04/2018.
 */
public class Copy implements Serializable {
    private String isbn, ownerId, copyId, details, googleApiId;
    private Condition condition;
    private Status status;
    private HashMap<String, String> pictures = new HashMap<>();
    private String town;
    private double latitude;
    private double longitude;

    public enum Condition {
        MINT (R.string.copy_condition_mint),
        VERY_GOOD (R.string.copy_condition_very_good),
        GOOD (R.string.copy_condition_good),
        WORN_OUT (R.string.copy_condition_worn_out),
        DAMAGED (R.string.copy_condition_damaged);

        private int localizedId;

        Condition(int localizedId) {
            this.localizedId = localizedId;
        }

        public String getLocalizedVersion(Context context) {
            return context.getString(localizedId);
        }
    }

    public enum Status {
        BORROWED(R.string.borrowed, android.R.color.holo_red_dark),
        NOT_BORROWED(R.string.available, android.R.color.holo_green_light),
        HIDDEN(R.string.hidden, android.R.color.darker_gray),
        DELETED(R.string.deleted, android.R.color.holo_red_dark);

        public final int localizedId;
        public final int colorId;

        Status(int localizedId, int colorId) {
            this.localizedId = localizedId;
            this.colorId = colorId;
        }
    }

    public Copy () { }

    public Copy(String ownerId) {
        this.ownerId = ownerId;
        this.status = Status.NOT_BORROWED;
    }

    //Copy should be created before passing to booktoshare view with the informations not supplied by the user
    public Copy(String ownerId, String copyId){
        this.ownerId = ownerId;
        this.copyId = copyId;
        this.isbn = "";
        this.pictures = null;
        this.condition = Condition.GOOD;
        this.status = Status.NOT_BORROWED;
        this.details = "";
        this.town = "";
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getCopyId() {
        return copyId;
    }

    public void setCopyId(String copyId) {
        this.copyId = copyId;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Lookup for Bookdata
     */
    @Exclude
    public Single<Book> getBookData() {
        if (googleApiId != null) { return BookQueryBuilder.lookupByApiId(googleApiId); }
        return Single.just(new Book());
    }

    public HashMap<String, String> getPictures() {
        return pictures;
    }

    public void setPictures(HashMap<String, String> pictures) {
        this.pictures = pictures;
    }

    @Exclude
    public List<String> getPictureList() {
        if(pictures == null){
            return null;
        }
        return new ArrayList<>(new TreeMap<>(pictures).values());
    }
    
    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getTown() { return town; }

    public void setTown(String town) { this.town = town; }

    public String getGoogleApiId() { return googleApiId; }

    public void setGoogleApiId(String googleApiId){
        this.googleApiId = googleApiId;
    }
}
