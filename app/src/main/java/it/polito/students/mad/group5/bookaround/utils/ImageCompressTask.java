package it.polito.students.mad.group5.bookaround.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Adapted from https://github.com/adigunhammedolalekan/easyphotoupload

public class ImageCompressTask /*implements Runnable*/ {
    private Context mContext;
    private List<String> originalPaths = new ArrayList<>();
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private List<File> result = new ArrayList<>();
    private IImageCompressTaskListener mIImageCompressTaskListener;
    private int _width, _height;

    /**
     *
     * @param context application context
     * @param path to the file to compress
     * @param compressTaskListener callback for the compression task
     * @param width = desired image width
     * @param height = desired image height
     */
    public ImageCompressTask(Context context, String path, IImageCompressTaskListener compressTaskListener,
                             int width,int height) {

        originalPaths.add(path);
        mContext = context;
        _width=width;
        _height=height;
        mIImageCompressTaskListener = compressTaskListener;
    }

    /**
     *
     * @param context application context
     * @param paths list of file to compress
     * @param compressTaskListener callback for the compresson task
     * @param width target width
     * @param height target height
     */
    public ImageCompressTask(Context context, List<String> paths, IImageCompressTaskListener compressTaskListener,
                             int width,int height) {
        originalPaths = paths;
        mContext = context;
        mIImageCompressTaskListener = compressTaskListener;
        _width=width;
        _height=height;
    }
    //@Override
    public void Compress() {

        try {

            //Loop through all the given paths and collect the compressed file from ImageUtil.getCompressed(Context, String)
            for (String path : originalPaths) {
                File file = ImageUtils.getCompressed(mContext, path, _width,_height);
                //add it!
                result.add(file);
            }
            //use Handler to post the result back to the main Thread
            mHandler.post(() -> {

                if(mIImageCompressTaskListener != null)
                    mIImageCompressTaskListener.onComplete(result);
            });
        }catch (final IOException ex) {
            //There was an error, report the error back through the callback
            mHandler.post(() -> {
                if(mIImageCompressTaskListener != null)
                    mIImageCompressTaskListener.onError(ex);
            });
        }
    }
}
