package it.polito.students.mad.group5.bookaround.services;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.JobIntentService;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.activities.InboxActivity;
import it.polito.students.mad.group5.bookaround.utils.Constants;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;

public class NewMessageNotificationService extends JobIntentService {

    private Disposable messageListDisposable = null;

    @Override
    protected void onHandleWork (@android.support.annotation.NonNull Intent startIntent){
        Intent intent = new Intent(this, InboxActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "default")
                .setSmallIcon(R.mipmap.logo_r)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        messageListDisposable = FirebaseUtils.downloadAllUnreadMessages()
                .doOnError(Throwable::printStackTrace)
                .doOnNext( unread -> {
                    int totalUnreadCount = 0;
                    int totalUnreadChats = 0;
                    for (String chatKey : unread.keySet()) {
                        int unreadCount = unread.get(chatKey).size();
                        if (unreadCount > 0) {
                            totalUnreadChats++;
                            totalUnreadCount += unreadCount;
                        }
                    }
                    if (totalUnreadCount > 0) {
                        mBuilder.setContentTitle(getString(R.string.notification_title_new_messages));
                        mBuilder.setContentText(getString(R.string.notification_text_new_messages, totalUnreadCount, totalUnreadChats));
                        notificationManager.notify(Constants.NOTIFY_ID_NEW_MSG ,mBuilder.build());
                    }
                    else {
                        notificationManager.cancel(Constants.NOTIFY_ID_NEW_MSG);
                    }
                })
                .subscribe();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        rootIntent = new Intent(getApplicationContext(), NewMessageNotificationService.class);
        NewMessageNotificationService.enqueueWork(getApplicationContext(),NewMessageNotificationService.class,1,rootIntent);
    }
}
