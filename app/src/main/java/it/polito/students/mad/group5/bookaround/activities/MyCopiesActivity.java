package it.polito.students.mad.group5.bookaround.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.List;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer;
import it.polito.students.mad.group5.bookaround.fragments.FragmentNavDrawer;
import it.polito.students.mad.group5.bookaround.models.Copy;

public class MyCopiesActivity extends AppCompatActivity {

    private static final int REQUEST_ADD_COPY = 1;

    // To know more: https://github.com/firebase/FirebaseUI-Android/blob/master/database/README.md#using-firebaseui-to-populate-a-recyclerview
    FirebaseRecyclerAdapter myCopiesListAdapter;
    private DrawerLayout drawer;
    private boolean isFiltersView=false;
    private FragmentFilterDrawer mFilterFragment;
    private String pendingCopyKey = null;

    private class MyCopyListItemHolder extends RecyclerView.ViewHolder {

        private TextView bookTitleView, bookDescriptionView;
        private ImageView pictureView;
        private Copy currentCopy;

        private Disposable bookDisposable = null;

        MyCopyListItemHolder(View itemView) {
            super(itemView);
            bookTitleView = itemView.findViewById(R.id.my_copies_li_title_view);
            bookDescriptionView = itemView.findViewById(R.id.my_copies_li_desc_view);
            pictureView = itemView.findViewById(R.id.my_copies_li_thumb_view);
        }

        public void setData(Copy copy) {
            // We no longer care of the previous copy's result
            if (bookDisposable != null) { bookDisposable.dispose(); }

            bookDisposable = copy.getBookData().subscribe(
                    book -> bookTitleView.setText(book.getTitle()),
                    error -> {
                        Toast.makeText(getApplicationContext(), R.string.error_network_old_data, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
            );
            bookDescriptionView.setText(copy.getDetails());

            List<String> pictures = copy.getPictureList();
            RequestCreator picassoLoader = pictures != null && pictures.size() > 0 ?
                    Picasso.get().load(pictures.get(0)) : Picasso.get().load(R.mipmap.not_found);

            picassoLoader.placeholder(R.mipmap.not_found)
                         .centerCrop()
                         .fit()
                         .into(pictureView);

            currentCopy = copy;
        }

        public Copy getCurrentCopy() { return currentCopy; }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_copies);

        setUpDrawer();
        setUpToolbar();
        setTitle(getString(R.string.title_activity_my_copies));

        mFilterFragment = (FragmentFilterDrawer)getSupportFragmentManager().findFragmentById(R.id.filter_drwr_fragment);

        setupRecyclerView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_ADD_COPY) {
            if (resultCode == RESULT_OK) {
                if (data.hasExtra(EditCopyActivity.COPY_KEY)) {
                    pendingCopyKey = data.getStringExtra(EditCopyActivity.COPY_KEY);
                    findViewById(R.id.copyLoadProgressBar).setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void setupRecyclerView() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.d("SetupTecyclerView","User must be authenticated to show its copies!");
        if (user == null) {
            //Something went wrong with the authentication, now we force the logout
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        Toast toast = Toast.makeText(getApplicationContext(),
                                R.string.force_relogin_toast, Toast.LENGTH_SHORT);
                        toast.show();
                        startActivity(new Intent(MyCopiesActivity.this, LoginActivity.class));
                        finish();
                    });
        }

        assert user != null;
        Query userCopiesQuery = FirebaseDatabase.getInstance()
                .getReference(getString(R.string.DBKEY_COPIES))
                .orderByChild(getString(R.string.DBKEY_OWNERID))
                .equalTo(user.getUid());

        FirebaseRecyclerOptions<Copy> options =
                new FirebaseRecyclerOptions.Builder<Copy>()
                    .setQuery(userCopiesQuery, Copy.class)
                    .setLifecycleOwner(this)
                    .build();

        myCopiesListAdapter = new FirebaseRecyclerAdapter<Copy, MyCopyListItemHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MyCopyListItemHolder holder, int position, @NonNull Copy model) {
                holder.setData(model);
            }

            @NonNull
            @Override
            public MyCopyListItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.my_copy_list_item, parent, false);
                MyCopyListItemHolder holder = new MyCopyListItemHolder(view);
                view.setOnClickListener(v -> StartShowCopyActivity(holder.getCurrentCopy()) );
                return holder;
            }

            @Override
            public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot, int newIndex, int oldIndex) {
                super.onChildChanged(type, snapshot, newIndex, oldIndex);
                if (snapshot.getKey().equals(pendingCopyKey)) {
                    findViewById(R.id.copyLoadProgressBar).setVisibility(View.GONE);
                    pendingCopyKey = null;
                }
            }
        };

        RecyclerView recyclerView = findViewById(R.id.recycler_copies_list);
        recyclerView.setAdapter(myCopiesListAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void StartShowCopyActivity(Copy copy) {
        // Finish downloading book data first
        Intent showCopyIntent = new Intent(getApplicationContext(), ShowCopyActivity.class);
        showCopyIntent.putExtra(ShowCopyActivity.COPY_KEY, copy);
        startActivity(showCopyIntent);
    }

    public void startCopyActivity(View v) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.d("startCopyActivity","User must be authenticated to show its copies!");
        if (user == null) {
            //Something went wrong with the authentication, now we force the logout
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(task -> {
                        // user is now signed out
                        Toast toast = Toast.makeText(getApplicationContext(),
                                R.string.force_relogin_toast, Toast.LENGTH_SHORT);
                        toast.show();
                        startActivity(new Intent(MyCopiesActivity.this, LoginActivity.class));
                        finish();
                    });
        }

        assert user != null;
        Copy copy = new Copy(user.getUid());
        Intent addCopyIntent = new Intent(getApplicationContext(), EditCopyActivity.class);
        addCopyIntent.putExtra(EditCopyActivity.COPY_KEY, copy);
        startActivityForResult(addCopyIntent, REQUEST_ADD_COPY);
    }
    //START Drawer Fragment Classes
    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.inflateMenu();
        setSupportActionBar(toolbar);ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }
    private void setUpDrawer(){
        FragmentNavDrawer drawerFragment= (FragmentNavDrawer) getSupportFragmentManager().findFragmentById(R.id.nav_drwr_fragment);
        drawer= findViewById(R.id.drawer_layout);
    }
    //END Drawer Fragment Classes

    //START SEARCH CLASSES

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_search, menu);
            //getMenuInflater().inflate(R.menu.menu_filters, menu);
            MenuItem searchItem = menu.findItem(R.id.mi_search);
            android.support.v7.widget.SearchView sv=(android.support.v7.widget.SearchView) searchItem.getActionView();
            sv.setIconifiedByDefault(false);
            View fv=findViewById(R.id.filters_frame);
            sv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);
                @Override
                public void onViewDetachedFromWindow(View arg0) {
                    filtersMenuItem.setVisible(false);
                    if(isFiltersView){
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                0,
                                -(fv.getHeight()));
                        animate.setDuration(500);
                        //animate.setFillAfter(false);
                        fv.startAnimation(animate);
                        fv.setVisibility(View.INVISIBLE);
                        isFiltersView=false;
                    }
                    findViewById(R.id.floatingActionButton).setVisibility(View.VISIBLE);

                }


                @Override
                public void onViewAttachedToWindow(View arg0) {

                    filtersMenuItem.setVisible(true);
                    findViewById(R.id.floatingActionButton).setVisibility(View.INVISIBLE);

                }
            });

            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if(isFiltersView){
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                0,
                                -(fv.getHeight()));
                        animate.setDuration(500);
                        //animate.setFillAfter(false);
                        fv.startAnimation(animate);
                        fv.setVisibility(View.INVISIBLE);
                        isFiltersView=false;
                    }

                    mFilterFragment.StartSearchActivity(query);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });

            Button advS= findViewById(R.id.searchButton);
            advS.setOnClickListener(view -> {
                String s=sv.getQuery().toString();

                mFilterFragment.StartSearchActivity(s);

                TranslateAnimation animate = new TranslateAnimation(
                        0,
                        0,
                        0,
                        -(fv.getHeight()));
                animate.setDuration(500);
                //animate.setFillAfter(false);
                fv.startAnimation(animate);
                fv.setVisibility(View.INVISIBLE);
                isFiltersView=false;
            });
            return true;
        }


        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            switch (id) {
                case R.id.btn_advanced_search:
                    View fv=findViewById(R.id.filters_frame);
                    if(!isFiltersView){
                        fv.setVisibility(View.VISIBLE);
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                -(fv.getHeight()),
                                0);
                        animate.setDuration(500);
                        //animate.setFillAfter(true);
                        fv.startAnimation(animate);
                        isFiltersView=true;
                    }
                    else{
                        TranslateAnimation animate = new TranslateAnimation(
                                0,
                                0,
                                0,
                                -(fv.getHeight()));
                        animate.setDuration(500);
                        //animate.setFillAfter(false);
                        fv.startAnimation(animate);
                        fv.setVisibility(View.INVISIBLE);
                        isFiltersView=false;
                    }
                    break;
                default:
                    break;
            }

            return super.onOptionsItemSelected(item);
        }


    //END SEARCH CLASSES

}
