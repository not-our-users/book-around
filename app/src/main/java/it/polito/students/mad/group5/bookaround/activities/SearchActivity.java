package it.polito.students.mad.group5.bookaround.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.models.Book;
import it.polito.students.mad.group5.bookaround.utils.BookQueryBuilder;

public class SearchActivity extends AppCompatActivity {

    /**
     * This class listens for any change in the search fields. Upon update, it will schedule
     * a query to be performed with some delay (to avoid doing one for each keypress).
     * Previous searches are aborted before scheduling a new one.
     */
    private class SearchQueryTextListener implements SearchView.OnQueryTextListener, OnLoadMoreListener {

        private static final int N_SEARCH_LIMIT = 20;

        private BookQueryBuilder queryBuilder = BookQueryBuilder.start().limitTo(N_SEARCH_LIMIT);
        private BookQueryBuilder.BookQueryTask bookQueryTask = null;
        private Handler lookupHandler = null;
        private int pageCounter = 0;

        private String keywords = "";

        @Override
        public boolean onQueryTextSubmit(String query) {
            if(isFiltersView){
                View fv=findViewById(R.id.filters_frame);
                TranslateAnimation animate = new TranslateAnimation(
                        0,
                        0,
                        0,
                        -(fv.getHeight()));
                animate.setDuration(500);
                fv.startAnimation(animate);
                fv.setVisibility(View.INVISIBLE);
                isFiltersView=false;
            }

            if (query.length() == 0) { return false; }

            stop();
            keywords = query;
            updateQuery();
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            if (newText.length() == 0) { return false; }

            stop();
            keywords = newText;
            reschedule();
            return true;
        }

        public void onAdvancedFieldsChanged(String field) {
            stop();
            reschedule();
        }

        private void reschedule() {
            lookupHandler = new Handler(getMainLooper());
            // Update UI after used stopped typing for 500ms
            lookupHandler.postDelayed(this::updateQuery, 500);
        }

        public void stop() {
            if (bookQueryTask != null) { bookQueryTask.cancel(false); }
            bookQueryTask = null;
            if (lookupHandler != null) { lookupHandler.removeCallbacksAndMessages(null); }
            lookupHandler = null;
        }

        /**
         * Build a query with all the fields of interest, then start an AsyncTask that will
         * update our RecyclerView
         */
        private void updateQuery() {
            EditText titleSearch = findViewById(R.id.title_search_edit);
            EditText authorSearch = findViewById(R.id.author_search_edit);
            EditText publisherSearch = findViewById(R.id.publisher_search_edit);

            String title = titleSearch.getText().toString();
            String author = authorSearch.getText().toString();
            String publisher = publisherSearch.getText().toString();

            // Clear current status before resubmitting the task
            queryBuilder = null;
            searchAdapter.clear();
            pageCounter = 0;

            if (keywords.isEmpty() && title.isEmpty() &&
                author.isEmpty() && publisher.isEmpty()
            ) {
                // Just clear recycler if all fields are empty
                return;
            }

            queryBuilder = BookQueryBuilder.start();
            if (keywords.length() > 0) { queryBuilder.byKeywords(keywords); }
            if (title.length() > 0) { queryBuilder.byTitle(title); }
            if (author.length() > 0) { queryBuilder.byAuthor(author); }
            if (publisher.length() > 0) { queryBuilder.byPublisher(publisher); }

            bookQueryTask = queryBuilder.limitTo(N_SEARCH_LIMIT).build();
            bookQueryTask.addOnProgressUpdateListener(books -> searchAdapter.addBooks(books));
            bookQueryTask.executeDefault();
        }

        @Override
        public void onLoadMore() {
            if (queryBuilder != null && (bookQueryTask == null || bookQueryTask.getStatus() == AsyncTask.Status.FINISHED)) {
                // Keep current query but load the next N items
                bookQueryTask = queryBuilder.startAt(++pageCounter * N_SEARCH_LIMIT).limitTo(N_SEARCH_LIMIT).build();
                bookQueryTask.addOnProgressUpdateListener(books -> searchAdapter.addBooks(books));
                bookQueryTask.addOnCompleteListener(books -> searchAdapter.setLoaded());
                bookQueryTask.executeDefault();
            }
            else {
                searchAdapter.setLoaded();
            }
        }
    }

    private static class BookSearchItemHolder extends RecyclerView.ViewHolder {

        private Book currentBook = null;

        private ImageView bookThumbView;
        private TextView bookAuthorView;
        private TextView bookTitleView;

        public BookSearchItemHolder(View itemView) {
            super(itemView);
            bookThumbView = itemView.findViewById(R.id.book_li_thumb_view);
            bookAuthorView = itemView.findViewById(R.id.book_li_author_view);
            bookTitleView = itemView.findViewById(R.id.book_li_title_view);
        }

        public void setData(Book data) {
            if (data.getImageUrl() != null) {
                Picasso.get().load(data.getImageUrl()).into(bookThumbView);
            }
            else {
                Picasso.get().load(android.R.drawable.ic_menu_report_image).into(bookThumbView);
            }
            if (data.getAuthors() != null && data.getAuthors().size() > 0) {
                bookAuthorView.setText(data.getAuthors().get(0));
            }
            bookTitleView.setText(data.getTitle());
            currentBook = data;
        }

        public Book getCurrentBook() { return currentBook; }
    }

    private interface OnLoadMoreListener {
        void onLoadMore();
    }

    private class BookSearchRecyclerAdapter extends RecyclerView.Adapter<BookSearchItemHolder> {

        private List<Book> booksFound = new ArrayList<>();
        private int totalItemCount, lastVisibleItem;
        private int visibleThreshold = 5;
        private boolean isLoading = false;
        private OnLoadMoreListener onLoadMoreListener = null;

        public BookSearchRecyclerAdapter(RecyclerView recyclerView) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dx == 0 && dy == 0) { return; } // Don't load more unless there is some user interaction

                    // Check which item is currently visibile.
                    // Then, if less than visibleThreshold items are yet to be displayed, load more
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        isLoading = true;
                        onLoadMore();
                    }
                }
            });
        }

        public void setOnLoadMoreListener(OnLoadMoreListener listener) {
            onLoadMoreListener = listener;
        }

        private void onLoadMore() {
            if (onLoadMoreListener != null) { onLoadMoreListener.onLoadMore(); }
            else { isLoading = false; }
        }

        public void setLoaded() { isLoading = false; }

        @NonNull
        @Override
        public BookSearchItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.book_search_list_item, parent, false);
            BookSearchItemHolder holder = new BookSearchItemHolder(view);
            view.setOnClickListener(v -> StartShowBookActivity(holder.getCurrentBook()) );
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull BookSearchItemHolder holder, int position) {
            holder.setData(booksFound.get(position));
        }

        @Override
        public int getItemCount() {
            return booksFound.size();
        }

        public void setBookList(List<Book> bookList) {
            booksFound.clear();
            booksFound.addAll(bookList);
            notifyDataSetChanged();
        }

        public void addBooks(Book... books) {
            int count = getItemCount();
            booksFound.addAll(Arrays.asList(books));
            notifyItemRangeInserted(count, books.length);
        }

        public List<Book> getBooksFound() { return booksFound; }

        public void clear() {
            int count = booksFound.size();
            booksFound.clear();
            notifyItemRangeRemoved(0, count);
        }
    }

    /**
     * Used to notify {@link SearchQueryTextListener} of changes within advanced search fields.
     */
    private class AdvancedSearchWatcher implements TextWatcher {

        private View view;

        public AdvancedSearchWatcher(View view) { this.view = view; }

        // Don't care
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        // Don't care
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            searchQueryListener.onAdvancedFieldsChanged(s.toString());
        }
    }

    public static final String KEYWORD_KEY = "Keywords";
    public static final String AUTHOR_KEY = "Author";
    public static final String TITLE_KEY = "Title";
    public static final String PUBLISHER_KEY = "Publisher";

    private BookSearchRecyclerAdapter searchAdapter;
    private SearchQueryTextListener searchQueryListener = new SearchQueryTextListener();

    private static final String SAVED_BOOKS_KEY = "BooksFound";
    private static final String TAG = "SearchActivity";

    private boolean isFiltersView = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_search));
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(v -> finish());

        setupRecyclerView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setSearchListeners();
        loadQueryFromIntent();
    }

    private void loadQueryFromIntent() {

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        searchQueryListener.stop();
        outState.putSerializable(SAVED_BOOKS_KEY, (Serializable)searchAdapter.getBooksFound());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(SAVED_BOOKS_KEY)) {
            List<Book> savedBooks = (List<Book>)savedInstanceState.getSerializable(SAVED_BOOKS_KEY);
            searchAdapter.setBookList(savedBooks);
        }
    }

    private void setSearchListeners() {

    }

    private void setupRecyclerView() {
        RecyclerView bookSearchView = findViewById(R.id.book_search_recycler);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        bookSearchView.setLayoutManager(layoutManager);

        searchAdapter = new BookSearchRecyclerAdapter(bookSearchView);
        searchAdapter.setOnLoadMoreListener(searchQueryListener);
        bookSearchView.setAdapter(searchAdapter);

        bookSearchView.setItemAnimator(new DefaultItemAnimator());
    }

    private void StartShowBookActivity(Book book) {
        Intent bookPageIntent = new Intent(getApplicationContext(), BookDetailsActivity.class);
        bookPageIntent.putExtra("googleApiId", book.getGoogleApiId());
        startActivity(bookPageIntent);
    }

    //START SEARCH CLASSES
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);

        // Apply listeners to advanced fields inside the fragment
        EditText titleSearch = findViewById(R.id.title_search_edit);
        EditText authorSearch = findViewById(R.id.author_search_edit);
        EditText publisherSearch = findViewById(R.id.publisher_search_edit);

        titleSearch.addTextChangedListener(new AdvancedSearchWatcher(titleSearch));
        authorSearch.addTextChangedListener(new AdvancedSearchWatcher(authorSearch));
        publisherSearch.addTextChangedListener(new AdvancedSearchWatcher(publisherSearch));

        // Update values according to intent. Null values are fine.
        Intent intent = getIntent();

        String author = intent.getStringExtra(AUTHOR_KEY);
        String title = intent.getStringExtra(TITLE_KEY);
        String publisher = intent.getStringExtra(PUBLISHER_KEY);
        String query = intent.getStringExtra(KEYWORD_KEY);

        authorSearch.setText(author);
        titleSearch.setText(title);
        publisherSearch.setText(publisher);

        MenuItem searchItem = menu.findItem(R.id.mi_search);
        android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) searchItem.getActionView();
        searchView.setIconifiedByDefault(false);
        View fv = findViewById(R.id.filters_frame);
        searchView.setOnQueryTextListener(searchQueryListener);
        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                filtersMenuItem.setVisible(false);
                if(isFiltersView){
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
            }


            @Override
            public void onViewAttachedToWindow(View arg0) {
                filtersMenuItem.setVisible(true);
            }
        });

        searchView.setQuery(query, true);

        Button advS= findViewById(R.id.searchButton);
        advS.setOnClickListener(view -> {
            searchQueryListener.onAdvancedFieldsChanged(null);
            TranslateAnimation animate = new TranslateAnimation(
                    0,
                    0,
                    0,
                    -(fv.getHeight()));
            animate.setDuration(500);
            //animate.setFillAfter(false);
            fv.startAnimation(animate);
            fv.setVisibility(View.INVISIBLE);
            isFiltersView = false;
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.btn_advanced_search:
                View fv=findViewById(R.id.filters_frame);
                if(!isFiltersView){
                    fv.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -(fv.getHeight()),
                            0);
                    animate.setDuration(500);
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate);
                    isFiltersView=true;
                }
                else{
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;

                }
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //END SEARCH CLASSES
}
