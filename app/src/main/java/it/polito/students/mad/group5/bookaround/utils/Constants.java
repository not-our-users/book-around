package it.polito.students.mad.group5.bookaround.utils;

/**
 * Helper class to hold project-wide constants like database keys.
 *
 * Created by Alessio on 07/05/2018.
 */
public class Constants {

    public static final String DBKEY_USERS = "users";
    public static final String DBKEY_MEMBERS = "members";
    public static final String DBKEY_MESSAGES = "messages";
    public static final String DBKEY_MESSAGES_TIMESTAMP = "timestamp";
    public static final String DBKEY_MESSAGES_SENDER = "senderId";
    public static final String DBKEY_LAST_READ = "last_read";
    public static final String DBKEY_TRANSACTIONS = "transactions";
    public static final String DBKEY_REVIEWS = "reviews";
    public static final String DBKEY_COPIES = "copies";
    public static final String DBKEY_REVIEW_SUBJECT = "subjectId";
    public static final String DBKEY_REVIEW_VOTE = "vote";
    public static final String DBKEY_TRANS_LENDER = "lenderId";
    public static final String DBKEY_TRANS_BORROWER = "borrowerId";
    public static final String DBKEY_COPY_STATUS = "status";
    public static final String DBKEY_REVIEW_TIMESTAMP = "timestamp";
    public static final String DBKEY_TRANS_LENDER_REVIEW = "lenderReviewId";
    public static final String DBKEY_TRANS_BORROWER_REVIEW = "borrowerReviewId";
    public static final String DBKEY_GEO_COPIES_IDX = "copiesPositions";

    public static final int NOTIFY_ID_NEW_MSG = 25;
}
