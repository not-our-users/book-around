package it.polito.students.mad.group5.bookaround.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import it.polito.students.mad.group5.bookaround.R;

public class ChoosePhotoDialogFragment extends DialogFragment {

    public interface ChoosePhotoDialogListener {
        void onCameraDialogClick(DialogFragment dialog);
        void onFileDialogClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    ChoosePhotoDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (ChoosePhotoDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choose_photo_dialog_title)
                .setItems(R.array.choosephotoarray, (dialog, which) -> {
                    // The 'which' argument contains the index position
                    // of the selected item
                    switch (which) {
                        case 0: //First button: choosing to take a new photo
                            mListener.onCameraDialogClick(ChoosePhotoDialogFragment.this);
                            break;
                        case 1:
                            mListener.onFileDialogClick(ChoosePhotoDialogFragment.this);
                            break;
                        case 2:
                        default:
                            break;
                    }
                });
        return builder.create();
    }


}
