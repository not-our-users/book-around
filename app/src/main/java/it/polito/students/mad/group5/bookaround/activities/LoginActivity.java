package it.polito.students.mad.group5.bookaround.activities;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.FirebaseUiException;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;

import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.services.NewMessageNotificationService;

/**
 * This is only a fake launcher to use as an entry point.
 * We do so in order to launch the real activity {@link ShowProfileActivity}
 * with an argument {@link User} object, in foresight for future integration.
 */
public class LoginActivity extends AppCompatActivity {

    private static final int REQUEST_AUTH = 0;
    private static final int REQUEST_EDIT_PROFILE = 1;

    private static final int INTERNET_PERMISSION_CODE = 3;

    private static final String TAG = "LoginActivity";

    // Imho, better if we store this inside string.xml as an untranslatable
    // private static final String USER_SAVE_FILENAME = "userdata.ser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            //The user is already logged, so we can retrieve his data from Firebase and launch the main menu
            Toast.makeText(
                    getApplicationContext(),
                    String.format(getString(R.string.format_welcome_back), user.getDisplayName()),
                    Toast.LENGTH_LONG
            )
                    .show();
            startAppFlow();
        }
        else {
            setContentView(R.layout.activity_login);

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            Button loginButton = findViewById(R.id.loginButton);

            loginButton.setOnClickListener(view -> {
                    // we need to start the Firebase UI Activity
                    doLogin(null);
            });
        }
    }

  @Override
    protected void onStart() {
        super.onStart();

        // Check for storage permission
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Read permission not granted, must ask first
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    INTERNET_PERMISSION_CODE
            );
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case INTERNET_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_storage_denied), Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AUTH) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            // Successfully signed in
            if (resultCode == RESULT_OK) {
                Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.signin_success),
                        Toast.LENGTH_LONG
                )
                .show();
                startAppFlow();
            }
            else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    return;
                }

                FirebaseUiException ex = response.getError();
                if (ex != null && ex.getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.no_internet_connection),
                            Toast.LENGTH_LONG
                    ).show();
                    return;
                }

                // Unknown error
                Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.internal_error),
                        Toast.LENGTH_LONG
                ).show();
                Log.e(TAG, "Sign-in error: ", response.getError());
            }
        }
        else if (requestCode == REQUEST_EDIT_PROFILE) {
            if (resultCode == RESULT_OK) {
                User user = (User)data.getSerializableExtra(getString(R.string.edit_user_key));
                DatabaseReference userRef = FirebaseDatabase.getInstance()
                        .getReference(getString(R.string.DBKEY_USERS))
                        .child(user.getUserId());

                userRef.setValue(user);
                Intent showProfileIntent = new Intent(getApplicationContext(), ShowcaseActivity.class);
                //showProfileIntent.putExtra(getString(R.string.show_user_key), user);
                startActivity(showProfileIntent);
                finish();
            }
            else {
                Toast.makeText(
                        getApplicationContext(),
                        getString(R.string.new_profile_cancelled),
                        Toast.LENGTH_LONG
                )
                .show();
                doLogout(null);
            }
        }
    }

    public void doLogout(View v) {
        //Sign out Method.
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener((task) -> {
                    // user is now signed out
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.logout_message),
                            Toast.LENGTH_LONG
                    )
                    .show();
                });
    }

    //This method invokes the Firebase UI for the login
    public void doLogin(View v) {
        Intent authIntent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(Arrays.asList(
                        new AuthUI.IdpConfig.EmailBuilder().build(),
                        new AuthUI.IdpConfig.GoogleBuilder().build()
                        // new AuthUI.IdpConfig.FacebookBuilder().build(),  DISABLED
                        // new AuthUI.IdpConfig.TwitterBuilder().build()    DISABLED
                ))
                .setTheme(R.style.AppTheme)
                .build();
        startActivityForResult(authIntent, REQUEST_AUTH);
    }

    private void createNotificationChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            CharSequence name = "Name";
            String description = "Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("default",name,importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);

        }
    }

    private void startAppFlow() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser == null) { return; }
        createNotificationChannel();
        NewMessageNotificationService.enqueueWork(getApplicationContext(),NewMessageNotificationService.class,1,
                new Intent(this, NewMessageNotificationService.class));

        ProgressDialog dialog = ProgressDialog.show(this, "", getString(R.string.wait_dialog), true);
        DatabaseReference userRef = FirebaseDatabase.getInstance()
            .getReference(getString(R.string.DBKEY_USERS))
            .child(firebaseUser.getUid());

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                dialog.dismiss();
                if (dataSnapshot.exists()) {
                    User u = dataSnapshot.getValue(User.class);
                    Intent showProfileIntent = new Intent(getApplicationContext(), ShowcaseActivity.class);
                    //showProfileIntent.putExtra(getString(R.string.show_user_key), u);
                    startActivity(showProfileIntent);
                    finish();
                }
                else {
                    Intent editProfileIntent = new Intent(getApplicationContext(), EditProfileActivity.class);
                    User u = new User(firebaseUser.getUid());
                    editProfileIntent.putExtra(getString(R.string.edit_user_key), u);
                    startActivityForResult(editProfileIntent, REQUEST_EDIT_PROFILE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dialog.dismiss();
            }
        });
    }
}
