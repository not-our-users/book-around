package it.polito.students.mad.group5.bookaround.activities

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import at.blogc.android.views.ExpandableTextView
import com.squareup.picasso.Picasso
import io.reactivex.rxkotlin.subscribeBy
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.models.Copy
import it.polito.students.mad.group5.bookaround.models.Review
import it.polito.students.mad.group5.bookaround.models.User
import it.polito.students.mad.group5.bookaround.utils.downloadUserReviews
import it.polito.students.mad.group5.bookaround.utils.toLiveData

class ShowReviewsActivity : AppCompatActivity() {

    private var isFiltersView = false
    private lateinit var ratedUser:User
    private lateinit var sharedCopy:Copy
    private lateinit var userId : String
    private var isOwner = false

    private lateinit var status: TextView
    private lateinit var adapter: ReviewListAdapter

    companion object {
        const val USERID_KEY: String = "USERID"
    }

    class ReviewListViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var review: Review
        private val bookPictureView: ImageView = view.findViewById(R.id.reviewed_thumb_img)
        private val authorNameView: TextView = view.findViewById(R.id.review_author)
        private val bookTitleView: TextView = view.findViewById(R.id.book_shared)
        private val ratingBarView: RatingBar = view.findViewById(R.id.given_rating)
        private val reviewTextView: ExpandableTextView = view.findViewById(R.id.review_text)

        fun getReview(): Review { return review }

        fun setReview(review: Review) {
            this.review = review

            authorNameView.text = review.author?.name

            bookPictureView.setImageResource(R.mipmap.not_found)
            review.transaction?.copy?.bookData?.subscribeBy(
                    onSuccess = {
                        bookTitleView.text = it.title
                        it.imageUrl?.let {
                            Picasso.get().load(it).centerInside().fit().into(bookPictureView)
                        }
                        bookPictureView.setOnClickListener({v->
                            val intent = Intent(view.context, ShowCopyActivity::class.java)
                            intent.putExtra(ShowCopyActivity.COPY_KEY, review.transaction?.copy)
                            view.context.startActivity(intent)
                        })
                    },
                    onError = Throwable::printStackTrace
            )

            ratingBarView.rating = review.vote.toFloat()
            reviewTextView.text = review.text
            reviewTextView.setOnClickListener {
                reviewTextView.toggle()
            }
        }
    }

    class ReviewListAdapter(val activity: Activity) : RecyclerView.Adapter<ReviewListViewHolder>() {
        private val reviewMap = HashMap<String, Review>()
        private val reviewList = ArrayList<Review>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewListViewHolder {
            val view = activity.layoutInflater.inflate(R.layout.review_list_item, parent, false)
            return ReviewListViewHolder(view)
        }

        override fun getItemCount(): Int {
            return reviewList.size
        }

        override fun onBindViewHolder(holder: ReviewListViewHolder, position: Int) {
            holder.setReview(reviewList[position])
        }

        fun setReviews(reviews: List<Review>) {
            reviews.reversed().forEachIndexed { i, review ->
                if (reviewMap.containsKey(review.reviewId)) {
                    reviewMap[review.reviewId] = review
                    reviewList[i] = review
                    notifyItemChanged(i)
                }
                else {
                    reviewMap[review.reviewId] = review
                    reviewList.add(i, review)
                    notifyItemInserted(i)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_reviews)

        userId = intent?.getStringExtra(USERID_KEY) ?: run { finish(); return }

        setUpToolbar()
        title = getString(R.string.my_reviews)
        setupRecyclerView()

        downloadUserReviews(userId).toLiveData().observe({ lifecycle }) {
            it?.value?.let { adapter.setReviews(it) }
            it?.exception?.printStackTrace()
        }
    }

    private fun setupRecyclerView(){
        val recyclerView= findViewById<RecyclerView>(R.id.recycler_reviews_list)
        adapter = ReviewListAdapter(this)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.itemAnimator = DefaultItemAnimator()
    }

    private fun setUpToolbar() {
        val toolbar = findViewById<android.support.v7.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.apply {
            setNavigationIcon(R.drawable.arrow_left)
            navigationIcon!!.setColorFilter(resources.getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP)
            setNavigationOnClickListener { v -> finish() }
        }
    }
}
