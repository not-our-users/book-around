package it.polito.students.mad.group5.bookaround.utils

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.wrapInOptional

/**
 * Created by Alessio on 23/05/2018.
 */

/**
 * <p>LiveData cannot convert Reactive streams that can produce errors.
 * To deal with it, we use a wrapper that intercepts and stores exceptions.</p>
 *
 * <p>This must be the last step in a chain of building a stream, so don't try to
 * combine Observables based on this class...</p>
 */
class ExceptionWrapper<out T> (val value: T? = null, val exception: Throwable? = null)

/**
 * Extension function to more clearly use the {@link FirebaseUtils#wrapInOptional} function
 */
fun <T> Observable<T>.toOptionalObservable() : Observable<Optional<T>> {
    return wrapInOptional(this)
}

/**
 * <p>Produces a LiveData instance for this Observable. To do so, it must first be converted
 * into a {@link Flowable}, using a given {@link BackpressureStrategy} (by default, only the latest
 * emitted value is retained).</p>
 *
 * <p>The resulting Flowable is then wrapped around an {@link ExceptionWrapper} to deal with
 * possible exceptions.</p>
 */
fun <T> Observable<T>.toLiveData(backpressureStrategy: BackpressureStrategy = BackpressureStrategy.LATEST)
        : LiveData<ExceptionWrapper<T>> {

    return LiveDataReactiveStreams.fromPublisher(
            this.toFlowable(backpressureStrategy)
            .map {ExceptionWrapper(it, null)}
            .onErrorReturn {ExceptionWrapper(null, it)}
    )
}