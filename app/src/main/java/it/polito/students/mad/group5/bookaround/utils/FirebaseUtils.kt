package it.polito.students.mad.group5.bookaround.utils

import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function3
import it.polito.students.mad.group5.bookaround.models.*
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadByIds
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadUser
import java.security.Security
import java.util.*

/**
 * Created by Alessio on 25/05/2018.
 */

/**
 * Shortcut function to guarantee that DatabaseErrors can always be converted to exceptions
 */
fun DatabaseError?.toException() : Exception { return this?.toException() ?: Exception("Unkown exception") }

/**
 * Convenience function to download raw entities that can be identified by a certain id  within
 * a group.
 *
 * @param <T> The base class type
 * @param groupRef The DatabaseReference for the entity group. E.g. all users fall within the 'users' reference.
 * @param id The entity's id
 * @param base The base class to deserialize the entity.
 *
 * @return An {@link Observable} for the desired entity.
 */
fun <T> downloadById(groupRef : DatabaseReference, id : String, base : Class<T>) : Observable<T> {
    val itemRef = groupRef.child(id)
    return Observable.create({emitter ->
        itemRef.addValueEventListener {
            onDataChange {
                if (emitter.isDisposed) {
                    emitter.onComplete()
                    itemRef.removeEventListener(this)
                    return@onDataChange
                }

                val item = it?.getValue(base)
                item?.let { emitter.onNext(it)} ?:
                emitter.onError(NullPointerException("Invalid item data for id $id"))
            }
            onCancelled {
                itemRef.removeEventListener(this)
                if (!emitter.isDisposed) { emitter.onError(it.toException()) }
            }
        }
    })
}

/**
 * Downloads an instance of {@link Copy}, given its id
 */
fun downloadCopy (copyId : String) : Observable<Copy> {
    return downloadById(getCopiesRef(), copyId, Copy::class.java)
}

/**
 * Downloads raw data for {@link Transaction} entities. Returned entities will not have
 * linked values (such as lener, borrower, copy)
 */
fun downloadTransactionData(transactionId : String) : Observable<LendingTransaction> {
    return downloadById(getTransactionRef(), transactionId, LendingTransaction::class.java)
}

/**
 * Downloads complete data for {@link Transaction} entities.
 */
fun downloadTransaction(transactionId: String) : Observable<LendingTransaction> {
    return downloadTransactionData(transactionId)
            .flatMap {
                val borrowerObservable: Observable<Optional<User>> = downloadUser(it.borrowerId).toOptionalObservable()
                val lenderObservable: Observable<Optional<User>> = downloadUser(it.lenderId).toOptionalObservable()
                val copyObservable: Observable<Optional<Copy>> = downloadCopy(it.copyId).toOptionalObservable()

                Observable.combineLatest(borrowerObservable, lenderObservable, copyObservable,
                    Function3<Optional<User>, Optional<User>, Optional<Copy>, LendingTransaction>{ borrower, lender, copy ->
                        it.borrower = borrower.get()
                        it.lender = lender.get()
                        it.copy = copy.get()
                        it
                    }
                )
            }
}

/**
 * Downloads all transactions in which a user participates, either as lender or as borrower.
 *
 * @param userId the user id whose transactions to download.
 * @paramt asLender whether to download transactions in which the user is lender or is borrower.
 */
fun downloadUserTransactions(userId: String, asLender: Boolean) : Observable<List<LendingTransaction>> {
    val subKey = if (asLender) Constants.DBKEY_TRANS_LENDER else Constants.DBKEY_TRANS_BORROWER
    val userTransactionsQuery = getTransactionRef()
            .orderByChild(subKey)
            .equalTo(userId)

    return Observable.create{ emitter: ObservableEmitter<List<String>> ->
        userTransactionsQuery.addValueEventListener {
            onDataChange {
                if (emitter.isDisposed) {
                    emitter.onComplete()
                    userTransactionsQuery.removeEventListener(this)
                    return@onDataChange
                }

                val transactionKeys = it?.children?.map { it.key } ?: Collections.emptyList()
                emitter.onNext(transactionKeys)
            }

            onCancelled {
                userTransactionsQuery.removeEventListener(this)
                if (!emitter.isDisposed) { emitter.onError(it.toException()) }
            }
        }
    }
    .flatMap{ FirebaseUtils.downloadByIds(it, ::downloadTransaction) }
}

/**
 * A simple {@link Comparator} that sorts transactions according to their issue date.
 */
class ByIssueDateTransactionComparator : Comparator<LendingTransaction> {
    override fun compare(t1: LendingTransaction, t2: LendingTransaction): Int {
        return t1.dateIssued.compareTo(t2.dateIssued)
    }
}

typealias OpenClosedTransactionMap = Map<Boolean, List<LendingTransaction>>
typealias TransactionListCombiner = BiFunction<List<LendingTransaction>, List<LendingTransaction>, OpenClosedTransactionMap>

/**
 * <p>Downloads all transactions the user is involved into, and additionally performs some extra operations</p>
 *
 * <ol>
 *     <li>Transactions are listed and sorted by a provided criterion. By default, they are sorted by issue date</li>
 *     <li>The ordered transactions are split in active (OPEN, APPROVED, ACTIVE) and closed (CLOSED, REJECTED)</li>
 *     <li>The two split lists are returned as a Map<Boolean, List> where the true element includes the open transactions,
 *     whereas the false element include the closed ones</li>
 * </ol>
 *
 * @param userId: the id of the user whose transactions to download
 * @param comparator: an instance of {@link Comparator} to sort transactions. By default it sorts by {@link LendingTransaction#dateIssued}
 * @return An Observable<Map<Boolean, List<LendingTransaction>>> built using the aforementioned criterion
 */
fun downloadOpenClosedTransactions(userId: String, comparator: Comparator<LendingTransaction> = ByIssueDateTransactionComparator()) : Observable<OpenClosedTransactionMap> {
    val asLenderObservable = downloadUserTransactions(userId, true)
    val asBorrowerObservable = downloadUserTransactions(userId, false)

    return Observable.combineLatest(asLenderObservable, asBorrowerObservable, TransactionListCombiner {
        asLender, asBorrower ->
        (asLender + asBorrower).sortedWith(comparator)
                .groupBy { it.status !in arrayOf(TransactionStatus.REJECTED, TransactionStatus.CLOSED) }
    })
}

/**
 * Downloads raw data for {@link Review} entities. Returned entities will not have
 * linked values (such as subject, author and transaction)
 */
fun downloadReviewData(userId: String, reviewId: String) : Observable<Review> {
    return downloadById(getReviewReference().child(userId), reviewId, Review::class.java)
}

/**
 * Downloads complete data for {@link Review} entities
 */
fun downloadReview(userId: String, reviewId: String) : Observable<Review> {
    return downloadReviewData(userId, reviewId)
            .flatMap {
                val authorObservable: Observable<Optional<User>> = downloadUser(it.authorId).toOptionalObservable()
                val subjectObservable: Observable<Optional<User>> = downloadUser(it.subjectId).toOptionalObservable()
                val transactionObservable: Observable<Optional<LendingTransaction>> = downloadTransaction(it.transactionId).toOptionalObservable()

                Observable.combineLatest(authorObservable, subjectObservable, transactionObservable,
                    Function3<Optional<User>, Optional<User>, Optional<LendingTransaction>, Review> { author, subject, transaction ->
                        it.author = author.get()
                        it.subject = subject.get()
                        it.transaction = transaction.get()
                        it
                    }
                )
            }
}

fun downloadUserReviews(userId: String, limitToLast: Int = -1) : Observable<List<Review>> {
    val userReviewQuery = getReviewReference().child(userId)
            .orderByChild(Constants.DBKEY_REVIEW_TIMESTAMP)
            .let { if (limitToLast > 0) it.limitToLast(limitToLast) else it }

    return Observable.create { emitter : ObservableEmitter<List<String>> ->
        userReviewQuery.addValueEventListener {
            onDataChange {
                if (emitter.isDisposed) {
                    emitter.onComplete()
                    userReviewQuery.removeEventListener(this)
                    return@onDataChange
                }

                val reviews = it?.children?.mapNotNull { it.key } ?: Collections.emptyList()
                emitter.onNext(reviews)
            }
            onCancelled {
                userReviewQuery.removeEventListener(this)
                if (!emitter.isDisposed) { emitter.onError(it.toException()) }
            }
        }
    }
    .flatMap {
        FirebaseUtils.downloadByIds(
                it,
                { id -> downloadReview(userId, id) }
        )
    }
}

/**
 * Return a given user's score, based on the average of all of its reviews
 */
fun downloadUserScore(userId : String) : Observable<Float> {
    val reviewQuery = getReviewReference().child(userId)

    return Observable.create { emitter ->
        reviewQuery.addValueEventListener {
            onDataChange {
                if (emitter.isDisposed) {
                    emitter.onComplete()
                    reviewQuery.removeEventListener(this)
                    return@onDataChange
                }

                val scoreList = it?.children?.mapNotNull { it.child(Constants.DBKEY_REVIEW_VOTE).getValue(Integer::class.java)?.toFloat() }

                scoreList?.let {
                    emitter.onNext(if (it.isNotEmpty()) (Math.round(scoreList.average() * 10.0) / 10.0).toFloat() else 0.0f)
                }
                ?: emitter.onError(Exception("Invalid score data"))
            }
            onCancelled {
                reviewQuery.removeEventListener(this)
                if (!emitter.isDisposed) { emitter.onError(it.toException()) }
            }
        }
    }
}

fun downloadNewestNearbyCopies(lat: Double, lon: Double, lastN: Int = 8) : Observable<List<Copy>> {
    val uid = FirebaseAuth.getInstance().uid ?: throw SecurityException("User must be authenticated!")
    val reference = FirebaseDatabase.getInstance()
                                    .reference
                                    .child(Constants.DBKEY_GEO_COPIES_IDX);

    val geoFire = GeoFire(reference)
    val geoQuery = geoFire.queryAtLocation(GeoLocation(lat, lon), 20.0)

    return Observable.create {emitter: ObservableEmitter<List<String>> ->
        geoQuery.addGeoQueryEventListener( object: GeoQueryEventListener {
            val ids = ArrayList<String>()

            override fun onGeoQueryReady() {
                geoQuery.removeGeoQueryEventListener(this)

                if (emitter.isDisposed) {
                    emitter.onComplete()
                    return
                }
                else {
                    emitter.onNext(ids)
                }
            }

            override fun onKeyEntered(key: String?, location: GeoLocation?) {
                if (emitter.isDisposed) {
                    geoQuery.removeGeoQueryEventListener(this)
                    emitter.onComplete()
                    return
                }
                key?.let { ids.add(it) }
            }

            override fun onKeyMoved(key: String?, location: GeoLocation?) { /* Don't care */ }

            override fun onKeyExited(key: String?) { /* Don't care */ }

            override fun onGeoQueryError(error: DatabaseError?) {
                if (!emitter.isDisposed) { emitter.onError(error.toException()) }
            }
        })
    }
    .flatMap { downloadByIds(it, { downloadCopy(it) }) }
    .map { it.filter { it.ownerId != uid }.takeLast(lastN) }
}

fun getTransactionRef() : DatabaseReference {
    return FirebaseDatabase.getInstance()
            .reference
            .child(Constants.DBKEY_TRANSACTIONS)
}

fun getCopiesRef() : DatabaseReference {
    return FirebaseDatabase.getInstance()
            .reference
            .child(Constants.DBKEY_COPIES)
}

fun getReviewReference() : DatabaseReference {
    return FirebaseDatabase.getInstance()
            .reference
            .child(Constants.DBKEY_REVIEWS)
}