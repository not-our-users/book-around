package it.polito.students.mad.group5.bookaround.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.models.Chat;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;

/**
 * Shows the list of chats the current user belongs to. The chat list is implemented by
 * {@link DialogsList}.
 */
public class InboxActivity extends AppCompatActivity {

    private static final String TAG = "InboxActivity";
    private Disposable chatDisposable = null;
    private DrawerLayout drawer;

    private ImageLoader base64ImageLoader = (imageView, profilePicB64) -> {
        if (profilePicB64 == null) {
            Picasso.get().load(android.R.drawable.ic_menu_report_image).into(imageView);
        }
        else {
            ByteArrayInputStream byteArray = new ByteArrayInputStream(Base64.decodeBase64(profilePicB64.getBytes()));
            Bitmap avatarBitmap = BitmapFactory.decodeStream(byteArray);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(avatarBitmap);
        }
    };

    private DialogsListAdapter<Chat> dialogsListAdapter = new DialogsListAdapter<>(base64ImageLoader);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        setUpDrawer();
        setTitle(R.string.inbox);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setUpToolbar();
        setupChatListeners();
    }


    //START Drawer Fragment Classes
    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.inflateMenu();
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }
    private void setUpDrawer(){
        drawer = findViewById(R.id.drawer_layout);
    }
    //END Drawer Fragment Classes

    private void setupChatListeners() {
        DialogsList dialogsListView = findViewById(R.id.dialogsList);
        dialogsListView.setAdapter(dialogsListAdapter);
        dialogsListAdapter.setOnDialogClickListener(chat -> startChat(chat.getId()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        startListeningForDialogs();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (chatDisposable != null) {
            chatDisposable.dispose();
            chatDisposable = null;
        }
    }

    private void startListeningForDialogs() {
        chatDisposable =  FirebaseUtils.downloadUserChats()
            .doOnNext(chats -> {
                // Filter out all empty chats
                List<Chat> nonEmptyChats = new ArrayList<>(chats.size());
                for (Chat chat : chats) {
                    if (chat.getLastMessage() != null) {
                        nonEmptyChats.add(chat);
                    }
                }
                dialogsListAdapter.setItems(nonEmptyChats);
            })
            .doOnError(this::onError)
            .subscribe();
    }

    private void startChat(String chatId) {
        Intent startChatIntent = new Intent(this, ChatActivity.class);
        startChatIntent.putExtra(ChatActivity.CHAT_ID_KEY, chatId);
        startActivity(startChatIntent);
    }

    private void onError(Throwable t) {
        Toast.makeText(getApplicationContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        Log.e(TAG, t.getMessage());
        finish();
    }
}
