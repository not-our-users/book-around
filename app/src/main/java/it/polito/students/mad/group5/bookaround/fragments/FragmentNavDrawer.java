package it.polito.students.mad.group5.bookaround.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.activities.InboxActivity;
import it.polito.students.mad.group5.bookaround.activities.LoginActivity;
import it.polito.students.mad.group5.bookaround.activities.MyCopiesActivity;
import it.polito.students.mad.group5.bookaround.activities.ShowProfileActivity;
import it.polito.students.mad.group5.bookaround.activities.ShowcaseActivity;
import it.polito.students.mad.group5.bookaround.activities.TransactionHistoryActivity;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;
import it.polito.students.mad.group5.bookaround.utils.ReactiveExtensionsKt;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNavDrawer extends android.support.v4.app.Fragment {

    private final static String TAG = "FragmentNavDrawer";

    private Disposable userDisposable = null;
    private HashMap<Integer, NavigationDrawerViewHolder> holders = new HashMap<>();

    private static class NavigationDrawerItem {
        private int titleId;
        private int imageId;

        public int getTitleId() {
            return titleId;
        }

        public void setTitleId(int titleId) {
            this.titleId = titleId;
        }

        public int getImageId() {
            return imageId;
        }

        public void setImageId(int imageId) {
            this.imageId = imageId;
        }

        public static List<NavigationDrawerItem> getData(){
            List<NavigationDrawerItem> dataList= new ArrayList<>();

            int[] imageIds=getImages();
            int[] titles= getTitles();
            for(int i=0; i<titles.length; i++){
                NavigationDrawerItem navItem=new NavigationDrawerItem();
                navItem.setTitleId(titles[i]);
                navItem.setImageId(imageIds[i]);
                dataList.add(navItem);
            }
            return dataList;
        }

        private static int[] getImages(){
            return new int[]{
                   // TODO:Add the browse book activity when it will be ready
                    R.mipmap.account,
                    R.drawable.home,
                    R.mipmap.book_open_variant,
                    R.mipmap.messages,
                    R.mipmap.transaction,
                    R.mipmap.logout
            };
        }
        private static int[] getTitles(){
            return new int[]{
                    R.string.drw_li_view_profile , R.string.homepage, R.string.drw_li_my_copies,   R.string.inbox, R.string.my_trades, R.string.drw_li_log_out
            };
        }
    }

    private class NavigationDrawerViewHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView title;
        TextView badge;
        View itemView;

        public NavigationDrawerViewHolder(View itemView){
            super(itemView);
            this.itemView = itemView;
            imgIcon = itemView.findViewById(R.id.menuImgIcon);
            title = itemView.findViewById(R.id.menuItemTitle);
            badge = itemView.findViewById(R.id.badge);
        }

        private final static int BASE_PADDING = 3;
        private final static int EXTRA_PADDING = 6;

        public void setBadge(int value) {
            if (value == 0) { badge.setVisibility(View.GONE); }
            else {
                badge.setVisibility(View.VISIBLE);
                badge.setText(value < 100 ? String.valueOf(value) : "99+");

                float density = getResources().getDisplayMetrics().density;
                if (value < 10) {
                    badge.setPadding(
                            (int) (EXTRA_PADDING * density),
                            (int) (BASE_PADDING * density),
                            (int) (EXTRA_PADDING * density),
                            (int) (BASE_PADDING * density));
                }
                else {
                    badge.setPadding(
                            (int) (BASE_PADDING * density),
                            (int) (BASE_PADDING * density),
                            (int) (BASE_PADDING * density),
                            (int) (BASE_PADDING * density));
                }
            }
        }
    }

    private class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerViewHolder>{

        private List<NavigationDrawerItem> mDataList;
        private LayoutInflater inflater;
        private Context context;

        public NavigationDrawerAdapter(Context context, List<NavigationDrawerItem> data){
            this.context=context;
            inflater=LayoutInflater.from(context);
            this.mDataList=data;
        }

        @NonNull
        @Override
        public NavigationDrawerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= inflater.inflate(R.layout.menu_list_item, parent, false);
            return new NavigationDrawerViewHolder(view);
        }

        public void onBindViewHolder(@NonNull NavigationDrawerViewHolder holder, int position){
            NavigationDrawerItem current=mDataList.get(position);
            holder.imgIcon.setImageResource(current.getImageId());
            holder.title.setText(context.getString(current.getTitleId()));
            Activity activity= (Activity)context;
            String title=activity.getTitle().toString();
            holder.itemView.setOnClickListener((v)-> {
                switch(current.getTitleId()){
                    case R.string.drw_li_view_profile:
                        if(title.equals(context.getString(R.string.title_activity_show_profile))){
                            CloseDrawer();
                            break;
                        }
                        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                        assert firebaseUser != null;
                        User u = new User(firebaseUser.getUid());
                        DatabaseReference userRef = FirebaseDatabase.getInstance()
                                .getReference(context.getString(R.string.DBKEY_USERS))
                                .child(u.getUserId());
                        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    User u = dataSnapshot.getValue(User.class);
                                    Intent showProfileIntent = new Intent(context, ShowProfileActivity.class);
                                    showProfileIntent.putExtra(context.getString(R.string.show_user_key), u);
                                    context.startActivity(showProfileIntent);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                        CloseDrawer();
                        break;

                    case R.string.drw_li_my_copies:
                        if(title.equals(context.getString(R.string.title_activity_my_copies))){
                            CloseDrawer();
                            break;
                        }
                        Intent viewCopiesIntent = new Intent(context, MyCopiesActivity.class);
                        context.startActivity(viewCopiesIntent);
                        CloseDrawer();
                        break;

               /* case R.string.drw_li_browse_books:
                    if(title.equals(context.getString(R.string.title_activity_showcase))){
                        CloseDrawer();
                        break;
                    }
                    Intent viewShowcaseIntent = new Intent(context,ShowcaseActivity.class);
                    context.startActivity(viewShowcaseIntent);
                    CloseDrawer();
                    break;*/ //TODO:Create the browse book activity
                    case R.string.drw_li_log_out:
                        FirebaseAuth.getInstance().signOut();
                        Intent loginIntent = new Intent(context, LoginActivity.class);
                        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);   // Pop all previous activities and start fresh
                        context.startActivity(loginIntent);
                        CloseDrawer();
                        break;

                    case R.string.my_trades:
                        if (title.equals(context.getString(R.string.my_trades))) {
                            CloseDrawer();
                            break;
                        }
                        Intent showTransactionHistory = new Intent(context, TransactionHistoryActivity.class);
                        context.startActivity(showTransactionHistory);
                        CloseDrawer();
                        break;

                    case R.string.inbox:
                        if(title.equals(context.getString(R.string.inbox))){
                            CloseDrawer();
                            break;
                        }
                        Intent inboxIntent = new Intent(context, InboxActivity.class);
                        context.startActivity(inboxIntent);
                        CloseDrawer();
                        break;
                    case R.string.homepage:
                        if(title.equals(context.getString(R.string.title_activity_showcase))){
                            CloseDrawer();
                            break;
                        }
                        Intent homeIntent = new Intent(context, ShowcaseActivity.class);
                        context.startActivity(homeIntent);
                        CloseDrawer();
                        break;


                }
            });

            holders.put(current.getTitleId(), holder);
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }

        private void CloseDrawer(){
            DrawerLayout dl= ((Activity)context).findViewById(R.id.drawer_layout);
            dl.closeDrawer(GravityCompat.START);
        }
    }

    public FragmentNavDrawer() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_nav_drawer, container, false);
        setUpRecyclerView(view);

        ReactiveExtensionsKt.toLiveData(FirebaseUtils.downloadCumulativeUnreadMessagesCount(), BackpressureStrategy.LATEST)
                .observe(this, wrapper -> {
                    if (wrapper != null && wrapper.getValue() != null && holders.containsKey(R.string.inbox)) {
                        holders.get(R.string.inbox).setBadge(wrapper.getValue());
                    }
                    if (wrapper != null && wrapper.getException() != null) { wrapper.getException().printStackTrace(); }
                });

        return view;
    }

    private void loadUserPicture(View v) {
        ImageView profilePicView = v.findViewById(R.id.drwr_profile_pic);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            userDisposable = FirebaseUtils.downloadUser(firebaseUser.getUid())
                .subscribe(
                    user -> {
                        Bitmap userProfilePicBitmap = user.getProfilePicBitmap();
                        if (userProfilePicBitmap != null) {
                            profilePicView.setImageBitmap(userProfilePicBitmap);
                        }
                    },
                    this::onError
                );
        }
    }

    private void setUpRecyclerView(View view){
        RecyclerView recyclerView= view.findViewById(R.id.drawerList);
        NavigationDrawerAdapter adapter= new NavigationDrawerAdapter(getActivity(), NavigationDrawerItem.getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getView() != null) {
            loadUserPicture(getView());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (userDisposable != null) {
            userDisposable.dispose();
            userDisposable = null;
        }
    }

    public void onError(Throwable t) {
        Toast.makeText(getContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        Log.e(TAG, t.getMessage());
        if (getActivity() != null) { getActivity().finish(); }
    }
}
