package it.polito.students.mad.group5.bookaround.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.BuildConfig;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.ChoosePhotoDialogFragment;
import it.polito.students.mad.group5.bookaround.models.Copy;
import it.polito.students.mad.group5.bookaround.utils.BookQueryBuilder;
import it.polito.students.mad.group5.bookaround.utils.Constants;
import it.polito.students.mad.group5.bookaround.utils.ImageUtils;

public class EditCopyActivity extends AppCompatActivity
                              implements ChoosePhotoDialogFragment.ChoosePhotoDialogListener
{

    private static final String TAG = "EditCopyActivity";

    private static final String NEW_BMPS_KEY = "selectedImage";

    private Copy copy;
    private static final int PICK_FILE_CODE = 2;
    private static final int CAMERA_REQUEST = 3;
    private static final int BARCODE_PERMISSION_REQUEST = 4;
    private static final int CAMERA_DIALOG_PERMISSION_REQUEST = 6;
    private static final int GALLERY_DIALOG_PERMISSION_REQUEST = 7;
    private static final int REQUEST_PICK_LOCATION = 5;
    public static final String COPY_KEY = "copy";
    private static final String AUTHORITY = BuildConfig.APPLICATION_ID+".fileprovider";

    private HashMap<Integer,Uri> selectedImagesUri = new HashMap<>();
    private String mCurrentPhotoPath=null; //String for saving the next photo taken from the camera
    private Double latitude = 0.0, longitude = 0.0;

    private Disposable saveDisposable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_copy);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(NEW_BMPS_KEY)) {
                selectedImagesUri = (HashMap<Integer, Uri>) savedInstanceState.getSerializable(NEW_BMPS_KEY);
            }
        }

        Intent callee = getIntent();

        if (callee != null) {
            copy = (Copy) callee.getSerializableExtra(COPY_KEY);
            List<String> copyUrl = copy.getPictureList();
            if(selectedImagesUri.size()==0){
                for (String url:copyUrl){
                    selectedImagesUri.put(selectedImagesUri.size(),Uri.parse(url));
                }
            }
            updateFields();
        } else {
            finish();
            return;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.title_activity_edit_copy));
        setSupportActionBar(toolbar);

        /*----------------------------------------------------------*/
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            finish();
            return;
        }

        findViewById(R.id.floatingActionButton).setOnClickListener(this::saveCopy);

        Spinner conditionSpinner = findViewById(R.id.edit_condition);
        conditionSpinner.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return Copy.Condition.values().length;
            }

            @Override
            public Object getItem(int i) {
                return Copy.Condition.values()[i];
            }

            @Override
            public long getItemId(int i) {
                return 0;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                if (view == null) {
                    view = getLayoutInflater()
                            .inflate(android.R.layout.simple_list_item_1, viewGroup, false);
                }
                Copy.Condition item = (Copy.Condition) getItem(i);
                ((TextView) view).setText(item.getLocalizedVersion(getApplicationContext()));
                return view;
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onStart() {
        super.onStart();
        // Put focus on root layout to prevent keyboard from autofocusing the first input
        View root = findViewById(R.id.edit_copy_root);
        root.requestFocus();

        EditText descText = findViewById(R.id.edit_details);
        descText.setOnTouchListener((v, event) -> {
            if (v.getId() == R.id.edit_details) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });

        KeyboardVisibilityEvent.setEventListener(
                this,
                isOpen -> findViewById(R.id.floatingActionButton).setVisibility(isOpen?View.GONE:View.VISIBLE));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (saveDisposable != null) { saveDisposable.dispose(); }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(NEW_BMPS_KEY,selectedImagesUri);
    }

    public void saveCopy(View v) {
        // Mandatory fields are checked
        if(!validate()) { return; }

        TextView isbnView = findViewById(R.id.edit_isbn);
        TextView townView = findViewById(R.id.edit_town_view);
        TextView detailsView = findViewById(R.id.edit_details);
        Spinner conditionSpinner = findViewById(R.id.edit_condition);

        String isbn_string = isbnView.getText().toString();
        copy.setIsbn(isbn_string);
        copy.setCondition((Copy.Condition) conditionSpinner.getSelectedItem());
        copy.setTown(townView.getText().toString());
        copy.setLatitude(latitude);
        copy.setLongitude(longitude);
        copy.setStatus(Copy.Status.NOT_BORROWED);
        copy.setDetails(detailsView.getText().toString());
        copy.getPictures().clear();


        ProgressDialog uploadDialog = new ProgressDialog(this);
        uploadDialog.setMessage(getString(R.string.wait_dialog));
        uploadDialog.show();

        if (saveDisposable != null) { saveDisposable.dispose(); }
        saveDisposable = BookQueryBuilder.start().byIsbn(isbn_string).build().getSingle().subscribe(
                b -> {
                    uploadDialog.hide();
                    copy.setGoogleApiId(b.getGoogleApiId());
                    String copyId = UploadCopyIntoFirebase(copy);

                    DatabaseReference geofireRef = FirebaseDatabase.getInstance()
                                                            .getReference()
                                                            .child(getString(R.string.DBKEY_POSITIONS))
                                                            .child(b.getGoogleApiId());

                    DatabaseReference globalCopiesGeoRef = FirebaseDatabase.getInstance()
                                                                .getReference()
                                                                .child(Constants.DBKEY_GEO_COPIES_IDX);

                    GeoFire geoFire = new GeoFire(geofireRef);
                    geoFire.setLocation(
                        copyId,
                        new GeoLocation(copy.getLatitude(), copy.getLongitude()),
                        (key, error) -> {
                            if (error != null) {
                                Log.d(TAG, error.getMessage());
                                Toast.makeText(getApplicationContext(), getString(R.string.copy_upload_error), Toast.LENGTH_LONG).show();
                            }
                        }
                    );

                    GeoFire copiesGeoFire = new GeoFire(globalCopiesGeoRef);
                    copiesGeoFire.setLocation(
                            copyId,
                            new GeoLocation(copy.getLatitude(), copy.getLongitude()),
                            (key, error) -> {
                                if (error != null) {
                                    Log.d(TAG, error.getMessage());
                                    Toast.makeText(getApplicationContext(), getString(R.string.copy_upload_error), Toast.LENGTH_LONG).show();
                                }
                            }
                    );

                    Intent savedCopyIntent = new Intent();
                    savedCopyIntent.putExtra(COPY_KEY, copyId);
                    setResult(Activity.RESULT_OK, savedCopyIntent);
                    finish();
                },
                e -> {
                    Log.e(TAG, e.getLocalizedMessage());
                    uploadDialog.hide();
                    isbnView.setError(getString(R.string.check_isbn));
                    Toast.makeText(this, getString(R.string.isbn_error), Toast.LENGTH_LONG).show();
                }
        );
    }

    private boolean validate() {
        boolean valid = true;
        TextView[] mandatory = {
                findViewById(R.id.edit_isbn),
                findViewById(R.id.edit_town_view),
                findViewById(R.id.edit_details)
        };
        for (TextView view : mandatory) {
            if (view.getText().length() == 0) {
                view.setError(getString(R.string.required));
                valid = false;
            }
        }
        return valid;
    }

    public void StartImageDialog(View src){
        DialogFragment dialog = new ChoosePhotoDialogFragment();
        dialog.show(getFragmentManager (),"ChoosePhotoDialogFragment");
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onCameraDialogClick(DialogFragment dialog) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED
            )  {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        CAMERA_DIALOG_PERMISSION_REQUEST
                );
                return;
            }

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getApplicationContext(),getString(R.string.free_some_space_toast),Toast.LENGTH_SHORT).show();
                Log.d("onCameraDialogClick", "File creation for taking a picture failed");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(  getApplicationContext(),
                                                            AUTHORITY,
                                                            photoFile);

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
    }

    @Override
    public void onFileDialogClick(DialogFragment dialog) {
        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED
        )  {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    GALLERY_DIALOG_PERMISSION_REQUEST
            );
            return;
        }

        Intent chooseFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
        chooseFile.setType("image/*");
        chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(Intent.createChooser(chooseFile, getString(R.string.choose_file_from_gallery)), PICK_FILE_CODE);
    }


    /**
     * Starts {@link BarcodeReaderActivity} to fetch a barcode for the user.
     * The returned ISBN barcode will be used to update the ISBN view.
     */
    public void StartBarcodeScanner(View src) {
        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CAMERA},
                    BARCODE_PERMISSION_REQUEST
            );
        }
        else {
            Intent barcodeIntent = new Intent(getApplicationContext(), BarcodeReaderActivity.class);
            startActivityForResult(barcodeIntent, BARCODE_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_DIALOG_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                   onCameraDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_camera_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;
            case GALLERY_DIALOG_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    onFileDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_storage_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;
            case BARCODE_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    StartBarcodeScanner(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_camera_denied_barcode), Toast.LENGTH_LONG).show();
                }
            }
            break;
        }
    }


    /**
     * Starts uploading data for the copy asynchronously to the server.
     *
     * First pictures are uploaded into the Firebase Storage. Then after all pictures
     * are uploaded (and their URLs stored) the copy is uploaded too.
     *
     * @param copy The copy to upload
     * @return The remote copyId
     */
    @SuppressLint("CheckResult")
    private String UploadCopyIntoFirebase(Copy copy) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            throw new SecurityException("User must be authenticated to push a new copy");
        }
        DatabaseReference copiesRef = FirebaseDatabase.getInstance().getReference(getString(R.string.DBKEY_COPIES));
        DatabaseReference copyRef;
        // If the copy was already existing, it is pushed to the DB
        if(copy.getCopyId() == null)
            copyRef = copiesRef.push();
        // Otherwise, the existing copy is modified
        else {
            copyRef = copiesRef.child(copy.getCopyId());
            copyRef.setValue(copy);
        }
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();



        UploadTask lastTask = null;
        int i=0;
        for (Uri mUri : selectedImagesUri.values()) {
            String pictureId = "" + System.currentTimeMillis() + (i++);
            if (mUri.toString().matches("https?://firebasestorage.googleapis.com/.*")) {
                copy.getPictures().put("test", mUri.toString());

            } else {
                StorageReference bookCopyRef = storageRef.child("/users/" + user.getUid() + "/images/" + copyRef.getKey()+ "/" + pictureId);
                bookCopyRef.delete().addOnFailureListener(
                        Throwable::printStackTrace
                );
                UploadTask uploadTask = bookCopyRef.putFile(mUri);
                if (lastTask != null) {
                    lastTask.continueWithTask(oldTask -> uploadTask);
                }
                lastTask = uploadTask;
                uploadTask.addOnFailureListener(exception -> {
                    Toast.makeText(
                            getApplicationContext(),
                            getString(R.string.data_upload_failed),
                            Toast.LENGTH_LONG
                    ).show();
                    exception.printStackTrace();
                }).addOnSuccessListener(taskSnapshot -> {
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    if (downloadUrl != null) {
                        copy.getPictures().put(pictureId, downloadUrl.toString());
                    }
                });
            }
        }

        if (lastTask != null) {
            lastTask.addOnCompleteListener(task ->
                BookQueryBuilder.start().byIsbn(copy.getIsbn()).build().getSingle().subscribe(
                    b -> {
                        copy.setGoogleApiId(b.getGoogleApiId());
                        UpdateIndices(copy, copyRef);
                    },
                    Throwable::printStackTrace
                )
            );
        }
        else {
            BookQueryBuilder.start().byIsbn(copy.getIsbn()).build().getSingle().subscribe(
                    b -> {
                        copy.setGoogleApiId(b.getGoogleApiId());
                        UpdateIndices(copy, copyRef);
                    },
                    Throwable::printStackTrace
            );
        }
        return copyRef.getKey();
    }


    // Is this even needed? We can do queries to lookup copies by owner or title.
    private void UpdateIndices(Copy copy, DatabaseReference copyRef) {
        copy.setCopyId(copyRef.getKey());
        copyRef.setValue(copy);

        DatabaseReference copiesByOwnerRef = FirebaseDatabase.getInstance()
                .getReference(getString(R.string.DBKEY_COPIESBYOWNERID))
                .child(copy.getOwnerId())
                .child(copy.getCopyId());
        copiesByOwnerRef.setValue(true);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_CODE && resultCode == RESULT_OK) {
            if (data == null) {
                //Signal Error
                return;
            }
            LinearLayout ll = findViewById(R.id.pictures_gallery);
            ImageView bookCover = findViewById(R.id.book_cover);
            // The default image is hidden
            bookCover.setVisibility(View.INVISIBLE);
            //Multiple photo are chosen
            ClipData clipData = data.getClipData();
            if (clipData != null) {
                for (int i = 0; i < clipData.getItemCount(); i++) {

                    ClipData.Item item = clipData.getItemAt(i);
                    Uri imageUri = item.getUri();
                    // The selected image is added to the gallery
                    View v = getLayoutInflater().inflate(R.layout.edit_book_picture,ll,false);
                    ImageView bookView = v.findViewById(R.id.edit_book_cover_picture);
                    bookView.setImageURI(imageUri);
                    ImageButton deleteButton = v.findViewById(R.id.delete_picture_button);
                    deleteButton.setContentDescription(Integer.toString(selectedImagesUri.size()));
                    ll.addView(v);
                    selectedImagesUri.put(selectedImagesUri.size(),imageUri);
                }
            }
            //Only one photo is chosen
            else if (data.getData() != null) {
                // Pick the loaded image's URI and update view and user values
                Uri imageUri = data.getData();
                View v = getLayoutInflater().inflate(R.layout.edit_book_picture,ll,false);
                ImageView bookView = v.findViewById(R.id.edit_book_cover_picture);
                bookView.setImageURI(imageUri);
                ImageButton deleteButton = v.findViewById(R.id.delete_picture_button);
                deleteButton.setContentDescription(Integer.toString(selectedImagesUri.size()));
                ll.addView(v);
                selectedImagesUri.put(selectedImagesUri.size(),imageUri);
            }
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {

            LinearLayout ll = findViewById(R.id.pictures_gallery);
            ImageView bookCover = findViewById(R.id.book_cover);
            bookCover.setVisibility(View.INVISIBLE);
            Uri mUri =  Uri.fromFile(new File(mCurrentPhotoPath));
            Bitmap photo = null;
            try {
                photo = ImageUtils.handleSamplingAndRotationBitmap(getApplicationContext(),mUri);
                photo = ImageUtils.compressAndConvert(photo,Bitmap.CompressFormat.JPEG,60);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photo != null) {
                View v = getLayoutInflater().inflate(R.layout.edit_book_picture,ll,false);
                ImageView bookView = v.findViewById(R.id.edit_book_cover_picture);

                bookView.setImageBitmap(photo);
                ImageButton deleteButton = v.findViewById(R.id.delete_picture_button);
                deleteButton.setContentDescription(Integer.toString(selectedImagesUri.size()));
                ll.addView(v);
                selectedImagesUri.put(selectedImagesUri.size(),ImageUtils.getImageUri(getApplicationContext(),photo));
            }
        }
        if (requestCode == BARCODE_PERMISSION_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null && data.getExtras().containsKey(getString(R.string.read_barcode_key))) {
                String isbn = data.getExtras().getString(getString(R.string.read_barcode_key));
                TextView isbnView = findViewById(R.id.edit_isbn);
                isbnView.setText(isbn);
            }
        }
        if (requestCode == REQUEST_PICK_LOCATION && resultCode == RESULT_OK ) {
            String town = "";
            Double lng = 0.0, lat = 0.0;
            if (data.hasExtra(LocationPickerActivity.ADDRESS_TOWN_KEY)) { town = data.getStringExtra(LocationPickerActivity.ADDRESS_TOWN_KEY); }
            if (data.hasExtra(LocationPickerActivity.ADDRESS_LON_KEY)) { lng = data.getDoubleExtra(LocationPickerActivity.ADDRESS_LON_KEY, 0.0); }
            if (data.hasExtra(LocationPickerActivity.ADDRESS_LAT_KEY)) { lat = data.getDoubleExtra(LocationPickerActivity.ADDRESS_LAT_KEY, 0.0); }

            TextView townView = findViewById(R.id.edit_town_view);
            townView.setText(town);

            latitude = lat;
            longitude = lng;
        }
    }

    private void updateFields() {
        if(selectedImagesUri != null) {
            LinearLayout ll = findViewById(R.id.pictures_gallery);
            if(selectedImagesUri.size() > 0) {
                ImageView bookCover = findViewById(R.id.book_cover);
                bookCover.setVisibility(View.INVISIBLE);
                // The pictures gallery is populated
                for(Integer mKey:selectedImagesUri.keySet()){
                    Uri mUri = selectedImagesUri.get(mKey);
                    View v = getLayoutInflater().inflate(R.layout.edit_book_picture,ll,false);
                    ImageView imageView = v.findViewById(R.id.edit_book_cover_picture);
                    Picasso.get().load(mUri).placeholder(R.mipmap.not_found).into(imageView);
                    ImageButton deleteButton = v.findViewById(R.id.delete_picture_button);
                    deleteButton.setContentDescription(getString(R.string.delete)+mKey);
                    ll.addView(v);

                }
            }
        }
        TextView isbnView = findViewById(R.id.edit_isbn);
        TextView townView = findViewById(R.id.edit_town_view);
        TextView detailsView = findViewById(R.id.edit_details);

        isbnView.setText(copy.getIsbn());
        townView.setText(copy.getTown());
        detailsView.setText(copy.getDetails());
        latitude = copy.getLatitude();
        longitude = copy.getLongitude();
    }

    // This method is called when the "delete picture" button is pressed
    public void deletePicture(View deleteButton) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.confirm_delete_title)
                .setMessage(R.string.confirm_delete_picture_text)
                .setIcon(R.drawable.ic_delete_black)
                .setPositiveButton(R.string.yes, (dialog, button) -> {
                    RelativeLayout p = (RelativeLayout) deleteButton.getParent();
                    LinearLayout pp = (LinearLayout) p.getParent();
                    if(deleteButton.getContentDescription().toString().startsWith(getString(R.string.delete))) {
                        Integer mInt = Integer.parseInt(deleteButton.getContentDescription().toString().substring(getString(R.string.delete).length()));
                        for(Map.Entry<Integer,Uri> e:selectedImagesUri.entrySet()) {
                            if(mInt.equals(e.getKey())) {
                               selectedImagesUri.remove(e.getKey());
                               break;
                            }
                        }
                    }
                    else
                        selectedImagesUri.remove(Integer.parseInt(deleteButton.getContentDescription().toString()));
                    pp.removeView(p);
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    public void pickLocation(View v) {
        startActivityForResult(new Intent(this, LocationPickerActivity.class), REQUEST_PICK_LOCATION);
    }

}
