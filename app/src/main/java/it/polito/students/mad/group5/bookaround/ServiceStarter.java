package it.polito.students.mad.group5.bookaround;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import it.polito.students.mad.group5.bookaround.services.NewMessageNotificationService;
// This class is used to start the NewMessageNotificationService when the phone boots up
public class ServiceStarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, NewMessageNotificationService.class);
        NewMessageNotificationService.enqueueWork(context,NewMessageNotificationService.class,1,intent);
    }
}
