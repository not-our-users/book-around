package it.polito.students.mad.group5.bookaround.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.google.firebase.auth.FirebaseAuth
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.fragments.PageTransactionFragment
import it.polito.students.mad.group5.bookaround.utils.downloadOpenClosedTransactions

class TransactionHistoryActivity : AppCompatActivity() {

    inner class PageTransactionAdapter ( fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager){
        private val pageCount = 2
        private val titles = arrayOf(R.string.active_transaction,R.string.archived_transaction)
        private val transactionsObservable = downloadOpenClosedTransactions(FirebaseAuth.getInstance().uid!!)

        override fun getCount(): Int {
            return pageCount
        }

        override fun getItem(position: Int): Fragment {
            return PageTransactionFragment.newInstance(position, transactionsObservable)
        }

        override fun getPageTitle(position: Int): CharSequence? {
           return getString(titles[position])
        }
    }

    private lateinit var pagerAdapter:PageTransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_history)

        //Set the pager adapter for the viewpager
        pagerAdapter = PageTransactionAdapter(supportFragmentManager)
        val viewPager = findViewById<ViewPager>(R.id.view_pager)
        viewPager.adapter = pagerAdapter

        //Give the tab layout to the ViewPager
        val tabLayout = findViewById<TabLayout>(R.id.transaction_tabs)
        tabLayout.setupWithViewPager(viewPager)

        //Setting the drawer
        val drawerFragment = supportFragmentManager.findFragmentById(R.id.nav_drwr_fragment)
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        title = getString(R.string.my_trades)

        //setting the toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        val toggle = ActionBarDrawerToggle(this,drawer,toolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()
    }
}
