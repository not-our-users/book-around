package it.polito.students.mad.group5.bookaround.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.polito.students.mad.group5.bookaround.BuildConfig;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.ChoosePhotoDialogFragment;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.utils.IImageCompressTaskListener;
import it.polito.students.mad.group5.bookaround.utils.ImageCompressTask;
import it.polito.students.mad.group5.bookaround.utils.ImageUtils;

import static it.polito.students.mad.group5.bookaround.utils.ImageUtils.getImageUri;
import static it.polito.students.mad.group5.bookaround.utils.ImageUtils.getRealPathFromURI;

public class EditProfileActivity extends AppCompatActivity
                                 implements ChoosePhotoDialogFragment.ChoosePhotoDialogListener
{

    private static final String NEW_BMP_KEY = "new_bitmap";
    private static final String CURRENT_PHOTO_KEY = "mCurrentPhotoPath";
    private static final int PICK_FILE_CODE=2;
    private static final int CAMERA_REQUEST=3;
    private static final int REQUEST_PICK_LOCATION = 4;
    private static final int CAMERA_DIALOG_PERMISSION_REQUEST = 6;
    private static final int GALLERY_DIALOG_PERMISSION_REQUEST = 7;
    private static final String AUTHORITY = BuildConfig.APPLICATION_ID+".fileprovider";

    private User user;

    private Bitmap newUserBitmap = null;
    private String mCurrentPhotoPath = null; //String for saving the next photo taken from the camera
   
    private Double latitude = 0.0, longitude = 0.0;

    /**
     * The callee must provide the original user to modify, otherwise the activity
     * will simply quit.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.profile_title));
        setSupportActionBar(toolbar);

        findViewById(R.id.floatingActionButton).setOnClickListener(this::saveUser);

        Intent callee = getIntent();
        if (callee != null) {
            user = (User)callee.getSerializableExtra(getString(R.string.edit_user_key));
            updateFields();
        }
        else { finish(); }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onStart() {
        super.onStart();
        // Put focus on root layout to prevent keyboard from autofocusing the first input
        View root = findViewById(R.id.edit_profile_root);
        root.requestFocus();

        EditText bioText = findViewById(R.id.edit_bio_view);
        bioText.setOnTouchListener((v, event) -> {
            if (v.getId() == R.id.edit_bio_view) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
            }
            return false;
        });
        KeyboardVisibilityEvent.setEventListener(
                this,
                isOpen -> findViewById(R.id.floatingActionButton).setVisibility(isOpen?View.GONE:View.VISIBLE));
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_CODE && resultCode == RESULT_OK) {
            if (data == null) {
                //Signal Error
                return;
            }
            try {
                // Pick the loaded image's URI and update view and user values
                Uri selectedImage = data.getData();
                if (selectedImage == null) { return; }

                newUserBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                // Update view
                ImageView imgView = findViewById(R.id.profile_pic);
                imgView.setImageBitmap(newUserBitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){

            // Update view
            ImageView imgView = findViewById(R.id.profile_pic);
            Uri mUri =  Uri.fromFile(new File(mCurrentPhotoPath));
            Bitmap photo = null;
            try {
                photo = ImageUtils.handleSamplingAndRotationBitmap(getApplicationContext(),mUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(photo==null){
                Log.wtf("Bitmap null","Something went very very wrong");
                return;
            }
            newUserBitmap = ImageUtils.compressAndConvert(photo, Bitmap.CompressFormat.JPEG,75);
            imgView.setImageBitmap(newUserBitmap);

        }
        if (requestCode == REQUEST_PICK_LOCATION && resultCode == RESULT_OK ) {
            String town = "";
            Double lat = 0.0, lng = 0.0;
            if (data.hasExtra(LocationPickerActivity.ADDRESS_TOWN_KEY)) { town = data.getStringExtra(LocationPickerActivity.ADDRESS_TOWN_KEY); }
            if (data.hasExtra(LocationPickerActivity.ADDRESS_LON_KEY)) { lng = data.getDoubleExtra(LocationPickerActivity.ADDRESS_LON_KEY, 0.0); }
            if (data.hasExtra(LocationPickerActivity.ADDRESS_LAT_KEY)) { lat = data.getDoubleExtra(LocationPickerActivity.ADDRESS_LAT_KEY, 0.0); }

            TextView townView = findViewById(R.id.edit_town_view);
            townView.setText(town);

            latitude = lat;
            longitude = lng;
        }
    }

    private void updateFields() {
        ImageView picView = findViewById(R.id.profile_pic);
        Bitmap profilePic = user.getProfilePicBitmap();
        if (profilePic != null) { picView.setImageBitmap(profilePic); }

        // Mandatory fields
        TextView nameView = findViewById(R.id.edit_name_view);
        TextView emailView = findViewById(R.id.edit_email_view);
        TextView bioView = findViewById(R.id.edit_bio_view);
        TextView townView = findViewById(R.id.edit_town_view);

        nameView.setText(user.getName());
        emailView.setText(user.getEmail());
        bioView.setText(user.getBio());
        townView.setText(user.getTown());

        // Optional fields
        TextView twitterView = findViewById(R.id.edit_twitter_view);
        TextView facebookView = findViewById(R.id.edit_facebook_view);
        TextView gplusView = findViewById(R.id.edit_gplus_view);
        TextView homePhoneView = findViewById(R.id.edit_home_phone_view);
        TextView officePhoneView = findViewById(R.id.edit_office_phone_view);
        TextView mobilePhoneView = findViewById(R.id.edit_mobile_phone_view);
        TextView telegramView = findViewById(R.id.edit_telegram_view);

        twitterView.setText(user.getContacts().get("Twitter"));
        facebookView.setText(user.getContacts().get("Facebook"));
        gplusView.setText(user.getContacts().get("GPlus"));
        homePhoneView.setText(user.getContacts().get("Home"));
        officePhoneView.setText(user.getContacts().get("Office"));
        mobilePhoneView.setText(user.getContacts().get("Mobile"));
        telegramView.setText(user.getContacts().get("Telegram"));

        latitude = user.getLatitude();
        longitude = user.getLongitude();
    }

    /**
     * This is called when pressing the Save button. It will update the underlying
     * {@link User} instance, save it on internal storage and return it to the calling activity.
     *
     * @param v The view which triggered this callback.
     */
    public void saveUser(View v) {

        if (!validate()) { return; }

        TextView nameView = findViewById(R.id.edit_name_view);
        TextView emailView = findViewById(R.id.edit_email_view);
        TextView bioView = findViewById(R.id.edit_bio_view);
        TextView townView = findViewById(R.id.edit_town_view);
        TextView twitterView = findViewById(R.id.edit_twitter_view);
        TextView facebookView = findViewById(R.id.edit_facebook_view);
        TextView gplusView = findViewById(R.id.edit_gplus_view);
        TextView homePhoneView = findViewById(R.id.edit_home_phone_view);
        TextView officePhoneView = findViewById(R.id.edit_office_phone_view);
        TextView mobilePhoneView = findViewById(R.id.edit_mobile_phone_view);
        TextView telegramView = findViewById(R.id.edit_telegram_view);

        user.setName(nameView.getText().toString());
        user.setEmail(emailView.getText().toString());
        user.setBio(bioView.getText().toString());
        user.setTown(townView.getText().toString());

        user.addContact("Twitter", twitterView.getText().toString());
        user.addContact("Facebook", facebookView.getText().toString());
        user.addContact("GPlus", gplusView.getText().toString());
        user.addContact("Home", homePhoneView.getText().toString());
        user.addContact("Office", officePhoneView.getText().toString());
        user.addContact("Mobile", mobilePhoneView.getText().toString());
        user.addContact("Telegram", telegramView.getText().toString());

        user.setLatitude(latitude);
        user.setLongitude(longitude);

        //Starting the asyncTask for saving the data into firebase and compressing the user profile pic
        SaveUserIntoFirebase save = new SaveUserIntoFirebase(user,newUserBitmap);
        save.execute();

        Intent savedUserIntent = new Intent();
        savedUserIntent.putExtra(getString(R.string.edit_user_key), user);
        setResult(Activity.RESULT_OK, savedUserIntent);
        finish();
    }

    /**
     * Async Task that manages the compression of the profile pic
     * and the upload on the firebase server afterwards
     */
    @SuppressLint("StaticFieldLeak")
    class SaveUserIntoFirebase extends AsyncTask<Void, Void, Void> {
        private User userToSave;
        private Bitmap userProfileBitmap;

        SaveUserIntoFirebase(User user, Bitmap userBitmap){
            userToSave=user;
            userProfileBitmap=userBitmap;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            //If there is a new user profile pic we need to compress it
            if(userProfileBitmap!=null) {
                // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                Uri tempUri = getImageUri(getApplicationContext(), userProfileBitmap);

                //image compress task callback
                IImageCompressTaskListener iImageCompressTaskListener = new IImageCompressTaskListener() {
                    @Override
                    public void onComplete(List<File> compressed) {
                        File file = compressed.get(0);

                        Log.d("ImageCompressor", "New photo size ==> " + file.length()); //log new file size.

                        userToSave.setProfilePicBitmap(BitmapFactory.decodeFile(file.getAbsolutePath()));

                        Log.d("Upload","Compression finished, starting upload into Firebase");
                        //Now we can upload everything into the database
                        UploadIntoFirebase(userToSave);
                    }

                    @Override
                    public void onError(Throwable error) {
                        //very unlikely, but it might happen on a device with extremely low storage.
                        Log.wtf("ImageCompressor", "Error occurred", error);
                    }
                };

                //Starting the actual compression task
                ImageCompressTask ict = new ImageCompressTask(getApplicationContext(),
                        getRealPathFromURI(getApplicationContext(), tempUri),
                        iImageCompressTaskListener, 300, 300);
                ict.Compress();
            }

            Log.d("Upload","No image to compress, we can just upload");
            //Now we can upload everything into the database
            UploadIntoFirebase(userToSave);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

    private void UploadIntoFirebase(User mUser){
        DatabaseReference userRef = FirebaseDatabase.getInstance()
                .getReference(getString(R.string.DBKEY_USERS))
                .child(mUser.getUserId());

        FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();

        if(loggedUser != null) {
            if (mUser.getUserId().equals(loggedUser.getUid())) {
                Log.d("Upload","Uploading into Firebase");
                userRef.setValue(mUser);

            } else {
                //Something went wrong with the authentication, now we force the logout
                Log.d("UploadIntoFirebase","User must be authenticated to show its copies!");

                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(task -> {
                            // user is now signed out
                            Toast.makeText(getApplicationContext(),
                                    R.string.force_relogin_toast, Toast.LENGTH_SHORT)
                                .show();
                            startActivity(new Intent(EditProfileActivity.this, LoginActivity.class));
                            finish();
                        });
            }
        }
    }


    private boolean validate() {
        boolean valid = true;

        TextView mandatory[] = {
            findViewById(R.id.edit_name_view),
            findViewById(R.id.edit_email_view),
            findViewById(R.id.edit_bio_view),
            findViewById(R.id.edit_town_view),
        };

        for (TextView view : mandatory) {
            if (view.getText().length() == 0) {
                view.setError(getString(R.string.required));
                valid = false;
            }
        }

        // If the email address is not valid, an error is displayed
       if(!Patterns.EMAIL_ADDRESS.matcher(mandatory[1].getText()).matches()) {
            mandatory[1].setError(getString(R.string.invalid_email));
            valid = false;
       }

        return valid;
    }


    public void StartImageDialog(View src){
        //Starting the fragment to make the user choose where to pick the photo
        DialogFragment dialog = new ChoosePhotoDialogFragment();
        dialog.show(getFragmentManager (),"ChoosePhotoDialogFragment");
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onCameraDialogClick(DialogFragment dialog) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                    PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED
            )  {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        CAMERA_DIALOG_PERMISSION_REQUEST
                );
                return;
            }

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getApplicationContext(), R.string.free_some_space_toast,Toast.LENGTH_SHORT)
                     .show();
                Log.d("onCameraDialogClick", "File creation for taking a picture failed");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(  getApplicationContext(),
                        AUTHORITY,
                        photoFile);

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        }
    }

    @Override
    public void onFileDialogClick(DialogFragment dialog) {
        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED
        )  {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    GALLERY_DIALOG_PERMISSION_REQUEST
            );
            return;
        }
        Intent chooseFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
        chooseFile.setType("image/*");
        startActivityForResult(Intent.createChooser(chooseFile , "Select Picture"), PICK_FILE_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_DIALOG_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    onCameraDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_camera_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;
            case GALLERY_DIALOG_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    onFileDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_storage_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;
        }
    }

    /**
     * When rotating, the new bitmap (if present) must be preserved.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (newUserBitmap != null) {
            // outState.putParcelable(NEW_BMP_KEY, newUserBitmap); //DON'T DO THIS AT HOME
            outState.putParcelable(NEW_BMP_KEY,getImageUri(getApplicationContext(),newUserBitmap));
        }
        if (mCurrentPhotoPath != null) {
            outState.putString(CURRENT_PHOTO_KEY, mCurrentPhotoPath);
        }
    }

    /**
     * Restore the new bitmap if there was any
     */
    @Override
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        if (inState.containsKey(NEW_BMP_KEY)) {
            Uri imageUri = inState.getParcelable(NEW_BMP_KEY);
            try {
                newUserBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ImageView imgView = findViewById(R.id.profile_pic);
            imgView.setImageBitmap(newUserBitmap);
        }
        if (inState.containsKey(CURRENT_PHOTO_KEY)) {
            mCurrentPhotoPath = inState.getString(CURRENT_PHOTO_KEY);
        }
    }


    public void pickLocation(View v) {
        startActivityForResult(new Intent(this, LocationPickerActivity.class), REQUEST_PICK_LOCATION);
    }


}
