package it.polito.students.mad.group5.bookaround;

import android.annotation.SuppressLint;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.database.FirebaseDatabase;

import it.polito.students.mad.group5.bookaround.utils.BookQueryBuilder;

import org.apache.commons.codec.binary.Hex;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Controls the application's lifecycle
 *
 * Created by Alessio on 14/04/2018.
 */
public class BookaroundApplication extends MultiDexApplication {

    // API Key for Google Play Books
    public static final String API_KEY = "AIzaSyBi2RBXjtf4Vm5vB6ZmowAKanRdbByXcTc";

    // Used for Google Play Books authentication
    private static String packageName = null;
    private static String sha1Fingerprint = null;

    @Override
    public void onCreate() {
        super.onCreate();
        // Enable persistence when launching the app.
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        BookQueryBuilder.initBookCache(getApplicationContext());
        initFingerprint();
    }

    // See https://stackoverflow.com/questions/39543105/youtube-api-key
    // Also https://stackoverflow.com/questions/9293019/get-certificate-fingerprint-from-android-app/
    private void initFingerprint() {
        PackageManager pm = getPackageManager();
        packageName = getPackageName();

        try {
            // The security issue SHOULDN'T be a problem after API 19 (Kitkat 4.4)
            // https://stackoverflow.com/questions/39192844/android-studio-warning-when-using-packagemanager-get-signatures
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            Signature[] signatures = packageInfo.signatures;
            byte[] cert = signatures[0].toByteArray();
            InputStream input = new ByteArrayInputStream(cert);
            CertificateFactory cf  = CertificateFactory.getInstance("X509");
            X509Certificate c = (X509Certificate) cf.generateCertificate(input);
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(c.getEncoded());
            sha1Fingerprint = new String(Hex.encodeHex(publicKey));
        } catch (PackageManager.NameNotFoundException | CertificateException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String getStaticPackageName() { return packageName; }
    public static String getSha1Fingerprint() { return sha1Fingerprint; }
}
