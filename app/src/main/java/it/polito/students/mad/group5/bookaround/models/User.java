package it.polito.students.mad.group5.bookaround.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.google.firebase.database.Exclude;
import com.stfalcon.chatkit.commons.models.IUser;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Alessio on 16/03/2018.
 */

public class User implements IUser, Serializable {

    // Mandatory fields
    private String userId;
    private String name;
    private String email;
    private String bio;
    private String profilePicB64 = null;
    private String town;
    private double latitude;
    private double longitude;
    private float stars;

    // This is the List of contacts
    private HashMap<String,String> contacts = new HashMap<>();

    public User() { }

    public User(String userId) {
        this.userId = userId;
    }

    public User(String userId, String name, String email, String bio, String town, double latitude, double longitude, int stars) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.bio = bio;
        this.town=town;
        this.stars=stars;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUserId() { return userId; }

    @Override
    @Exclude
    public String getId() {
        return getUserId();
    }

    public String getName() {
        return name;
    }

    @Override
    @Exclude
    public String getAvatar() {
        return getProfilePicB64();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public float getStars() {
        return stars;
    }

    public void setStars(float stars) {
        this.stars = stars;
    }

    public String getProfilePicB64() { return profilePicB64; }
    public void setProfilePicB64(String profilePic) { this.profilePicB64 = profilePic; }

    @Exclude
    public Bitmap getProfilePicBitmap() {
        if (profilePicB64 == null) { return null; }
        else {
            ByteArrayInputStream byteArray = new ByteArrayInputStream(Base64.decodeBase64(profilePicB64.getBytes()));
            return BitmapFactory.decodeStream(byteArray);
        }
    }

    @Exclude
    public void setProfilePicBitmap(Bitmap pic) {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        pic.compress(Bitmap.CompressFormat.JPEG, 100, byteArray); //No need to compress here
        this.profilePicB64 = new String(Base64.encodeBase64(byteArray.toByteArray()));
    }
    
    public  String getTown() { return town; }

    public void setTown(String town) { this.town = town; }

    public HashMap<String,String> getContacts() { return contacts; }

    public void addContact(String type, String value) {
        contacts.put(type, value);
    }

}
