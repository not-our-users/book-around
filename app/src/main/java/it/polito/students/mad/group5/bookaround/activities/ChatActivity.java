package it.polito.students.mad.group5.bookaround.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.ViewHolder;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import it.polito.students.mad.group5.bookaround.BuildConfig;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.ChoosePhotoDialogFragment;
import it.polito.students.mad.group5.bookaround.models.Message;
import it.polito.students.mad.group5.bookaround.utils.Constants;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;
import it.polito.students.mad.group5.bookaround.utils.ImageUtils;

import static it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadMessages;
import static it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadUser;
import static it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.updateReceivedTimestamp;

/**
 * Shows a chat session between two users, in real time. Just pass the chatId in the intent
 * Please be sure the user is part of this chat! Otherwise it will just throw a generic error.
 */
public class ChatActivity extends AppCompatActivity
        implements ChoosePhotoDialogFragment.ChoosePhotoDialogListener {

    private static final int PICK_FILE_CODE = 2;
    private static final int CAMERA_REQUEST = 3;
    private static final int CAMERA_DIALOG_PERMISSION_REQUEST = 4;
    private static final int GALLERY_DIALOG_PERMISSION_REQUEST = 5;
    private static final String AUTHORITY = BuildConfig.APPLICATION_ID+".fileprovider";

    private static final int PAGE_SIZE = 25;
    private static final String TAG = "ChatActivity";

    public static final String CHAT_ID_KEY = "chatID";

    private String currentUserId;
    private String chatId;
    private Long earliestTimestamp = null;
    private MessagesListAdapter<Message> messagesListAdapter = null;

    private Observable<List<Message>> messageListObservable = null;
    private Disposable pageDisposable = null;
    private Disposable messageListDisposable = null;
    private Disposable otherUserDisposable = null;

    private Uri mNextPhotoUri = null;
    private String mCurrentPhotoPath = null;

    private ImageLoader imageLoader = (imageView, image) -> {
        if (image == null) {
            Picasso.get().load(android.R.drawable.ic_menu_report_image).into(imageView);
        } else {
            if (image.matches("https?://.*")) {
                Picasso .get()
                        .load(image)
                        .transform(this.cropPosterTransformation)
                        .placeholder(R.mipmap.not_found)
                        .into(imageView);
            } else {
                ByteArrayInputStream byteArray = new ByteArrayInputStream(Base64.decodeBase64(image.getBytes()));
                Bitmap avatarBitmap = BitmapFactory.decodeStream(byteArray);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setImageBitmap(avatarBitmap);
            }
        }
    };

    private MessagesListAdapter.OnLoadMoreListener onLoadMore = (page, totalCount) -> {
        if (earliestTimestamp == null) {
            return;
        }
        if (pageDisposable != null) {
            return;
        } // Only allow to download one page at a time

        // Use earliest timestamp - 1 to avoid including the earliest message.
        // Might cause issues if two messages have the exact same timestamp within a chat... although this is quite unlikely
        pageDisposable = FirebaseUtils.downloadMessages(chatId, -1, PAGE_SIZE, earliestTimestamp - 1)    // Download a new page
                .subscribe(
                        messages -> {
                            if (messages.size() > PAGE_SIZE) {
                                Log.e(TAG, "Messages were more than expected, something went wrong. Expected " +
                                        PAGE_SIZE + " but was " + messages.size());

                                // Clean up spurious messages
                                for (Iterator<Message> it = messages.iterator(); it.hasNext(); ) {
                                    Message msg = it.next();
                                    if (String.valueOf(earliestTimestamp).compareTo(msg.getTimestamp()) < 0) {
                                        it.remove();
                                    }
                                }
                            }
                            if (!messages.isEmpty()) {
                                updateEarliestTimestamp(messages.get(0));
                                messagesListAdapter.addToEnd(messages, true);
                            }

                            pageDisposable.dispose();
                            pageDisposable = null;
                        },
                        this::onError
                );
    };

    private Transformation cropPosterTransformation = new Transformation() {
        private int targetWidth = 0;

        @Override public Bitmap transform(Bitmap source) {
            targetWidth = (int)(
                    getResources().getInteger(R.integer.item_image_message_target_width) *
                    getResources().getDisplayMetrics().density
            );
            if (source.getWidth() >= targetWidth) {
                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }
            else {
                return source;
            }
        }

        @Override public String key() {
            return "cropPosterTransformation" + targetWidth;
        }
    };

    private static class ExtendedIncomingTextViewHolder extends MessageHolders.IncomingTextMessageViewHolder<Message> {
        private ChatActivity context = null;

        public ExtendedIncomingTextViewHolder(View itemView) {
            super(itemView);
            userAvatar.setOnClickListener(v -> {
                if (context != null) { context.ShowProfilePage(); }
            });
        }

        private void setContext(ChatActivity context) {
            this.context = context;
        }
    }

    private static class ExtendedIncomingImageViewHolder extends MessageHolders.IncomingImageMessageViewHolder<Message> {
        private ChatActivity context = null;

        public ExtendedIncomingImageViewHolder(View itemView) {
            super(itemView);
            userAvatar.setOnClickListener(v -> {
                if (context != null) { context.ShowProfilePage(); }
            });
        }

        private void setContext(ChatActivity context) {
            this.context = context;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        setUpToolbar();
        setTitle(R.string.title_chat);

        Intent callee = getIntent();
        if (callee.getExtras() != null) {
            Bundle extras = callee.getExtras();
            if (extras.containsKey(CHAT_ID_KEY)) {
                chatId = extras.getString(CHAT_ID_KEY);
            }
        }

        currentUserId = FirebaseAuth.getInstance().getUid();

        if (currentUserId == null || chatId == null) {
            finish();
            return;
        }

        setupChatListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();
        messageListDisposable = messageListObservable.subscribe();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (messageListDisposable != null) {
            messageListDisposable.dispose();
            messageListDisposable = null;
        }

        if (pageDisposable != null) {
            pageDisposable.dispose();
            pageDisposable = null;
        }
        if(otherUserDisposable != null) {
            otherUserDisposable.dispose();
            otherUserDisposable = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.mi_label_show_profile:
                ShowProfilePage();
                break;
            default:
                break;
        }
        return true;
    }

    private void ShowProfilePage(){
        otherUserDisposable = FirebaseUtils.downloadChatParticipantsIds(chatId)
                .flatMap(participantsIds -> {
                    for (String userId : participantsIds) {
                        if (!userId.equals(currentUserId)) {
                            return downloadUser(userId);
                        }
                    }
                    return Observable.error(new Exception("Invalid member data in chat " + chatId));
                })
                .doOnError(Throwable::printStackTrace)
                .doOnNext(user -> {
                    if (otherUserDisposable != null) {
                        otherUserDisposable.dispose();
                        otherUserDisposable = null;
                    }
                    Intent userProfileIntent = new Intent(getApplicationContext(), ShowProfileActivity.class);
                    userProfileIntent.putExtra(getString(R.string.show_user_key), user);
                    startActivity(userProfileIntent);
                })
                .subscribe();
    }

    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void setupChatListeners() {

        MessageHolders messageHolders = new MessageHolders()
                .setIncomingTextHolder(ExtendedIncomingTextViewHolder.class)
                .setIncomingImageHolder(ExtendedIncomingImageViewHolder.class)
                .setOutcomingImageLayout(R.layout.item_outcoming_image_message_fix);

        messagesListAdapter = new MessagesListAdapter<Message>(currentUserId, messageHolders, imageLoader) {
            @Override
            public void onBindViewHolder(ViewHolder holder, int position) { // Pass context to Extended view Holders to allow loading user profile
                super.onBindViewHolder(holder, position);
                if (holder instanceof ExtendedIncomingTextViewHolder) {
                    ((ExtendedIncomingTextViewHolder)holder).setContext(ChatActivity.this);
                }
                else if (holder instanceof ExtendedIncomingImageViewHolder) {
                    ((ExtendedIncomingImageViewHolder)holder).setContext(ChatActivity.this);
                }
            }
        };
        messagesListAdapter.setLoadMoreListener(onLoadMore);
        MessagesList messagesList = findViewById(R.id.messagesList);
        messagesList.setAdapter(messagesListAdapter);

        messageListObservable = downloadMessages(chatId, -1, PAGE_SIZE)
            .doOnNext(new Consumer<List<Message>>() {
                // Keep track of already downloaded messages
                // Don't put this listener's init onStart otherwise it will lose tracking
                // And load messages multiple times
                private LinkedHashMap<String, Message> messageMap = new LinkedHashMap<>();

                @Override
                public void accept(List<Message> messages) {
                    for (Message msg : messages) {
                        if (messageMap.containsKey(msg.getId())) {
                            messagesListAdapter.update(msg.getId(), msg);
                        }
                        else {
                            messagesListAdapter.addToStart(msg, true);
                        }
                        messageMap.put(msg.getId(), msg);
                        updateReceivedTimestamp(chatId, System.currentTimeMillis());
                    }

                    if (!messages.isEmpty()) {
                        updateEarliestTimestamp(messages.get(0));
                    }
                }
            })
            .doOnError(this::onError);

        MessageInput msgInput = findViewById(R.id.input);
        msgInput.setInputListener(input -> {
           sendMessage(input.toString(), null);
            return true;
        });

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            float density = getResources().getDisplayMetrics().density;
            ViewCompat.setElevation(msgInput, density * 3);
        }

        msgInput.setAttachmentsListener(() -> {
            DialogFragment dialog = new ChoosePhotoDialogFragment();
            dialog.show(getFragmentManager(), "ChoosePhotoDialogFragment");
        });
    }

    private void sendMessage(@Nullable String message, @Nullable String imageURL){
        DatabaseReference chatReference = FirebaseDatabase
                .getInstance()
                .getReference()
                .child(Constants.DBKEY_MESSAGES).child(chatId);

        DatabaseReference msgReference = chatReference.push();
        Message msg = new Message(
                msgReference.getKey(),
                message,
                String.valueOf(System.currentTimeMillis()),
                currentUserId,
                imageURL
        );
        msgReference.setValue(msg);

    }

    // Keep track of the earliest timestamp in the message list.
    // When downloading a new page, only messages earlier than this timestamp
    // will be requested.
    private void updateEarliestTimestamp(Message msg) {
        // Update timestamp of earliest message
        Long timestamp;
        try {
            timestamp = Long.parseLong(msg.getTimestamp());
            if (earliestTimestamp == null) {
                earliestTimestamp = timestamp;
            }
            else {
                earliestTimestamp = Math.min(earliestTimestamp, timestamp);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void onError(Throwable t) {
        Toast.makeText(getApplicationContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        Log.e(TAG, t.getMessage());
        finish();
    }

    //+++++++++++++++++++++++++++++++++++++++++
    //Callback for the ChoosePhotoDialogListener
    @Override
    public void onCameraDialogClick(DialogFragment dialog) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            if (
                    ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
                            PackageManager.PERMISSION_GRANTED ||
                            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                                    PackageManager.PERMISSION_GRANTED
                ) {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        CAMERA_DIALOG_PERMISSION_REQUEST
                );
                return;
            }

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
                photoFile.delete();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(getApplicationContext(), R.string.free_some_space_toast,Toast.LENGTH_SHORT)
                        .show();
                Log.d("onCameraDialogClick", "File creation for taking a picture failed");
            }

           if(photoFile!=null){
                mNextPhotoUri= FileProvider.getUriForFile(  getApplicationContext(),
                                                            AUTHORITY,
                                                            photoFile);
           }
           cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, mNextPhotoUri);
           startActivityForResult(cameraIntent,CAMERA_REQUEST);
        }

    }

    @Override
    public void onFileDialogClick(DialogFragment dialog) {
        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    GALLERY_DIALOG_PERMISSION_REQUEST
            );
            return;
        }

        Intent chooseFile = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
        chooseFile.setType("image/*");
        chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
        startActivityForResult(Intent.createChooser(chooseFile, getString(R.string.choose_file_from_gallery)), PICK_FILE_CODE);

    }
    //++++++++++++++++++++++++++++++++++++++++++

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_DIALOG_PERMISSION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    onCameraDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_camera_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;
            case GALLERY_DIALOG_PERMISSION_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    onFileDialogClick(null);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permission_storage_denied), Toast.LENGTH_LONG).show();
                }
            }
            break;

        }
    }

    @SuppressLint("CheckResult")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_FILE_CODE && resultCode == RESULT_OK) {
            if (data == null) {
                //Signal Error
                return;
            }
            // Upload the file online
            Uri selectedImage = data.getData();
            if (selectedImage == null) {
                return;
            }

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert bitmap != null;
            Bitmap jpgBitmap = ImageUtils.compressAndConvert(bitmap,Bitmap.CompressFormat.JPEG, 60);

            UploadCopyIntoFirebase(ImageUtils.getImageUri(getApplicationContext(), jpgBitmap))
                    .subscribeWith(new DisposableSingleObserver<String>() {
                        @Override
                        public void onSuccess(String string) {
                            sendMessage(null, string);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("ChatActivity", "error when sending a message");
                        }
                    });
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Uri mUri =  Uri.fromFile(new File(mCurrentPhotoPath));
            Bitmap bitmap = null;
            try {
                bitmap=ImageUtils.handleSamplingAndRotationBitmap(getApplicationContext(),mUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap jpgBitmap = null;
            if (bitmap != null) {
                jpgBitmap = ImageUtils.compressAndConvert(bitmap, Bitmap.CompressFormat.JPEG, 60);
            }
            Uri jpgUri = null;
            if (jpgBitmap != null) {
                jpgUri = ImageUtils.getImageUri(getApplicationContext(), jpgBitmap);
            }
            mCurrentPhotoPath = null;
            if (jpgUri == null) Log.d("ChatActivity", "error when converting the camera photo");
            UploadCopyIntoFirebase(jpgUri)
                    .subscribeWith(new DisposableSingleObserver<String>() {
                        @Override
                        public void onSuccess(String string) {
                            sendMessage(null, string);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("ChatActivity", "error when sending a message");
                        }
                    });
        }
    }

    private Single<String> UploadCopyIntoFirebase(Uri messageImageUri) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            throw new SecurityException("User must be authenticated to push a new copy");
        }
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        String pictureId = "chat_" + System.currentTimeMillis();
        StorageReference messageImageRef = storageRef.child("/users/" + user.getUid() + "/images/"+chatId+"/" + pictureId);

        UploadTask uploadTask = messageImageRef.putFile(messageImageUri);

        Task<Uri> urlTask  = uploadTask.continueWithTask(task -> {
            if (!task.isSuccessful()) {
                if (task.getException() != null) { throw task.getException(); }
                else { throw new UnknownError("Upload task failed"); }
            }
            // Continue with the task to get the download URL
            return messageImageRef.getDownloadUrl();
        });

        return Single.create(emitter-> urlTask.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Uri downloadUri = task.getResult();
                emitter.onSuccess(downloadUri.toString());
            } else {
                emitter.onError(task.getException());
            }
        }));
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" +"CHAT_"+ timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
