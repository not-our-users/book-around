package it.polito.students.mad.group5.bookaround.models;

import com.google.firebase.database.Exclude;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

/**
 * Created by Alessio on 06/05/2018.
 */
public class Message implements IMessage,
        MessageContentType.Image{

    private String messageId;
    private String text;
    private String timestamp;
    private String senderId;
    private String imageUrl;

    @Exclude
    private User user;

    public Message() {
        // Required empty constructor
    }

    public Message(String messageId, String text, String timestamp, String senderId) {
        this(messageId,text,timestamp,senderId,null);
    }

    public Message(String messageId, String text, String timestamp, String senderId, String imageUrl) {
        this.messageId = messageId;
        this.text = text;
        this.timestamp = timestamp;
        this.senderId = senderId;
        this.imageUrl = imageUrl;
    }

    @Override
    public String getId() {
        return messageId;
    }

    public void setId(String id) { this.messageId = id; }

    @Override
    public String getText() {
        return text;
    }

    public String getSenderId() { return senderId; }

    public String getTimestamp() { return timestamp; }

    public void setTimestamp(String timestamp) { this.timestamp = timestamp; }

    // Note: this must be set manually! You need to fetch the User's data
    // from the senderId before adding the message to any display element
    @Override
    @Exclude
    public IUser getUser() {
        return user;
    }

    @Exclude
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    @Exclude
    public Date getCreatedAt() {
        return new Date(Long.parseLong(timestamp));
    }

    @Override
    public String getImageUrl() {
        return imageUrl;
    }
}
