package it.polito.students.mad.group5.bookaround.utils;

import java.io.File;
import java.util.List;

public interface IImageCompressTaskListener {

    void onComplete(List<File> compressed);
    void onError(Throwable error);
}
