package it.polito.students.mad.group5.bookaround.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.Collections;
import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer;
import it.polito.students.mad.group5.bookaround.fragments.FragmentNavDrawer;
import it.polito.students.mad.group5.bookaround.models.Copy;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtilsKt;
import it.polito.students.mad.group5.bookaround.utils.ReactiveExtensionsKt;


public class ShowcaseActivity extends AppCompatActivity {

    private DrawerLayout drawer;
    private boolean isFiltersView = false;
    private FragmentFilterDrawer mFilterFragment;
    RecyclerAdapter nearbyCopiesListAdapter;
    private String pendingCopyKey = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showcase);

        String uid = FirebaseAuth.getInstance().getUid();
        if (uid == null) { throw new SecurityException("User must be authenticated!"); }
        Observable<List<Copy>> copiesObservable = FirebaseUtils.downloadUser(uid)
                .flatMap(user -> FirebaseUtilsKt.downloadNewestNearbyCopies(
                        user.getLatitude(),
                        user.getLongitude(),
                        8)
                );

        ReactiveExtensionsKt.toLiveData(copiesObservable, BackpressureStrategy.LATEST)
                .observe(this, wrapper -> {
                    if (wrapper != null && wrapper.getValue() != null) { nearbyCopiesListAdapter.setCopies(wrapper.getValue()); }
                    if (wrapper != null && wrapper.getException() != null) { wrapper.getException().printStackTrace(); }
                });

        setUpDrawer();
        setUpToolbar();
        setupRecyclerView();
        setTitle(getString(R.string.title_activity_showcase));
        mFilterFragment = (FragmentFilterDrawer) getSupportFragmentManager().findFragmentById(R.id.filter_drwr_fragment);
    }

    private void setupRecyclerView() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Log.d("SetupTecyclerView","User must be authenticated to view nearby copies!");


        nearbyCopiesListAdapter = new RecyclerAdapter(ShowcaseActivity.this);

        RecyclerView recyclerView = findViewById(R.id.recycler_copies_list);
        recyclerView.setAdapter(nearbyCopiesListAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }
    private void StartShowCopyActivity(Copy copy) {
        // Finish downloading book data first
        Intent showCopyIntent = new Intent(getApplicationContext(), ShowCopyActivity.class);
        showCopyIntent.putExtra(ShowCopyActivity.COPY_KEY, copy);
        startActivity(showCopyIntent);
    }

/*
    private void setCopies(List<Copy> value) {
        // TODO Set copies in layout
        int l= value.size();
        int n;
        for(n=0; n<8; n++ ){
            int resId = ShowcaseActivity.this.getResources().getIdentifier("copy_img_" + n, "id", ShowcaseActivity.this.getPackageName());
            ImageView img = findViewById(resId);
            resId = ShowcaseActivity.this.getResources().getIdentifier("copy_txt_" + n, "id", ShowcaseActivity.this.getPackageName());
            TextView tv = findViewById(resId);
            if(n<l) {

                List<String> pictures = value.get(n).getPictureList();
                RequestCreator picassoLoader = pictures != null && pictures.size() > 0 ?
                        Picasso.get().load(pictures.get(0)) : Picasso.get().load(R.mipmap.not_found);

                picassoLoader.placeholder(R.mipmap.not_found)
                        .centerInside()
                        .fit()
                        .into(img);


                Disposable bookDisposable = null;

                bookDisposable = value.get(n).getBookData().subscribe(
                        book -> tv.setText(book.getTitle()),
                        error -> {
                            Toast.makeText(getApplicationContext(), R.string.error_network_old_data, Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                );
            }else{
                img.setVisibility(View.INVISIBLE);
                tv.setVisibility(View.INVISIBLE);
            }

        }
    }
*/
    //START Drawer Fragment Classes
    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.inflateMenu();
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }
    private void setUpDrawer(){
        FragmentNavDrawer drawerFragment= (FragmentNavDrawer) getSupportFragmentManager().findFragmentById(R.id.nav_drwr_fragment);
        drawer= findViewById(R.id.drawer_layout);
    }
    //END Drawer Fragment Classes

    //START SEARCH CLASSES
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.mi_search);
        android.support.v7.widget.SearchView sv=(android.support.v7.widget.SearchView) searchItem.getActionView();
        sv.setIconifiedByDefault(false);
        View fv=findViewById(R.id.filters_frame);
        sv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            final MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                filtersMenuItem.setVisible(false);
                //editMenuItem.setVisible(true);
                if (isFiltersView) {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;

                }

            }


            @Override
            public void onViewAttachedToWindow(View arg0) {
                filtersMenuItem.setVisible(true);

            }
        });

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (isFiltersView) {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;
                }
                mFilterFragment.StartSearchActivity(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        Button advS = findViewById(R.id.searchButton);
        advS.setOnClickListener(view -> {
            String s = sv.getQuery().toString();
            mFilterFragment.StartSearchActivity(s);
            TranslateAnimation animate = new TranslateAnimation(
                    0,
                    0,
                    0,
                    -(fv.getHeight()));
            animate.setDuration(500);
            //animate.setFillAfter(false);
            fv.startAnimation(animate);
            fv.setVisibility(View.INVISIBLE);
            isFiltersView = false;
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.btn_advanced_search:
                View fv = findViewById(R.id.filters_frame);
                if (!isFiltersView) {
                    fv.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -(fv.getHeight()),
                            0);
                    animate.setDuration(500);
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate);
                    isFiltersView = true;
                } else {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;

                }
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //END SEARCH CLASSES




    private class ListItemHolder extends RecyclerView.ViewHolder {

        private TextView bookTitleView, bookDescriptionView;
        private ImageView pictureView;
        private Copy currentCopy;

        private Disposable bookDisposable = null;

        public ListItemHolder(View itemView) {
            super(itemView);
            bookTitleView = itemView.findViewById(R.id.copy_title);
            bookDescriptionView= itemView.findViewById(R.id.book_description);
            pictureView = itemView.findViewById(R.id.copy_image);
        }

        public void setData(Copy copy) {
            // We no longer care of the previous copy's result
            if (bookDisposable != null) { bookDisposable.dispose(); }

            bookDisposable = copy.getBookData().subscribe(
                    book -> bookTitleView.setText(book.getTitle()),
                    error -> {
                        Toast.makeText(getApplicationContext(), R.string.error_network_old_data, Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
            );
            bookDescriptionView.setText(copy.getDetails());

            List<String> pictures = copy.getPictureList();
            RequestCreator picassoLoader = pictures != null && pictures.size() > 0 ?
                    Picasso.get().load(pictures.get(0)) : Picasso.get().load(R.mipmap.not_found);

            picassoLoader.placeholder(R.mipmap.not_found)
                    .centerInside()
                    .fit()
                    .into(pictureView);

            currentCopy = copy;
        }

        public Copy getCurrentCopy() { return currentCopy; }
    }



    private class RecyclerAdapter extends RecyclerView.Adapter<ListItemHolder>{

        private List<Copy> mDataList;
        private LayoutInflater inflater;

        public RecyclerAdapter(Context context){
            inflater=LayoutInflater.from(context);
            this.mDataList = Collections.emptyList();
        }

        @NonNull
        @Override
        public ListItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= inflater.inflate(R.layout.showcase_list_item, parent, false);
            return new ListItemHolder(view);

        }

        public void onBindViewHolder(@NonNull ListItemHolder holder, int position){
            Copy current=mDataList.get(position);
            holder.setData(current);
            holder.itemView.setOnClickListener(
                v -> StartShowCopyActivity(holder.getCurrentCopy())
            );
        }

        @Override
        public int getItemCount() {
            return mDataList.size();
        }

        public void setCopies(List<Copy> copies) {
            mDataList = copies;
            notifyDataSetChanged();
        }
    }



}