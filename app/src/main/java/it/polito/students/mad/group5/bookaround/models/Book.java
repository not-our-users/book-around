package it.polito.students.mad.group5.bookaround.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is used to save information on the book read from the web service
 */
public class Book implements Serializable {
    private String isbn;
    private String title;
    private String publisher;
    private String googleApiId;
    private String editionYear;
    private String imageUrl;
    // Authors can be more than one
    private ArrayList<String> authors;

    public Book() { }

    public Book(String isbn, String title, String publisher, String googleApiId, String editionYear, String imageUrl, ArrayList<String> authors) {
        this.isbn = isbn;
        this.title = title;
        this.publisher = publisher;
        this.googleApiId = googleApiId;
        this.editionYear = editionYear;
        this.imageUrl = imageUrl;
        this.authors = authors;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getGoogleApiId() {
        return googleApiId;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public String getEditionYear() {
        return editionYear;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
