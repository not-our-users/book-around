package it.polito.students.mad.group5.bookaround.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Objects;

import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.activities.SearchActivity;

public class FragmentFilterDrawer extends Fragment {

    public FragmentFilterDrawer() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter_drawer, container, false);
    }

    public void StartSearchActivity(String query) {
        EditText authorEdit = Objects.requireNonNull(getView()).findViewById(R.id.author_search_edit);
        EditText titleEdit = getView().findViewById(R.id.title_search_edit);
        EditText publisherEdit = getView().findViewById(R.id.publisher_search_edit);

        String author = authorEdit.getText().toString();
        String title = titleEdit.getText().toString();
        String publisher = publisherEdit.getText().toString();

        if (author.isEmpty() && title.isEmpty() && publisher.isEmpty() && query.isEmpty()) {
            return;
        }

        Intent startSearchIntent = new Intent(getActivity(), SearchActivity.class);
        if (!author.isEmpty()) { startSearchIntent.putExtra(SearchActivity.AUTHOR_KEY, author); }
        if (!title.isEmpty()) { startSearchIntent.putExtra(SearchActivity.TITLE_KEY, title); }
        if (!publisher.isEmpty()) { startSearchIntent.putExtra(SearchActivity.PUBLISHER_KEY, publisher); }
        if (!query.isEmpty()) { startSearchIntent.putExtra(SearchActivity.KEYWORD_KEY, query); }

        startActivity(startSearchIntent);
    }
}
