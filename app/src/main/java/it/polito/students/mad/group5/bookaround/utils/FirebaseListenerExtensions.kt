package it.polito.students.mad.group5.bookaround.utils

import com.google.firebase.database.*

/**
 * Created by Alessio on 25/05/2018.
 */

private typealias ValueListener = (DataSnapshot?) -> Unit
private typealias DatabaseErrorListener = (DatabaseError?) -> Unit

class ValueEventListenerHelper : ValueEventListener {
    private var dataChangeLambda: ValueListener? = null

    fun onDataChange(onDataChange: ValueListener) {
        dataChangeLambda = onDataChange
    }

    override fun onDataChange(snapshot: DataSnapshot?) {
        dataChangeLambda?.invoke(snapshot)
    }

    private var errorLambda: DatabaseErrorListener? = null

    fun onCancelled(onCancelled: DatabaseErrorListener) {
        errorLambda = onCancelled
    }

    override fun onCancelled(dbError: DatabaseError?) {
        errorLambda?.invoke(dbError)
    }

}

// THIS IS BLACK MAGIC.
// https://antonioleiva.com/listeners-several-functions-kotlin/
fun Query.addValueEventListener(init: ValueEventListenerHelper.() -> Unit) {
    val listener = ValueEventListenerHelper()
    listener.init()
    this.addValueEventListener(listener)
}

private typealias DoTransaction = (MutableData?) -> Transaction.Result
private typealias OnCompleteTransaction = (DatabaseError?, Boolean, DataSnapshot?) -> Unit

class TransactionHandlerHelper : Transaction.Handler {
    private var doTransactionLambda : DoTransaction? = null

    fun doTransaction(doTransaction : DoTransaction) {
        doTransactionLambda = doTransaction
    }

    override fun doTransaction(data: MutableData?): Transaction.Result {
        return doTransactionLambda?.invoke(data) ?: Transaction.abort()
    }

    private var onCompleteLambda : OnCompleteTransaction? = null

    fun onComplete(onComplete : OnCompleteTransaction) {
        onCompleteLambda = onComplete
    }

    override fun onComplete(dbError: DatabaseError?, transactionCompleted: Boolean, dataSnapshot: DataSnapshot?) {
        onCompleteLambda?.invoke(dbError, transactionCompleted, dataSnapshot)
    }
}


fun DatabaseReference.runTransaction(init: TransactionHandlerHelper.() -> Unit) {
    val listener = TransactionHandlerHelper()
    listener.init()
    this.runTransaction(listener)
}