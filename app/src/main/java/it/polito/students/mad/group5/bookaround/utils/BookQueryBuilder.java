package it.polito.students.mad.group5.bookaround.utils;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nytimes.android.external.store3.base.Persister;
import com.nytimes.android.external.store3.base.impl.RealStoreBuilder;
import com.nytimes.android.external.store3.base.impl.Store;
import com.nytimes.android.external.store3.base.impl.StoreBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;

import io.reactivex.Maybe;
import io.reactivex.Single;
import it.polito.students.mad.group5.bookaround.BookaroundApplication;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.models.Book;

/**
 * Created by Alessio on 21/04/2018.
 */
public class BookQueryBuilder {

    /**
     * Given the a query URL for the Books API, this task will asynchronously load
     * and return the first N result as Book instances. The limit N is passed by
     * constructor and can be initialized through BookQueryBuilder
     */
    public static class BookQueryTask extends AsyncTask<URL, Book, List<Book>> {

        public interface BookQueryOnCompleteListener {
            void onComplete(List<Book> books);
        }

        public interface BookQueryOnProgressUpdateListener {
            void onProgressUpdate(Book... books);
        }

        private int limit;
        private URL query;
        private boolean singleVolumeQuery;  // If true, this task is a single volume based query

        private Queue<BookQueryOnCompleteListener> onCompleteListeners = new ConcurrentLinkedQueue<>();
        private Queue<BookQueryOnProgressUpdateListener> onProgressUpdateListeners = new ConcurrentLinkedQueue<>();

        BookQueryTask(URL query, int limit, boolean singleVolumeQuery) {
            this.query = query;
            this.limit = limit;
            this.singleVolumeQuery = singleVolumeQuery;
        }

        /**
         * Connects to the Google Books API service and returns the response String.
         * Authentication headers for our API KEY are added in this step.
         *
         * @param query The query to perform
         * @return The server's response
         */
        private String interrogateService(URL query) throws IOException {
            String sha1FingerPrint = BookaroundApplication.getSha1Fingerprint();
            String packageName = BookaroundApplication.getStaticPackageName();

            InputStream inputStream;
            BufferedReader reader = null;
            HttpURLConnection urlConnection = null;

            StringBuilder sb = new StringBuilder();

            try {
                urlConnection = (HttpURLConnection) query.openConnection();

                // This smells fishy to me. Required to perform queries bound to an API key
                if (sha1FingerPrint != null) {
                    urlConnection.setRequestProperty("X-Android-Package", packageName);
                    urlConnection.setRequestProperty("X-Android-Cert", sha1FingerPrint);
                }

                try {
                    inputStream = urlConnection.getInputStream();
                } catch (IOException ioe) {
                    Log.e(TAG, "Server response: " +
                            urlConnection.getResponseCode() + "; " +
                            urlConnection.getResponseMessage());

                    throw ioe;
                }

                reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 4096);

                String line;
                while ((line = reader.readLine()) != null) { sb.append(line); }
            }
            catch (UnsupportedEncodingException e) {
                Log.wtf(TAG, "UTF-8 is not supported. Just... how?!");
            }
            finally {
                if (urlConnection != null) { urlConnection.disconnect(); }
                if (reader != null) {
                    try { reader.close(); }
                    catch (IOException ioe) { ioe.printStackTrace(); }
                }
            }

            return sb.toString();
        }

        /**
         * Converts the Google Books API response into a Book for each item provided.
         * Note that items coming from a search query usually miss the Publisher property,
         * so it will be set to null. Objects without a valid ISBN are ignored.
         *
         * @param item The JSONObject to convert. Format is expected to be that of a single item.
         * @return A {@link Book} representation of the given JSON
         * @throws JSONException If deserialization errors occur.
         */
        private Book decodeBook(JSONObject item) throws JSONException {
            String googleApiId = item.getString("id");

            JSONObject volumeInfo = item.getJSONObject("volumeInfo");

            JSONArray ids = volumeInfo.getJSONArray("industryIdentifiers");
            String isbn13 = null, isbn10 = null;
            for (int j = 0; j < ids.length(); j++) {
                JSONObject id = ids.getJSONObject(j);
                if (id.getString("type").equals("ISBN_13")) {
                    isbn13 = id.getString("identifier");
                } else if (id.getString("type").equals("ISBN_10")) {
                    isbn10 = id.getString("identifier");
                }
            }
            // Skip books without ISBNs
            if (isbn10 == null && isbn13 == null) {
                throw new JSONException("No isbn field!");
            }
            String isbn = isbn13 != null ? isbn13 : isbn10;

            JSONArray authors = volumeInfo.optJSONArray("authors");
            ArrayList<String> bookAuthors = new ArrayList<>();
            if (authors != null) {
                for (int j = 0; j < authors.length(); j++) {
                    bookAuthors.add(authors.getString(j));
                }
            }

            JSONObject links = volumeInfo.optJSONObject("imageLinks");
            String thumbnail = links != null ? links.optString("thumbnail") : null;

            return new Book(
                    isbn,
                    volumeInfo.optString("title"),
                    volumeInfo.optString("publisher"),
                    googleApiId,
                    volumeInfo.optString("publishedDate"),
                    thumbnail,
                    bookAuthors
            );
        }

        /**
         * Performs a book search query. Up to #limit values will be returned, whether that
         * matches the query's maxResult parameter or not.
         *
         * @param bookListQuery The query to perform
         * @return The list of books found. According to API formats, the Publisher field will
         * be missing. Use single-volume queries (based on the googleApiId) instead if that's
         * needed.
         */
        private List<Book> downloadBookList(URL bookListQuery) {
            List<Book> result = new ArrayList<>();

            // Download the json array of the result
            JSONArray items;
            try {
                String queryResult = interrogateService(bookListQuery);

                JSONObject jo = new JSONObject(queryResult);
                items = jo.getJSONArray("items");
            }
            catch (IOException | JSONException je) {
                Log.w(TAG, je.getLocalizedMessage());
                return result;
            }

            // Produce Book instances for each item
            for (int i = 0 ; i < items.length() && i < limit && !isCancelled() ; i++) {
                try {
                    JSONObject item = items.getJSONObject(i);
                    Book book = decodeBook(item);
                    result.add(book);
                    // Notify UI of the progress. Only the last book is provided.
                    publishProgress(book);
                }
                catch (JSONException je) {
                    Log.w(TAG, je.getLocalizedMessage());
                }
            }

            return result;
        }

        /**
         * Performs a single volume-based query, returing all info regarding a certain volume.
         *
         * @param bookInfoQuery The query to perform. It must reference a single volume.
         * @return A book representation of the found book, or an empty instance if none is found.
         */
        private Book downloadBookInfo(URL bookInfoQuery) {
            try {
                JSONObject queryResult = new JSONObject(interrogateService(bookInfoQuery));
                return decodeBook(queryResult);
            }
            catch (IOException | JSONException je) {
                return null;
            }
        }

        @Override
        protected List<Book> doInBackground(URL... queries) {
            if (queries.length < 1) { return new ArrayList<>(0); }

            URL query = queries[0];
            if (singleVolumeQuery) {
                Book book = downloadBookInfo(query);
                return book != null ? Collections.singletonList(book) : Collections.emptyList();
            }
            else { return downloadBookList(query); }
        }

        @Override
        protected void onProgressUpdate(Book... values) {
            super.onProgressUpdate(values);
            if (!isCancelled()) {
                for (BookQueryOnProgressUpdateListener listener : onProgressUpdateListeners) {
                    listener.onProgressUpdate(values);
                }
            }
        }

        @Override
        protected void onPostExecute(List<Book> books) {
            super.onPostExecute(books);
            for (BookQueryOnCompleteListener listener : onCompleteListeners) {
                listener.onComplete(books);
            }
        }

        /**
         * Executes this task with the constructor-provided query.
         */
        public BookQueryTask executeDefault() {
            super.execute(query);
            return this;
        }

        /**
         * Adds a completion listener for this task. If the task already finished it will
         * be executed immediately. Callbacks are executed on the UI thread.
         *
         * @param listener
         */
        public void addOnCompleteListener(BookQueryOnCompleteListener listener) {
            if (this.getStatus() != Status.FINISHED) {
                onCompleteListeners.add(listener);
            }
            else {  // Execute immediately
                try { listener.onComplete(this.get()); }
                // These should not happen since the task should have finished
                catch (InterruptedException | ExecutionException e) { e.printStackTrace(); }
            }
        }

        /**
         * Adds a progress listener for this task. The task should not have already finished,
         * otherwise it will be simply ignored.
         *
         * @param listener
         * @return Whether the listener was added successfully or not (if the task already finished).
         */
        public boolean addOnProgressUpdateListener(BookQueryOnProgressUpdateListener listener) {
            if (this.getStatus() != Status.FINISHED) {
                onProgressUpdateListeners.add(listener);
                return true;
            }
            return false;
        }

        /**
         * Returns a {@link Single} observable for this task. When someone subscribes it will
         * start the task if not already started and register a listener to emit the result.
         */
        public Single<Book> getSingle() {
            return Single.create(bookSingleEmitter -> {
                this.addOnCompleteListener(books -> {
                    if (books.size() > 0) { bookSingleEmitter.onSuccess(books.get(0)); }
                    else { bookSingleEmitter.onError(new Exception("No Book found for given query")); }
                });
                if (getStatus() == Status.PENDING) {
                    executeDefault();
                }
            });
        }
    }

    // Cache for Book data. Check https://github.com/NYTimes/Store
    private static Store<Book, String> bookStore;

    public static void initBookCache(Context context) {
        // Init store with simple fetcher based on AsyncTask
        RealStoreBuilder<Book, Book, String> builder = StoreBuilder.<String, Book>key()
                .fetcher(apiId -> BookQueryBuilder.start().byGoogleApiId(apiId).build().getSingle());

        // Try to init the disk cache using an object stream persistor
        try {
            File cacheDir = new File(context.getCacheDir().getAbsolutePath() +
                    File.separator +
                    context.getString(R.string.book_cache_dir)
            );

            if (!cacheDir.exists()) {
                 if (!cacheDir.mkdir()) { throw new IOException("Failed to create cache directory"); }
            }

            builder = builder.persister(new Persister<Book, String>() {
                @NonNull
                @Override
                public Maybe<Book> read(@NonNull String s) {
                    File bookFile = new File(cacheDir.getAbsolutePath() + File.separator + s);
                    if (bookFile.exists()) {
                        return Maybe.fromCallable(() -> {
                            FileInputStream inputStream = new FileInputStream(bookFile);
                            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                            Book book = (Book)objectInputStream.readObject();
                            objectInputStream.close();
                            return book;
                        });
                    }
                    else {
                        return Maybe.empty();
                    }
                }

                @NonNull
                @Override
                public Single<Boolean> write(@NonNull String s, @NonNull Book book) {
                    File bookFile = new File(cacheDir.getAbsolutePath() + File.separator + s);
                    try {
                        FileOutputStream outputStream = new FileOutputStream(bookFile);
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                        objectOutputStream.writeObject(book);
                        objectOutputStream.close();
                        return Single.just(true);
                    }
                    catch (IOException ioe) {
                        Log.i(TAG, "Couldn't create cache file for book " + s);
                        Log.i(TAG, ioe.getLocalizedMessage());
                        return Single.just(false);
                    }
                }
            });
        }
        catch (IOException ioe) {
            Log.i(TAG, "Couldn't instantiate file persister. Book cache will not persist on disk");
            Log.i(TAG, ioe.getLocalizedMessage());
        }

        bookStore = builder.open();
    }

    private static final String TAG = "BookQueryBuilder";

    private int limit = 1;
    private int startIndex = 0;

    private String googleApiId = null;

    private String isbn = null;
    private String author = null;
    private String title = null;
    private String publisher = null;
    private String keywords = null;

    private BookQueryBuilder() {  }

    public static BookQueryBuilder start() { return new BookQueryBuilder(); }

    /**
     * Sets the input string as the keyword parameter of the query
     */
    public BookQueryBuilder byKeywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    /**
     * Sets the input string as the isbn parameter of the query
     */
    public BookQueryBuilder byIsbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    /**
     * Sets the input string as the author parameter of the query
     */
    public BookQueryBuilder byAuthor(String author) {
        this.author = author;
        return this;
    }

    /**
     * Sets the input string as the title parameter of the query
     */
    public BookQueryBuilder byTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Sets the input string as the keyword parameter of the query
     */
    public BookQueryBuilder byPublisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    /**
     * Overrides all query parameters, by directly looking up through Google Api Id
     */
    public BookQueryBuilder byGoogleApiId(String googleApiId) {
        this.googleApiId = googleApiId;
        return this;
    }

    /**
     * Limits the query to the first limit results.
     */
    public BookQueryBuilder limitTo(int limit) {
        this.limit = limit > 1 ?  limit : 1;
        return this;
    }

    /**
     * Sets the starting index for the query. Results are 0-indexed
     */
    public BookQueryBuilder startAt(int index) {
        startIndex = index;
        return this;
    }

    public BookQueryTask build() {
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme("https")
                  .authority("www.googleapis.com")
                  .appendEncodedPath("books/v1/volumes");

        // Build by ID overrides query params
        if (googleApiId != null) {
            uriBuilder.appendPath(googleApiId);
            uriBuilder.appendQueryParameter(    // This param limits the amount of information to be downloaded
                    "fields",                   // to just those we need
                    "id,volumeInfo(title,authors,publisher,publishedDate,industryIdentifiers,imageLinks)"
            );
        }
        else {
            String query = "";
            if (keywords != null) { query += keywords + ";"; }
            if (title != null) { query += "intitle:" + title + ";"; }
            if (author != null) { query += "inauthor:" + author + ";"; }
            if (publisher != null) { query += "inpublisher:" + publisher + ";"; }
            if (isbn != null) { query += "isbn:" + isbn; }  // Don't append semicolon ; to ISBN. Query fails.
            uriBuilder.appendQueryParameter("q", query);
            uriBuilder.appendQueryParameter(
                    "fields",
                    "items(id,volumeInfo(title,authors,publisher,publishedDate,industryIdentifiers,imageLinks))"
            );
        }

        // If it was possible to get the fingerprint, use our API Key
        if (BookaroundApplication.getSha1Fingerprint() != null) {
            uriBuilder.appendQueryParameter("key", BookaroundApplication.API_KEY);
        }

        uriBuilder.appendQueryParameter("maxResults", String.valueOf(limit));
        if (startIndex > 0) {
            uriBuilder.appendQueryParameter("startIndex", String.valueOf(startIndex));
        }

        BookQueryTask task;
        try {
            // Perform a single-volume based search if a googleApiId is provided, otherwise a regular search
            task = new BookQueryTask(new URL(uriBuilder.build().toString()), limit, googleApiId != null);
        }
        catch (MalformedURLException mue) {
            mue.printStackTrace();
            return null;
        }

        return task;
    }

    /**
     * Use this to do GoogleApiId based queries, it will look up inside the cache first.
     * @param apiId The book's GoogleApiId to lookup
     * @return A Single instance for the looked up book. Subscribe to it to react when
     * data is ready.
     */
    public static Single<Book> lookupByApiId(String apiId) {
        return bookStore.get(apiId);
    }
}
