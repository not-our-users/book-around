package it.polito.students.mad.group5.bookaround.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.ExtendedSupportMapFragment;
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer;
import it.polito.students.mad.group5.bookaround.models.Book;
import it.polito.students.mad.group5.bookaround.models.Copy;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.utils.BookQueryBuilder;

/**
 * This activity is used to provide details on a specific book.
 */
public class BookDetailsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerClickListener {

    private String googleApiId;
    private Book book;
    private User user;
    private DrawerLayout drawer;
    private boolean isFiltersView=false;
    private GoogleMap mMap;
    private Marker lastMarker = null;
    private HashMap<Marker, Copy> markerCopyMap = new HashMap<>();
    private FragmentFilterDrawer mFilterFragment;

    private GeoQuery geoQuery = null;
    private Query copiesLookupQuery = null;
    private ValueEventListener copiesLookupEventListener = null;

    private class CopyListItemHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private ImageView thumbnailView;
        private Copy copy;

        public CopyListItemHolder(View itemView) {
            super(itemView);
            thumbnailView = itemView.findViewById(R.id.my_copies_li_thumb_view);
        }

        public void setCopy(Copy copy) {
            List<String> pictureList = copy.getPictureList();
            RequestCreator picassoLoader = pictureList != null && pictureList.size() > 0 ?
                    Picasso.get().load(pictureList.get(0)) : Picasso.get().load(R.mipmap.not_found);

            picassoLoader.placeholder(R.mipmap.not_found)
                         .centerCrop()
                         .fit()
                         .into(thumbnailView);

            this.copy = copy;
        }

        public Copy getCopy() {
            return copy;
        }
    }

    private class CopiesAdapter extends RecyclerView.Adapter<CopyListItemHolder> {
        private List<Copy> copyList = new ArrayList<>();

        @NonNull
        @Override
        public CopyListItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.copy_list_item_mini, parent, false);
            CopyListItemHolder holder = new CopyListItemHolder(view);
            view.setOnClickListener(v -> StartShowCopyActivity(holder.getCopy()));
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull CopyListItemHolder holder, int position) {
            holder.setCopy(copyList.get(position));
        }

        @Override
        public int getItemCount() {
            return copyList.size();
        }

        public void addCopy(Copy copy) {
            copyList.add(copy);
            notifyItemInserted(copyList.size() - 1);
        }

        public void reset() {
            copyList.clear();
            notifyDataSetChanged();
        }
    }

    private final static String TAG = "BookDetailsActivity";

    private Disposable bookDataDisposable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        setUpToolbar();
        setTitle(getString(R.string.title_activity_book_details));

        ExtendedSupportMapFragment mMapFragment = (ExtendedSupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mFilterFragment = (FragmentFilterDrawer)getSupportFragmentManager().findFragmentById(R.id.filter_drwr_fragment);

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        Intent callee = getIntent();
        if (callee != null) {
            googleApiId = callee.getStringExtra("googleApiId");
            if(googleApiId == null)
                return;
        }
        else { finish(); return ;}

        if (firebaseUser != null) {
            FirebaseDatabase.getInstance()
                    .getReference()
                    .child(getString(R.string.DBKEY_USERS))
                    .child(firebaseUser.getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            user = dataSnapshot.getValue(User.class);
                            mMapFragment.getMapAsync(BookDetailsActivity.this);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // Don't show map if no user info
                            getSupportFragmentManager().popBackStack();
                            updateFields(googleApiId);
                        }
                    });
        }
        else {
            // Don't show map if no user info
            getSupportFragmentManager().popBackStack();
            updateFields(googleApiId);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (bookDataDisposable != null) { bookDataDisposable.dispose(); }
        if (geoQuery != null) { geoQuery.removeAllListeners(); }
        if (copiesLookupQuery != null && copiesLookupEventListener != null) {
            copiesLookupQuery.removeEventListener(copiesLookupEventListener);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        ScrollView mScrollView = findViewById(R.id.book_details_scroll_view);
        ((ExtendedSupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .setListener(() -> mScrollView.requestDisallowInterceptTouchEvent(true));
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(user.getLatitude(), user.getLongitude()),
                10.3f
        ));
        updateFields(googleApiId);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (lastMarker != null) {
            lastMarker.hideInfoWindow();
            if (lastMarker == marker) { lastMarker = null; }
        }

        marker.showInfoWindow();
        lastMarker = marker;

        return true;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        if (markerCopyMap.containsKey(marker)) {
            Copy cp = markerCopyMap.get(marker);
            Intent showCopyIntent = new Intent(this, ShowCopyActivity.class);
            showCopyIntent.putExtra(ShowCopyActivity.COPY_KEY, cp);
            startActivity(showCopyIntent);
        }
    }

    private void updateFields(String googleApiId) {
        // The ISBN is read from the Copy object
        if (bookDataDisposable != null) { bookDataDisposable.dispose(); }

        bookDataDisposable = BookQueryBuilder.lookupByApiId(googleApiId).subscribe(
            book -> {
                this.book = book;
                TextView title = findViewById(R.id.show_book_title);
                title.setText(book.getTitle());
                TextView publisher = findViewById(R.id.show_book_publisher);
                publisher.setText(book.getPublisher());
                TextView editionYear = findViewById(R.id.show_book_edition_year);
                editionYear.setText(book.getEditionYear());
                setAuthorsView(book);
                ImageView image = findViewById(R.id.book_picture);
                Picasso.get().load(book.getImageUrl()).placeholder(android.R.drawable.ic_menu_report_image).into(image);
                setCopiesView();

                image.setOnClickListener(view -> {
                    //set the image in dialog popup
                    //below code fullfil the requirement of xml layout file for dialog popup
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    LinearLayout layout = new LinearLayout(getApplicationContext());
                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.setGravity(Gravity.CENTER);

                    LinearLayout.LayoutParams imageViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    float density = getResources().getDisplayMetrics().density;
                    imageViewParams.setMargins(
                            (int)(density * 80),
                            (int)(density * 80),
                            (int)(density * 80),
                            (int)(density * 80)
                    );

                    ImageView iv = new ImageView(getApplicationContext());
                    iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    iv.setAdjustViewBounds(true);
                    layout.addView(iv, imageViewParams);

                    Dialog builder = new Dialog(BookDetailsActivity.this);
                    builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    Objects.requireNonNull(builder.getWindow()).setBackgroundDrawable(
                            new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    builder.setOnDismissListener(dialogInterface -> {
                        //nothing;
                    });
                    builder.addContentView(layout, layoutParams);
                    layout.setOnClickListener(v -> builder.dismiss());
                    builder.show();
                    builder.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    Picasso.get().load(book.getImageUrl()).placeholder(android.R.drawable.ic_menu_report_image).fit()
                            .centerInside().into(iv);
                });

            },
            Throwable::printStackTrace
        );
    }

    private void setAuthorsView(Book book) {
        TextView tv= findViewById(R.id.show_book_authors);
        String authors="";
        boolean first=true;
        if (book.getAuthors() != null) {
            for (String author : book.getAuthors()) {
                if(!first)
                    authors=authors.concat(", ");
                first=false;
                authors=authors.concat(author);
            }
        }
        tv.setText(authors);


    }

    private void setCopiesView() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            DatabaseReference userRef = FirebaseDatabase.getInstance()
                                                        .getReference()
                                                        .child(getString(R.string.DBKEY_USERS))
                                                        .child(user.getUid());

            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);

                    DatabaseReference copyRef = FirebaseDatabase.getInstance()
                                                                .getReference()
                                                                .child(getString(R.string.DBKEY_POSITIONS))
                                                                .child(book.getGoogleApiId());

                    GeoFire geoFire = new GeoFire(copyRef);
                    assert user != null;
                    GeoQuery geoQuery = geoFire.queryAtLocation(
                            new GeoLocation(user.getLatitude(), user.getLongitude()),
                            20
                    );

                    geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
                        private HashMap<String, GeoLocation> copiesMap = new HashMap<>();

                        @Override
                        public void onKeyEntered(String key, GeoLocation location) {
                            copiesMap.put(key, location);
                        }

                        @Override
                        public void onKeyExited(String key) {
                            // Don't care
                        }

                        @Override
                        public void onKeyMoved(String key, GeoLocation location) {
                            // Don't care
                        }

                        @Override
                        public void onGeoQueryReady() {
                            setCopiesViewWithFilter(copiesMap);
                        }

                        @Override
                        public void onGeoQueryError(DatabaseError error) {
                            setCopiesViewWithFilter(null);
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    databaseError.toException().printStackTrace();
                }
            });
        }
        else { setCopiesViewWithFilter(null); }
    }

    private void setCopiesViewWithFilter(Map<String, GeoLocation> filters) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        RecyclerView myCopiesRecycler = findViewById(R.id.recycler_my_copies_list);
        CopiesAdapter myCopiesAdapter = new CopiesAdapter();

        if (user == null) {
            // I guess we can just hide the 'My Copies' part,
            // although maybe we should force reauthentication
            TextView myCopiesLabel = findViewById(R.id.my_book_copies_label);
            myCopiesLabel.setVisibility(View.GONE);
            myCopiesRecycler.setVisibility(View.GONE);
        }
        else {
            myCopiesRecycler.setAdapter(myCopiesAdapter);
            GridLayoutManager myCopiesLayoutManager = new GridLayoutManager(this, 5);
            myCopiesRecycler.setLayoutManager(myCopiesLayoutManager);
            myCopiesRecycler.setItemAnimator(new DefaultItemAnimator());
        }

        RecyclerView localCopiesRecycler = findViewById(R.id.recycler_copies_list);
        CopiesAdapter localCopiesAdapter = new CopiesAdapter();
        localCopiesRecycler.setAdapter(localCopiesAdapter);
        GridLayoutManager localCopiesLayoutManager = new GridLayoutManager(this, 5);
        localCopiesRecycler.setLayoutManager(localCopiesLayoutManager);
        localCopiesRecycler.setItemAnimator(new DefaultItemAnimator());

        copiesLookupQuery = FirebaseDatabase.getInstance()
                .getReference()
                .child(getString(R.string.DBKEY_COPIES))
                .orderByChild(getString(R.string.DBKEY_GOOGLEAPIID))
                .equalTo(book.getGoogleApiId());

        copiesLookupEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                myCopiesAdapter.reset();
                localCopiesAdapter.reset();
                markerCopyMap.clear();
                mMap.clear();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Copy copy = child.getValue(Copy.class);
                    if (copy != null) {
                        if (user != null && user.getUid().equals(copy.getOwnerId())) {
                            myCopiesAdapter.addCopy(copy);
                        } else {
                            if (filters == null || filters.containsKey(copy.getCopyId())) {
                                localCopiesAdapter.addCopy(copy);
                                MarkerOptions mo = new MarkerOptions();
                                mo.position(new LatLng(copy.getLatitude(), copy.getLongitude()));
                                mo.title(copy.getTown());
                                mo.snippet(copy.getDetails());
                                markerCopyMap.put(mMap.addMarker(mo), copy);
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.i(TAG, databaseError.getMessage());
            }
        };

        copiesLookupQuery.addValueEventListener(copiesLookupEventListener);
    }

    private void StartShowCopyActivity(Copy copy) {
        Intent showCopyIntent = new Intent(getApplicationContext(), ShowCopyActivity.class);
        showCopyIntent.putExtra(ShowCopyActivity.COPY_KEY, copy);
        startActivity(showCopyIntent);
    }


    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    //START SEARCH CLASSES

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.mi_search);
        android.support.v7.widget.SearchView sv=(android.support.v7.widget.SearchView) searchItem.getActionView();
        sv.setIconifiedByDefault(false);
        View fv=findViewById(R.id.filters_frame);
        sv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);
            @Override
            public void onViewDetachedFromWindow(View arg0) {
                filtersMenuItem.setVisible(false);
                if(isFiltersView){
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
            }


            @Override
            public void onViewAttachedToWindow(View arg0) {
                filtersMenuItem.setVisible(true);
            }
        });

        sv.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(isFiltersView){
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
                mFilterFragment.StartSearchActivity(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        Button advS= findViewById(R.id.searchButton);
        advS.setOnClickListener(view -> {
            String s=sv.getQuery().toString();

            mFilterFragment.StartSearchActivity(s);

            TranslateAnimation animate = new TranslateAnimation(
                    0,
                    0,
                    0,
                    -(fv.getHeight()));
            animate.setDuration(500);
            //animate.setFillAfter(false);
            fv.startAnimation(animate);
            fv.setVisibility(View.INVISIBLE);
            isFiltersView=false;
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.btn_advanced_search:
                View fv=findViewById(R.id.filters_frame);
                if(!isFiltersView){
                    fv.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -(fv.getHeight()),
                            0);
                    animate.setDuration(500);
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate);
                    isFiltersView=true;
                }
                else{
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    //END SEARCH CLASSES
}


