package it.polito.students.mad.group5.bookaround.models;

import com.google.firebase.database.Exclude;
import com.stfalcon.chatkit.commons.models.IDialog;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.List;

/**
 * Created by Alessio on 07/05/2018.
 */
public class Chat implements IDialog {

    private String id;
    private String lastMessageId;

    @Exclude
    private String chatName;        // Should be display name of other participant

    @Exclude
    private String dialogPhoto;     // Should be profile pic of other participant

    @Exclude
    private List<? extends IUser> participants;

    @Exclude
    private IMessage lastMessage;

    @Exclude
    private int unreadCount;    // Referred to the current user

    public Chat() {
        // Required empty constructor
    }

    public Chat(String chatId){
        this.id=chatId;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    @Exclude
    public String getDialogPhoto() {
        return dialogPhoto;
    }

    @Exclude
    public void setDialogPhoto(String dialogPhoto) {
        this.dialogPhoto = dialogPhoto;
    }

    @Override
    @Exclude
    public String getDialogName() {
        return chatName;
    }

    @Exclude
    public void setDialogName(String chatName) {
        this.chatName = chatName;
    }

    @Override
    @Exclude
    public List<? extends IUser> getUsers() {
        return participants;
    }

    @Exclude
    public void setParticipants(List<? extends IUser> participants) {
        this.participants = participants;
    }

    @Override
    @Exclude
    public IMessage getLastMessage() {
        return lastMessage;
    }

    @Override
    @Exclude
    public void setLastMessage(IMessage message) {
        this.lastMessage = message;
    }

    @Override
    @Exclude
    public int getUnreadCount() {
        return unreadCount;
    }

    @Exclude
    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }
}
