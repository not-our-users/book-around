package it.polito.students.mad.group5.bookaround.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import it.polito.students.mad.group5.bookaround.R;
import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class BarcodeReaderActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {

    private final static String TAG = "BarcodeReaderActivity";

    private ZBarScannerView barScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        barScannerView = new ZBarScannerView(this.getApplicationContext());

        // This should filter non-ISBN codes, but sometimes ISBN codes are initially
        // recognized as EAN8/13 or other formats and would thus be filtered out.
        // The drawback to not filtering codes is that the camera will shutter when
        // pointing at other types of codes.
        //barScannerView.setFormats(Arrays.asList(BarcodeFormat.ISBN13, BarcodeFormat.ISBN10));

        setContentView(barScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        barScannerView.setResultHandler(this);
        barScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        barScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        if (rawResult.getBarcodeFormat() == BarcodeFormat.ISBN13 ||
            rawResult.getBarcodeFormat() == BarcodeFormat.ISBN10)
        {
            String readValue = rawResult.getContents();
            Intent readBarcodeIntent = new Intent();
            readBarcodeIntent.putExtra(getString(R.string.read_barcode_key), readValue);
            setResult(RESULT_OK, readBarcodeIntent);
            finish();
        }
        else {
            barScannerView.resumeCameraPreview(this);
        }
    }
}
