package it.polito.students.mad.group5.bookaround.utils;

import android.support.v4.util.Pair;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import it.polito.students.mad.group5.bookaround.models.Chat;
import it.polito.students.mad.group5.bookaround.models.Message;
import it.polito.students.mad.group5.bookaround.models.User;

/**
 * <p>Frontend class to access reactive instances of database entities.</p>
 *
 * <p>As a general rule, returned Observables represent different snapshots of remote
 * entities as they evolve in time. When subscribing to said observables, a listener
 * is registered to all dependent data, and sub-entity changes triggers an onNext call
 * providing an updated snapshot.</p>
 *
 * <p>Due to how Firebase persistence works, it is recommended to listen for all future
 * emissions of the observable, until no longer interested in its value. Listening
 * to just one single value will most likely return the cached data and never update.</p>
 *
 * <p>It is <em>strongly</em> suggested to keep track of disposables generated when
 * subscribing to said observables and dispose of them when no longer interested in
 * the tracked value. Doing so will unsubscribe the database listeners and stop
 * any pending update</p>
 *
 * <p>For information about RxJava and reactive programming check this introduction:</p>
 *
 * <p><a href=https://gist.github.com/staltz/868e7e9bc2a7b8c1f754">
 *     The introduction to Reactive Programming you've been missing · GitHub
 * </a></p>
 *
 * <em><p>Created by Alessio on 07/05/2018.</p></em>
 */
public class FirebaseUtils {

    private static final String TAG = "FirebaseUtils";

    /**
     * Helper method to convert an Object array from {@link Observable#combineLatest(ObservableSource[], Function)},
     * assuming the observed values are instances of {@link Optional}, into a List with only non
     * empty elements. Of course the returned list can be empty if all items are empty.
     *
     * @param optArray the input object array (must be of Optionals!) to convert
     * @param <T> the inner observed type
     * @return A List of all values for non-empty Optionals in optArray
     * @throws ClassCastException if an item in optArray is not an instance of Optional<T>
     */
    public static <T> List<T> fromOptArray(Object[] optArray) {
        List<T> retList = new ArrayList<>(optArray.length);
        for (Object optionalObject : optArray) {
            // Thanks to Java's AMAZING type casting, Observable.combineLatest() provides
            // Object[] arrays, so this unchecked cast is required
            Optional<T> optional = (Optional<T>)optionalObject;
            if (!optional.isEmpty()) { retList.add(optional.get()); }
        }
        return retList;
    }

    /**
     * <p>Helper method to wrap {@link Observable}s that might produce errors within an observable
     * {@link Optional} that emits an empty instance in case of error.</p>
     *
     * <p>Please be aware that errors emitted by the source observable are masked and won't be
     * propagated downstream. Nevertheless the default implementation logs the error's message
     * as a warning.</p>
     *
     * @param observable the observable to wrap
     * @param <T> the observed type
     * @return An error-safe Observable that returns empty Optionals in case of error.
     */
    public static <T> Observable<Optional<T>> wrapInOptional(Observable<T> observable) {
        return observable.map(Optional::new)
                .doOnError(e -> Log.w(TAG, e.getMessage()))
                .onErrorReturnItem(new Optional<>(null));
    }

    /**
     * Creates an {@link Observable} that tracks a {@link User} based on its id.
     *
     * @param userId The id of the user to track
     * @return An Observable for the requested user.
     */
    public static Observable<User> downloadUser(String userId) {
        DatabaseReference userRef = getUsersRef().child(userId);

        Observable<Optional<User>> userObservable = wrapInOptional(
            Observable.create(emitter -> userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (emitter.isDisposed()) {
                        emitter.onComplete();
                        userRef.removeEventListener(this);
                        return;
                    }
                    User user = dataSnapshot.getValue(User.class);

                    if (user == null) {
                        emitter.onError(new Exception("Unknown userId: " + userId));
                        return;
                    }

                    emitter.onNext(user);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    userRef.removeEventListener(this);
                    if (!emitter.isDisposed()) {
                        emitter.onError(new Exception(databaseError.getMessage()));
                    }
                }
            }))
        );

        Observable<Optional<Float>> ratingObservable = wrapInOptional(FirebaseUtilsKt.downloadUserScore(userId));

        return Observable.combineLatest(userObservable, ratingObservable, (user, rating) -> {
            if (user.isEmpty() || rating.isEmpty()) {
                throw new NullPointerException("Could not find data for userId: " + userId);
            }
            user.get().setStars(rating.get());
            return user.get();
        });
    }

    /**
     * Creates an {@link Observable} that tracks the list of {@link Chat}s the currently
     * authenticated user is part of. Returned instances are completely reconstructed, as per
     * {@link #downloadChat(String)}.
     *
     * @return an Observable to the List of Chats the authenticate user is part of.
     * @throws SecurityException if the user is not authenticated
     */
    public static Observable<List<Chat>> downloadUserChats() {
        return downloadUserChatIds()
                .flatMap(chatIds -> downloadByIds(chatIds, FirebaseUtils::downloadChat));
    }

    private static Observable<List<String>> downloadUserChatIds() {
        String currentUserId = FirebaseAuth.getInstance().getUid();
        if (currentUserId == null) {
            throw new SecurityException("User must authenticated to access chat data!");
        }

        // Chats the user doesn't belong to will have a null value for the currentUserId key.
        // These elements are sorted first. By specifying startAt(true) we filter only chats
        // that have a valid value for currentUserId.
        //
        // To know more, check: https://firebase.google.com/docs/database/web/lists-of-data#data-order
        Query myChatsQuery = getMembersRef().orderByChild(currentUserId).startAt(true);
        return Observable.create(emitter -> myChatsQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    myChatsQuery.removeEventListener(this);
                    return;
                }

                // Emit list of chatIds our user belongs to
                List<String> chatIds = new ArrayList<>();
                for (DataSnapshot chat : dataSnapshot.getChildren()) {
                    chatIds.add(chat.getKey());
                }
                emitter.onNext(chatIds);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                myChatsQuery.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }));
    }

    /**
     * <p>Creates an {@link Observable} which tracks a given {@link Chat}. Chat instances generated
     * are completely reconstructed, including the last message's data and participants'
     * {@link User} data. The only exception is for empty chats without any message, for
     * which lastMessage will be null.</p>
     *
     * <p>The user must be authenticated in order to distinguish it from the other participant.
     * By default, the other participant's name and profile picture will be set to the Chat's
     * {@link Chat#chatName} and {@link Chat#dialogPhoto} respectively. As such, a Base64 loader
     * must be set to decode the chat's images. See {@link User#getProfilePicB64()}.</p>
     *
     * @param chatId the id of the chat to track.
     * @return An observable for the requested chat
     * @throws SecurityException if the user is not authenticated
     */
    public static Observable<Chat> downloadChat(String chatId) {
        String currentUserId = FirebaseAuth.getInstance().getUid();
        if (currentUserId == null) {
            throw new SecurityException("User must authenticated to access chat data!");
        }

        Observable<List<User>> chatParticipantsObservable =
                downloadChatParticipantsIds(chatId)
                .flatMap(userIds -> downloadByIds(userIds, FirebaseUtils::downloadUser))
                .doOnError(e -> Log.w(TAG, e.getMessage()))
                .onErrorReturnItem(Collections.emptyList());


        Observable<Optional<Message>> lastMessageObservable = downloadMessages(chatId, -1, 1)
                .map(msgs -> new Optional<>(msgs.isEmpty() ? null : msgs.get(0)))
                .doOnError(e -> Log.w(TAG, e.getMessage()))
                .onErrorReturnItem(new Optional<>(null));

        Observable<Optional<Integer>> unreadCountObservable = wrapInOptional(downloadUnreadMessagesCount(chatId));

        // We need each of these four entities to reconstruct a Chat. By using combineLatest we emit
        // a new Chat instance each time one of them emits an update, with the latest update from
        // the others.
        return Observable.combineLatest( chatParticipantsObservable, lastMessageObservable, unreadCountObservable,
            (participants, lastMessage, unreadCount) -> {
                Chat chat = new Chat(chatId);
                boolean isUserParticipant = false; // Ensure the user is part of this chat.
                User otherUser = null;

                for (User user : participants) {
                    if (user.getUserId().equals(currentUserId)) {
                        isUserParticipant = true;
                    }
                    else { otherUser = user; }  // This makes sense only assuming there is just
                }                               // one more participant in the chat
                if (!isUserParticipant) {
                    throw new SecurityException("Authenticated user is not part of chat " + chatId);
                }
                if (otherUser == null) {
                    throw new NullPointerException("Somehow, chat " + chatId + " had the wrong number of users");
                }

                chat.setDialogName(otherUser.getName());
                chat.setDialogPhoto(otherUser.getProfilePicB64());
                chat.setParticipants(participants);
                if (!lastMessage.isEmpty()) { chat.setLastMessage(lastMessage.get()); }
                if (!unreadCount.isEmpty()) { chat.setUnreadCount(unreadCount.get()); }

                return chat;
            }
        );
    }

    /**
     * Creates an {@link Observable} which tracks the List of {@link Message} in a given chat.
     * Message instances generated are completely reconstructed, as per {@link #downloadMessage(String, String)}.
     *
     * @param chatId the id of the chat whose messages are requested
     * @param firstN If greater than 0, will limit to first n messages
     * @param lastN If greater than 0, will limit to last n messages
     * @return An observable for the requested list of messages
     */
    public static Observable<List<Message>> downloadMessages(String chatId, int firstN, int lastN) {
        return downloadMessages(chatId, firstN, lastN, -1);
    }

    /**
     * <p>Creates an {@link Observable} which tracks the List of {@link Message} in a given chat, optionally
     * limited up to a given timestamp.</p>
     *
     * <p>Message instances generated are completely reconstructed, as per {@link #downloadMessage(String, String)}.</p>
     *
     * @param chatId the id of the chat whose messages are requested
     * @param firstN If greater than 0, will limit to first n messages
     * @param lastN If greater than 0, will limit to last n messages
     * @param endAt If greater than 0, messages earlier than this timestamp will be displayed.
     *              LastN and FirstN filters are applied AFTER this one so mind that.
     * @return An observable for the requested list of messages
     */
    public static Observable<List<Message>> downloadMessages(String chatId, int firstN, int lastN, long endAt) {
        Query msgQuery = getMessagesRef().child(chatId).orderByChild(Constants.DBKEY_MESSAGES_TIMESTAMP).startAt(0);

        if (endAt > 0) { msgQuery = msgQuery.endAt(String.valueOf(endAt)); }
        if (firstN > 0) { msgQuery = msgQuery.limitToFirst(firstN); }
        if (lastN > 0) { msgQuery = msgQuery.limitToLast(lastN); }

        final Query fMsgQuery = msgQuery;
        return Observable.create((ObservableOnSubscribe<List<String>>) emitter -> fMsgQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    fMsgQuery.removeEventListener(this);
                    return;
                }

                // Start by querying the requested message ids
                List<String> childrenKeys = new ArrayList<>();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    childrenKeys.add(child.getKey());
                }
                emitter.onNext(childrenKeys);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                fMsgQuery.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }))
        .flatMap(messagesIds -> downloadByIds(messagesIds, msgId -> downloadMessage(chatId, msgId)));
    }

    /**
     * Creates an {@link Observable} which tracks a given {@link Message}. The generated instance
     * is completely reconstructed, including sender's {@link User} data.
     *
     * @param chatId The id of the requested message's chat.
     * @param msgId The id of the requested message
     * @return An observable for the requested message
     */
    public static Observable<Message> downloadMessage(String chatId, String msgId) {
        // Get partial message data
        return downloadMessageData(chatId, msgId)
                .flatMap(
                    msg -> downloadUser(msg.getSenderId()), // Use it to query User data
                    (msg, user) -> {
                        msg.setUser(user);  // Finally emit complete Message instance
                        return msg;
                    }
                );
    }

    /**
     * Creates an {@link Observable} which tracks a given {@link Message}. The generated instance
     * is only partial, and contains only data within the messages database element.
     *
     * @param chatId The id of the requested message's chat.
     * @param msgId The id of the requested message
     * @return An observable for the requested message
     */
    private static Observable<Message> downloadMessageData(String chatId, String msgId) {
        DatabaseReference msgRef = getMessagesRef().child(chatId).child(msgId);

        return Observable.create(emitter -> msgRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    msgRef.removeEventListener(this);
                    return;
                }

                // TODO This is one of many, we must protect other deserialization too!
                Message msg;
                try { msg = dataSnapshot.getValue(Message.class); }
                catch (Exception e) { emitter.onError(e); return; }

                if (msg != null && msg.getSenderId() != null && msg.getTimestamp() != null && msg.getId() != null) {
                    emitter.onNext(msg);
                }
                else if (msg == null) {
                    emitter.onError(new NullPointerException("Unknown message " + msgId));
                }
                else {
                    emitter.onError(new Exception("Invalid message format for message " + msgId));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                msgRef.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }));
    }

    /**
     * <p>Creates an {@link Observable} which tracks the &lt;user-id, unread&gt; map for a given chat,
     * which basically boils down to children of the members database element</p>
     *
     * <p>The user must be authenticated in order to enforce its membership to the requested
     * chat. However this will probably be later removed as we enforce these rules
     * server-side.</p>
     *
     * @param chatId The chat id for the requested unread map.
     * @return An observable fo the requested unread map. The map contains the timestamp of the last read message
     * @throws SecurityException if the user is not authenticated
     */
    private static Observable<Map<String, Long>> getMembersUnreadMap(String chatId) {
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId == null) { throw new SecurityException("User must be authenticated to access chat data!"); }

        DatabaseReference unreadMapRef = getLastReadRef().child(chatId);

        return Observable.create(emitter -> unreadMapRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    unreadMapRef.removeEventListener(this);
                    return;
                }

                // Convert chat unread data as a map of String,Long
                Map<String, Long> unreadMap = dataSnapshot.getValue(new GenericTypeIndicator<Map<String, Long>>(){});
                if (unreadMap == null) {
                    emitter.onError(new Exception("No unread data for chat " + chatId));
                    return;
                }
                if (!unreadMap.containsKey(userId)) {
                    // Assume this is a new chat with no value for the current user
                    unreadMap.put(userId, 0L);
                }
                if (unreadMap.size() != 2) {
                    emitter.onError(new UnsupportedOperationException("Wrong number of users in chat " + chatId + "! Group chats or single user chats are not yet implemented"));
                    return;
                }

                emitter.onNext(unreadMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                unreadMapRef.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }));
    }

    /**
     * Creates an {@link Observable} for the set of user ids of a chat's participants.
     *
     * @param chatId the id of the requested participant's chat.
     * @return An observable for the set of requested ids.
     */
    public static Observable<Set<String>> downloadChatParticipantsIds(String chatId) {
        DatabaseReference membersRef = getMembersRef().child(chatId);

        return Observable.create(emitter -> membersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    membersRef.removeEventListener(this);
                    return;
                }

                Map<String, Boolean> members = dataSnapshot.getValue(new GenericTypeIndicator<Map<String, Boolean>>(){});
                if (members == null) {
                    emitter.onError(new Exception("No member data for chat " + chatId));
                    return;
                }
                if (members.size() != 2) {
                    emitter.onError(new UnsupportedOperationException("Wrong number of users in chat " + chatId + "! Group chats or single user chats are not yet implemented"));
                    return;
                }

                emitter.onNext(members.keySet());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                membersRef.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }));
    }

    /**
     * Creates an {@link Observable} which tracks the cumulative number of unread messages
     * over all of the user's chats.
     *
     * @return An observable for the total of unread messages.
     */
    public static Observable<Integer> downloadCumulativeUnreadMessagesCount() {
        return downloadAllUnreadMessages()
                .map(unread -> {
                    int unreadTotal = 0;
                    for (String chatId : unread.keySet()) { unreadTotal += unread.get(chatId).size(); }
                    return unreadTotal;
                });
    }

    /**
     * Creates an {@link Observable} for the number of messages the current user has not read
     * yet from a given chat. The user must be authenticated in order to do so.
     *
     * @param chatId The id of the chat whose unread message count is requested.
     * @return An observable for the number of messages the user has not yet read from this chat.
     * @throws SecurityException if the user is not authenticated.
     */
    public static Observable<Integer> downloadUnreadMessagesCount(String chatId) {
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId == null) { throw new SecurityException("User must be authenticated to access chat data!"); }

        return getMembersUnreadMap(chatId)
                .map(unreadMap -> unreadMap.get(userId))
                .flatMap(lastTimestamp -> downloadMessagesCountAfter(chatId, lastTimestamp));
    }

    /**
     * Creates an {@link Observable} for the number of messages in a chat which come after a given
     * timestamp. Usually used to get the count of unread messages
     *
     * @param chatId The chat for which to download the message count
     * @param timestamp The lower bound for the message timestamp
     * @return An observable for the number of messages in this chat that come after timestamp.
     * @throws SecurityException if the user is not authenticated
     */
    private static Observable<Integer> downloadMessagesCountAfter(String chatId, long timestamp) {
        return downloadMessageIdsAfter(chatId, timestamp).map(List::size);
    }

    /**
     * Creates an {@link Observable} for messages in a given chat that come after a given
     * instant in time.
     *
     * @param chatId the desired chat
     * @param timestamp the desire timestamp
     * @return An observable to the list of messages in the chat that come after the given timestamp.
     */
    private static Observable<List<Message>> downloadMessagesAfter(String chatId, long timestamp) {
        return downloadMessageIdsAfter(chatId, timestamp)
                .flatMap(msgIds -> downloadByIds(msgIds, msgId -> downloadMessage(chatId, msgId)));
    }

    /**
     * Creates an {@link Observable} for all messages the user has not yet read in a given chat.
     *
     * @param chatId the chatId of interest
     * @return An Observable for all unread messages in the chat.
     * @throws SecurityException if the user is not authenticated.
     */
    public static Observable<List<Message>> downloadUnreadMessages(String chatId) {
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId == null) { throw new SecurityException("User must be authenticated to access chat data!"); }

        return getMembersUnreadMap(chatId)
                .map(unreadMap -> unreadMap.get(userId))
                .flatMap(lastTimestamp -> downloadMessagesAfter(chatId, lastTimestamp));
    }

    /**
     * Creates an {@link Observable} for all unread messages of the current user.
     *
     * @return An Observable for a map that ties all chat ids to the list of unread messages
     * (which can be empty).
     */
    public static Observable<Map<String, List<Message>>> downloadAllUnreadMessages() {
        return downloadUserChatIds()
                .flatMap(chatIds -> mapByIds(chatIds, FirebaseUtils::downloadUnreadMessages));
    }

    /**
     * Creates an {@link Observable} for the list of message ids in a chat for messages
     * which come after a given timestamp.
     *
     * @param chatId The chat for which to download the list of message ids
     * @param timestamp The lower bound for the message timestamp
     * @return An observable for the list of ids of messages in this chat that come after timestamp.
     * @throws SecurityException if the user is not authenticated
     */
    private static Observable<List<String>> downloadMessageIdsAfter(String chatId, long timestamp) {
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId == null) { throw new SecurityException("User must be authenticated to access chat data!"); }

        Query unreadQuery = getMessagesRef()
                .child(chatId)
                .orderByChild(Constants.DBKEY_MESSAGES_TIMESTAMP)
                .startAt(String.valueOf(timestamp));

        return Observable.create(emitter -> unreadQuery.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (emitter.isDisposed()) {
                    emitter.onComplete();
                    unreadQuery.removeEventListener(this);
                    return;
                }

                List<String> selectedMessageIds = new ArrayList<>();
                for (DataSnapshot msg : dataSnapshot.getChildren()) {
                    String sender = msg.child(Constants.DBKEY_MESSAGES_SENDER).getValue(String.class);
                    // Can't be null due to startAt constraint
                    String msgTimestamp = msg.child(Constants.DBKEY_MESSAGES_TIMESTAMP).getValue(String.class);
                    Long msgTimestampLong = Long.parseLong(msgTimestamp);
                    if (sender != null && !sender.equals(userId) && msgTimestampLong > timestamp) {
                        selectedMessageIds.add(msg.getKey());
                    }
                }
                emitter.onNext(selectedMessageIds);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                unreadQuery.removeEventListener(this);
                if (!emitter.isDisposed()) {
                    emitter.onError(new Exception(databaseError.getMessage()));
                }
            }
        }));
    }

    /**
     * Makes a remote transaction that updates the last-read timestamp for the current user
     * in a given chat.
     *
     * @param chatId the chat whose last-read timestamp must be updated
     * @param timeMillis the new timestamp
     * @throws SecurityException if the user is not authenticated
     */
    public static void updateReceivedTimestamp(String chatId, long timeMillis) {
        String userId = FirebaseAuth.getInstance().getUid();
        if (userId == null) {
            throw new SecurityException("User must be authenticated to update the timestamp!");
        }

        DatabaseReference timestampRef = getLastReadRef().child(chatId).child(userId);
        timestampRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                if (mutableData == null) {
                    throw new DatabaseException("Chat " + chatId + " doesn't exists!");
                }
                Long prevTimestamp = mutableData.getValue(Long.class);
                if (prevTimestamp == null) { prevTimestamp = 0L; }
                mutableData.setValue(Math.max(prevTimestamp, timeMillis));

                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, DataSnapshot dataSnapshot) {
                if (!committed) { Log.w(TAG, databaseError.getMessage()); }
            }
        });
    }

    /**
     * <p>Helper method to safely combine lists of observables into a single observable list.</p>
     *
     * <p>This method suits downloading collections of entities for which an id is available.
     * The {@link Collection} of ids is provided as an input argument, along with a function
     * that returns an {@link Observable} for the matching entity.</p>
     *
     * <p>The Observables will wrapped behind an Optional by means of {{@link #wrapInOptional(Observable)}}
     * and then safely combined into a single observable with {@link Observable#combineLatest(ObservableSource[], Function)}
     * along with {@link #fromOptArray(Object[])}. The result is an Observable for a list of all base
     * entities which is emitted each time any of them is updated.</p>.
     *
     * @param ids the collection of the ids for the entities to group.
     * @param fetcher a function which returns an Observable for the requested entity by using its id.
     * @param <T> the type of the requested entities
     * @return An Observable List of requested items.
     */
    public static <T> Observable<List<T>> downloadByIds(Collection<String> ids, Function<String, Observable<T>> fetcher) {
        if (ids.isEmpty()) { return Observable.just(Collections.emptyList()); }

        List<Observable<Optional<T>>> optionals = new ArrayList<>(ids.size());
        for (String id : ids) {
            try { optionals.add(wrapInOptional(fetcher.apply(id))); }
            catch (Exception e) { Log.e(TAG, e.getMessage()); }
        }

        return Observable.combineLatest(optionals, FirebaseUtils::fromOptArray);
    }

    /**
     * <p>Helper method which works similarly to {@link #downloadByIds(Collection, Function)},
     * except the observed value is a map that ties each arbitrary Id to its corresponding element.</p>
     *
     * @param ids the collection of ids for the entities to download
     * @param fetcher a function which maps an id to its observable entity
     * @param <K> the key type
     * @param <R> the observed entity type
     * @return An observable Map for the requested items, indexed by id.
     */
    private static <K, R> Observable<Map<K, R>> mapByIds(Collection<K> ids, Function<K, Observable<R>> fetcher) {
        if (ids.isEmpty()) { return Observable.just(Collections.<K, R>emptyMap()); }

        List<Observable<Optional<Pair<K, R>>>> pairObservables = new ArrayList<>(ids.size());
        for (K id : ids) {
            try {
                Observable<R> baseObservable = fetcher.apply(id);
                Observable<Pair<K, R>> unreadEntryObservable = baseObservable
                        .map(list -> new Pair<>(id, list));
                pairObservables.add(wrapInOptional(unreadEntryObservable));
            }
            catch (Exception e) { Log.e(TAG, e.getMessage()); }
        }

        return Observable.combineLatest(pairObservables, FirebaseUtils::<Pair<K, R>>fromOptArray)
                .map(entries -> {
                    Map<K, R> map = new HashMap<>();
                    for (Pair<K, R> entry : entries) {
                        map.put(entry.first, entry.second);
                    }
                    return map;
                });
    }

    private static DatabaseReference getUsersRef() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DBKEY_USERS);
    }

    private static DatabaseReference getMembersRef() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DBKEY_MEMBERS);
    }

    private static DatabaseReference getMessagesRef() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DBKEY_MESSAGES);
    }

    private static DatabaseReference getLastReadRef() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DBKEY_LAST_READ);
    }
}
