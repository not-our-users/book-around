package it.polito.students.mad.group5.bookaround.models

import com.google.firebase.database.Exclude
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.R.mipmap.transaction
import java.io.Serializable

/**
 * Created by Alessio on 20/05/2018. Bestemmie by Java on 25/05
 */

data class LendingTransaction (
        var dateIssued: Long = System.currentTimeMillis(),
        var status: TransactionStatus = TransactionStatus.OPEN,
        var dateClosed: Long? = null,
        var expireDate: Long? = null,
        var borrowerApprovalFlag: Boolean = true,
        var lenderApprovalFlag: Boolean = false,
        var borrowerReviewId: String? = null,
        var lenderReviewId: String? = null
) : Serializable {
    constructor(
            transactionId: String,
            borrowerId: String,
            lenderId: String,
            copyId: String
    ) : this() {
        this.transactionId = transactionId
        this.borrowerId = borrowerId
        this.lenderId = lenderId
        this.copyId = copyId
    }

    lateinit var transactionId: String
    lateinit var borrowerId: String
    lateinit var lenderId: String
    lateinit var copyId: String

    @Exclude var borrower: User? = null
    @Exclude var lender: User? = null
    @Exclude var copy: Copy? = null

    @Exclude fun getStripped() : LendingTransaction{
        return run {
            LendingTransaction(dateIssued,
                    status,
                    dateClosed,
                    expireDate,
                    borrowerApprovalFlag,
                    lenderApprovalFlag,
                    borrowerReviewId,
                    lenderReviewId)
                    .let {
                        it.copyId=copyId
                        it.transactionId = transactionId
                        it.borrowerId = borrowerId
                        it.lenderId = lenderId
                        it
                    }
        }
    }
}

enum class TransactionStatus(val localizedId:Int) {
    OPEN(R.string.pending),
    APPROVED(R.string.approved),
    ACTIVE(R.string.active),
    CLOSED(R.string.closed),
    REJECTED(R.string.rejected)
}


data class Review (var vote: Int = 0, var text: String = "") : Serializable {
    constructor(
            reviewId: String,
            transactionId: String,
            authorId: String,
            subjectId: String,
            vote: Int,
            text: String,
            timestamp: Long
    ) : this(vote, text) {
        this.reviewId = reviewId
        this.transactionId = transactionId
        this.authorId = authorId
        this.subjectId = subjectId
        this.timestamp = timestamp
    }

    lateinit var reviewId: String
    lateinit var transactionId: String
    lateinit var authorId: String
    lateinit var subjectId: String
    private var timestamp: Long = 0

    @Exclude var transaction: LendingTransaction? = null
    @Exclude var author: User? = null
    @Exclude var subject: User? = null
}