package it.polito.students.mad.group5.bookaround.utils;

import java.util.NoSuchElementException;

import io.reactivex.annotations.Nullable;

/**
 * An implementation {@link java.util.Optional}, because we can't have pretty things before API 24.
 *
 * Created by Alessio on 11/05/2018.
 */
public class Optional<M> {

    private final M optional;

    public Optional(@Nullable M optional) {
        this.optional = optional;
    }

    public boolean isEmpty() {
        return this.optional == null;
    }

    public M get() {
        if (optional == null) {
            throw new NoSuchElementException("No value present");
        }
        return optional;
    }
}