package it.polito.students.mad.group5.bookaround.activities

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.robertlevonyan.views.customfloatingactionbutton.FloatingActionButton
import com.squareup.picasso.Picasso
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.models.Copy
import it.polito.students.mad.group5.bookaround.models.LendingTransaction
import it.polito.students.mad.group5.bookaround.models.TransactionStatus
import it.polito.students.mad.group5.bookaround.models.User
import it.polito.students.mad.group5.bookaround.utils.*
import java.text.DateFormat
import java.util.*

class ShowTransaction : AppCompatActivity() {

    private lateinit var userId : String
    private var isOwner = false

    private lateinit var transactionId: String
    private lateinit var transaction: LendingTransaction
    private lateinit var status: TextView

    companion object {
        val TRANSACTION_KEY: String = "TRANSACTION"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_loan)
        status = findViewById(R.id.loan_status)

        userId = FirebaseAuth.getInstance().uid ?: throw SecurityException("User must be authenticated!")

        transaction = intent?.getSerializableExtra(TRANSACTION_KEY) as? LendingTransaction
        ?: run { this@ShowTransaction.finish(); return@onCreate }
        setTransaction(transaction)
        transactionId = transaction.transactionId

        downloadTransaction(transactionId).toLiveData().observe(
                { this.lifecycle },
                {
                    it?.value?.let { setTransaction(it) }
                    it?.exception?.printStackTrace()
                }
        )

        setUpToolbar()
        title = getString(R.string.title_transaction)

        findViewById<FloatingActionButton>(R.id.onDeclineFAB).setOnClickListener(this@ShowTransaction::onDecline)
        findViewById<FloatingActionButton>(R.id.onAcceptFAB).setOnClickListener(this@ShowTransaction::onAccept)
        findViewById<FloatingActionButton>(R.id.confirm_handing_button).setOnClickListener(this@ShowTransaction::onHanded)
        findViewById<FloatingActionButton>(R.id.onReturnedFAB).setOnClickListener(this@ShowTransaction::onReturned)
        findViewById<FloatingActionButton>(R.id.leaveReviewFAB).setOnClickListener(this@ShowTransaction::startLeaveReview)
    }

    private fun setTransaction(transaction: LendingTransaction) {
        this.transaction = transaction
        transaction.copy?.let{ setCopy(it) }

        isOwner = transaction.lenderId.equals(userId)

        val otherUser = if (isOwner) transaction.borrower else transaction.lender
        otherUser?.let { setOtherUser(otherUser) }

        when (transaction.status) {
            TransactionStatus.OPEN -> setOpen()
            TransactionStatus.ACTIVE -> setActive()
            TransactionStatus.APPROVED -> setApproved(transaction)
            TransactionStatus.CLOSED -> setClosed()
            TransactionStatus.REJECTED -> setRejected()
        }

        val dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, resources.configuration.locale)
        val formattedIssuedDate = dateFormat.format(Date(transaction.dateIssued))
        val formattedEndDate = transaction.dateClosed?.let { dateFormat.format(it) }

        findViewById<TextView>(R.id.start_date).text = formattedIssuedDate
        formattedEndDate?.let { findViewById<TextView>(R.id.end_date).text = it }
    }

    private fun setCopy(copy: Copy) {
        val thumbImg = findViewById<ImageView>(R.id.copy_thumb)
        val pictureToLoad = copy.pictureList?.let {
            if (it.size > 0) it[0] else null
        }
        val loader = pictureToLoad?.let { Picasso.get().load(it) } ?: Picasso.get().load(R.mipmap.not_found)
        loader.placeholder(R.mipmap.not_found).fit().into(thumbImg)

        thumbImg?.setOnClickListener { v ->
            val intent = Intent(this, ShowCopyActivity::class.java)
            intent.putExtra(ShowCopyActivity.COPY_KEY,copy)
            startActivity(intent)
        }

        copy.bookData.toObservable().toLiveData().observe(
                { this.lifecycle },
                {
                    it?.value?.let { findViewById<TextView>(R.id.show_title).text = it.title }
                    it?.exception?.printStackTrace()
                }
        )
    }

    private fun setOtherUser(u: User) {
        findViewById<TextView>(R.id.show_name).text = u.name
        val userImage = findViewById<ImageView>(R.id.profile_pic)
        u.profilePicBitmap?.let{ userImage?.setImageBitmap(it) }
        userImage?.setOnClickListener({v->
            val intent = Intent(this, ShowProfileActivity::class.java)
            intent.putExtra(getString(R.string.show_user_key),u)
            startActivity(intent)
        })
    }

    private fun setOpen() {
        resetView()
        val tv = findViewById<TextView>(R.id.status_description)

        if (isOwner) {
            findViewById<View>(R.id.accept_or_decline_req).visibility = View.VISIBLE
            tv.setText(R.string.book_request)
        } else {
            tv.setText(R.string.book_request_wait)
        }
        tv.visibility = View.VISIBLE
        status.setText(R.string.pending)
        status.setTextColor(resources.getColor(R.color.statusWaiting))
    }

    private fun setApproved(transaction: LendingTransaction) {
        resetView()
        val handingView = findViewById<View>(R.id.confirm_handing)
        val handingButton = handingView.findViewById<FloatingActionButton>(R.id.confirm_handing_button)

        if (isOwner) {
            if (!transaction.lenderApprovalFlag) {
                handingView.visibility = View.VISIBLE
                handingButton.setText(R.string.confirm_book_handed)
                findViewById<TextView>(R.id.status_description).apply {
                    setText(context.getString(R.string.book_handed_owner))
                    visibility = View.VISIBLE
                }
            }
            else {
                findViewById<TextView>(R.id.status_description).apply {
                    setText(R.string.book_handed)
                    visibility = View.VISIBLE
                }
            }
        }
        else if (!isOwner) {
            if (!transaction.borrowerApprovalFlag) {
                handingView.visibility = View.VISIBLE
                handingButton.setText(R.string.confirm_book_received)
                findViewById<TextView>(R.id.status_description).apply {
                    setText(R.string.transaction_approved_go_get)
                    visibility = View.VISIBLE
                }
            }
            else {
                findViewById<TextView>(R.id.status_description).apply {
                    setText(R.string.waiting_book)
                    visibility = View.VISIBLE
                }
            }
        }

        status.setText(R.string.approved)
        status.setTextColor(resources.getColor(R.color.statusWaiting))
    }

    private fun setActive() {
        resetView()
        if (isOwner) {
            findViewById<View>(R.id.confirm_return).visibility = View.VISIBLE
        }
        findViewById<TextView>(R.id.status_description).apply {
            setText(
                    if (isOwner) R.string.book_handed_confirm_return
                    else R.string.book_handed_wait_return
            )
            visibility = View.VISIBLE
        }

        status.setText(R.string.active)
        status.setTextColor(resources.getColor(R.color.statusPositive))
    }

    private fun setClosed() {
        resetView()
        status.setText(R.string.closed)
        status.setTextColor(resources.getColor(R.color.darkNeutral))

        if ((isOwner && transaction.lenderReviewId == null) ||
            (!isOwner && transaction.borrowerReviewId == null)
        ) {
            findViewById<TextView>(R.id.status_description).apply {
                setText(R.string.review_suggestion)
                visibility = View.VISIBLE
            }
            findViewById<View>(R.id.leave_review).visibility = View.VISIBLE
        }
    }

    private fun setRejected() {
        resetView()
        status.setText(R.string.rejected)
        status.setTextColor(resources.getColor(R.color.statusNegative))
    }

    fun onAccept(v: View) {
        if (transaction.copy?.status != Copy.Status.NOT_BORROWED) {
            AlertDialog.Builder(this)
                    .setMessage(R.string.cant_approve_copy_borrowed)
                    .setPositiveButton(android.R.string.ok, { _, _ -> })
                    .create()
                    .show()
            return
        }

        AlertDialog.Builder(this)
                .setTitle(getString(R.string.confirm_acceptation_title))
                .setMessage(getString(R.string.confirm_acceptation))
                .setPositiveButton(R.string.yes,{ _, _ ->
                    lenderApproveOrDenyTransaction(transactionId, true)
                })
                .setNegativeButton(R.string.no,null)
                .show()
    }

    fun onDecline(v: View) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.confirm_decline_title))
                .setMessage(getString(R.string.confirm_decline))
                .setPositiveButton(R.string.yes,{ _, _ ->
                    lenderApproveOrDenyTransaction(transactionId, false)
                })
                .setNegativeButton(R.string.no,null)
                .show()
    }

    fun onReturned(v: View) {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.confirm_restitution_title))
                .setMessage(getString(R.string.confirm_restitution))
                .setPositiveButton(R.string.yes,{ _, _ ->
                    confirmBookRestitution(transactionId)
                })
                .setNegativeButton(R.string.no,null)
                .show()


    }

    fun onHanded(v: View) {
        AlertDialog.Builder(this).apply {
            if(FirebaseAuth.getInstance().uid==transaction.lenderId){
                setTitle(getString(R.string.confirm_handed_title))
                setMessage(getString(R.string.confirm_handed))
            } else{
                setTitle(getString(R.string.confirm_receiving_title))
                setMessage(getString(R.string.confirm_receiving))
                }
        }
            .setPositiveButton(R.string.yes,{ _, _ ->
                confirmBookExchange(transactionId, System.currentTimeMillis())
            })
            .setNegativeButton(R.string.no,null)
            .show()
    }

    fun startLeaveReview(v: View) {
        val startReviewIntent = Intent(this, LeaveReviewActivity::class.java)
        startReviewIntent.putExtra(LeaveReviewActivity.TRANSACTION_KEY, transaction.getStripped())
        startActivity(startReviewIntent)
    }

    private fun resetView() {
        findViewById<View>(R.id.status_description).visibility = View.GONE
        findViewById<View>(R.id.accept_or_decline_req).visibility = View.GONE
        findViewById<View>(R.id.confirm_return).visibility = View.GONE
        findViewById<View>(R.id.confirm_handing).visibility = View.GONE
        findViewById<View>(R.id.leave_review).visibility = View.GONE
    }

    private fun setUpToolbar() {
        val toolbar = findViewById<android.support.v7.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.apply {
            setNavigationIcon(R.drawable.arrow_left)
            navigationIcon!!.setColorFilter(resources.getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP)
            setNavigationOnClickListener { v -> finish() }
        }
    }

}
