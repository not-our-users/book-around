package it.polito.students.mad.group5.bookaround.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.List;
import java.util.Objects;

import at.blogc.android.views.ExpandableTextView;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer;
import it.polito.students.mad.group5.bookaround.fragments.FragmentNavDrawer;
import it.polito.students.mad.group5.bookaround.fragments.FragmentSendMessageDialog;
import it.polito.students.mad.group5.bookaround.models.Chat;
import it.polito.students.mad.group5.bookaround.models.Review;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.utils.ExceptionWrapper;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtilsKt;
import it.polito.students.mad.group5.bookaround.utils.ReactiveExtensionsKt;

import static it.polito.students.mad.group5.bookaround.utils.FirebaseUtils.downloadUser;

public class ShowProfileActivity    extends AppCompatActivity
                                    implements FragmentSendMessageDialog.SendMessageDialogListener{

    private static final String TAG = "ShowProfileActivity";
    private static final int REQUEST_EDIT_PROFILE = 1;
    private static final int LAST_REVIEW_LIMIT = 1;
    private DrawerLayout drawer;
    private boolean isFiltersView = false;
    private User user = new User();

    private ValueEventListener mConnectedListener;
    private DatabaseReference userRef;

    private Disposable chatDisposable = null;
    private String existingChatId = null;

    private FragmentFilterDrawer mFilterFragment;
    private ShowReviewsActivity.ReviewListViewHolder lastReviewHolder;

    /**
     * Load data from the calling Intent. If no {@link User} was provided, fail and
     * generate an error.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_profile);
        //View menu= getLayoutInflater().inflate(R.layout.drawer_menu, null);

        mFilterFragment = (FragmentFilterDrawer)getSupportFragmentManager().findFragmentById(R.id.filter_drwr_fragment);

        Bundle callingData = getIntent().getExtras();
        if (callingData != null && callingData.containsKey(getString(R.string.show_user_key))) { //Show the main user profile
            user = (User) callingData.getSerializable(getString(R.string.show_user_key));

            if (user == null) {
                Log.d(TAG, "User data not provided");
                finish();
                return;
            }

            ReactiveExtensionsKt.toLiveData(downloadUser(user.getUserId()), BackpressureStrategy.LATEST)
                    .observe(this, u -> {
                        if (u != null && u.getValue() != null) { user = u.getValue(); updateFields(); }
                        else if (u != null && u.getException() != null) { u.getException().printStackTrace(); }
                    });

            ReactiveExtensionsKt.toLiveData(
                    FirebaseUtilsKt.downloadUserReviews(user.getUserId(), LAST_REVIEW_LIMIT),
                    BackpressureStrategy.LATEST
            )
            .observe(this, this::setLastReviews);

            setContactView();
            setUpDrawer();
            setUpToolbar();
            lastReviewHolder = new ShowReviewsActivity.ReviewListViewHolder(findViewById(R.id.last_review));
            FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
            if(user.getUserId().equals(FirebaseAuth.getInstance().getUid())){
                setTitle(getString(R.string.title_activity_show_profile));
                floatingActionButton.setImageResource(R.mipmap.edit);
                floatingActionButton.setOnClickListener(this::StartEditProfile);
            }
            else{
                setTitle(user.getName());
                startListeningForDialogs();
                floatingActionButton.setImageResource(R.mipmap.message_plus);
                floatingActionButton.setOnClickListener(this::SendMessage);
            }
            findViewById(R.id.show_bio_view).setOnClickListener(v -> ((ExpandableTextView)v).toggle());
        }
        else {
            Toast.makeText(getApplicationContext(), R.string.error_no_user_supplied, Toast.LENGTH_LONG).show();
            finish();
        }
        updateFields();
    }

    private void setLastReviews(ExceptionWrapper<List<Review>> reviews) {
        if (reviews.getValue() != null) {
            findViewById(R.id.review_list_card).setVisibility(reviews.getValue().isEmpty() ? View.GONE : View.VISIBLE);
            if (!reviews.getValue().isEmpty()) { lastReviewHolder.setReview(reviews.getValue().get(0)); }
        }
        if (reviews.getException() != null) {
            reviews.getException().printStackTrace();
        }
    }


    //START Drawer Fragment Classes
    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        //toolbar.inflateMenu();
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

    }
    private void setUpDrawer(){
        FragmentNavDrawer drawerFragment= (FragmentNavDrawer) getSupportFragmentManager().findFragmentById(R.id.nav_drwr_fragment);
        drawer = findViewById(R.id.drawer_layout);
    }
    //END Drawer Fragment Classes

    @Override
    protected void onPause() {
        super.onPause();
        if (chatDisposable != null) {
            chatDisposable.dispose();
            chatDisposable = null;
        }
    }

    private void startListeningForDialogs() {
        if (chatDisposable != null) {
            chatDisposable.dispose();
            chatDisposable = null;
        }
        chatDisposable =  FirebaseUtils.downloadUserChats()
                .doOnNext(chats -> {
                    // Filter out all empty chats
                    for (Chat chat : chats) {
                        for(IUser iUser : chat.getUsers()){
                            if(user.getId().equals(iUser.getId())){
                                existingChatId =chat.getId();
                                chatDisposable.dispose();
                                chatDisposable = null;
                                return;
                            }
                        }
                    }
                })
                .doOnError(this::onError)
                .subscribe();
    }

    private void onError(Throwable t) {
        Toast.makeText(getApplicationContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        Log.e("ShowCopyActivity", t.getMessage());
        finish();
    }
    /********************************************************************/


    @Override
    protected void onStart(){
        super.onStart();
        userRef = FirebaseDatabase.getInstance()
                .getReference(getString(R.string.DBKEY_USERS))
                .child(user.getUserId());
        mConnectedListener = userRef.addValueEventListener(new ValueEventListener() {
            @Override
            //Updating the view every time the data in the remote db are updated
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("callback", "onDataChange");
                user = dataSnapshot.getValue(User.class);
                updateFields();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Something went wrong with the remote database",
                        Toast.LENGTH_LONG).show();
            }

        });

    }

    @Override
    public void onStop() {
        super.onStop();
        userRef.removeEventListener(mConnectedListener);
    }

    private void setContactView() {
        LinearLayout ll=findViewById(R.id.show_contacts);
        if (ll == null) {
            Log.e(TAG, "Show contacts has disappeared! Call the GIT POLICE!");
            return;
        }
        ll.removeAllViews();

        // The contacts View is populated by using data from the user contacts
        String[] types = {"Twitter", "Facebook", "Gplus", "Home", "Office", "Mobile", "Telegram"};
        for(String type:types) {
            String value = user.getContacts().get(type);
            if(value != null && !value.equals("")) {
                View vi = getLayoutInflater().inflate(R.layout.show_contacts, ll, false);
                TextView typeView = vi.findViewById(R.id.contact_label);
                typeView.setText(type);
                TextView valueView = vi.findViewById(R.id.show_contact);
                valueView.setText(value);
                ll.addView(vi);
            }
        }
    }

    /**
     * Callback function for the edit button. It will launch {@link EditProfileActivity}
     * and signal the 'leaving for edit' flag. This will prevent {@link #onSaveInstanceState(Bundle)}
     * from saving the current user and later override the result.
     *
     * @param src The source View which triggered this callback
     */
    public void StartEditProfile(View src) {
        Intent EditProfileIntent = new Intent(getApplicationContext(), EditProfileActivity.class);
        EditProfileIntent.putExtra(getString(R.string.edit_user_key), user);
        startActivityForResult(EditProfileIntent, REQUEST_EDIT_PROFILE);
    }

    /**
     * Callback function for the send message button. It implements the {@link FragmentSendMessageDialog}
     * @param src The source View which triggered this callback
     */
    public void SendMessage(View src) {
        if(existingChatId != null){
            Intent startChatIntent = new Intent(this, ChatActivity.class);
            startChatIntent.putExtra(ChatActivity.CHAT_ID_KEY, existingChatId);
            startActivity(startChatIntent);
        }
        else{
            if (chatDisposable != null) {
                chatDisposable.dispose();
                chatDisposable = null;
            }
            FragmentSendMessageDialog newFragment = FragmentSendMessageDialog.newInstance(R.string.title_chat, user.getUserId());
            newFragment.show(getSupportFragmentManager(),"FragmentSendMessageDialog");
        }
    }

    /**
     * Override for the interface FragmentSendMessageDialog.SendMessageDialogListener
     */
    @Override
    public void onCancelClicked() {
        startListeningForDialogs();
    }

    /**
     * Override for the interface FragmentSendMessageDialog.SendMessageDialogListener
     */
    @Override
    public void onSendClicked(String newChatId) {
        Intent startChatIntent = new Intent(getApplicationContext(), ChatActivity.class);
        startChatIntent.putExtra(ChatActivity.CHAT_ID_KEY, newChatId);
        startActivity(startChatIntent);
    }

    /**
     * Updates the value fields with the current user data
     */
    public void updateFields() {
        if (user == null) { return; }

        ImageView picView = findViewById(R.id.profile_pic);
        Bitmap userPic = user.getProfilePicBitmap();
        if (userPic != null) {
            picView.setImageBitmap(userPic);
        }
        picView.setOnClickListener(view -> {
            //set the image in dialog popup
            //below code fullfil the requirement of xml layout file for dialog popup
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            LinearLayout layout = new LinearLayout(getApplicationContext());
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER);

            LinearLayout.LayoutParams imageViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            float density = getResources().getDisplayMetrics().density;
            imageViewParams.setMargins(
                    (int) (density * 80),
                    (int) (density * 80),
                    (int) (density * 80),
                    (int) (density * 80)
            );

            ImageView iv = new ImageView(getApplicationContext());
            iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            iv.setAdjustViewBounds(true);
            layout.addView(iv, imageViewParams);

            Dialog builder = new Dialog(ShowProfileActivity.this);
            builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
            Objects.requireNonNull(builder.getWindow()).setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            builder.setOnDismissListener(dialogInterface -> {
                //nothing;
            });
            builder.addContentView(layout, layoutParams);
            layout.setOnClickListener(v -> builder.dismiss());
            builder.show();
            builder.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            iv.setImageBitmap(user.getProfilePicBitmap());
        });

        TextView nameView = findViewById(R.id.show_name_view);
        TextView emailView = findViewById(R.id.show_email_view);
        TextView bioView = findViewById(R.id.show_bio_view);
        TextView townView = findViewById(R.id.show_town_view);
        RatingBar ratingBar = findViewById(R.id.show_reputation);

        nameView.setText(user.getName());
        emailView.setText(user.getEmail());
        bioView.setText(user.getBio());
        townView.setText(user.getTown());
        ratingBar.setRating(user.getStars());

        setContactView();
    }

    //START SEARCH CLASSES
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_search, menu);
            MenuItem searchItem = menu.findItem(R.id.mi_search);
            android.support.v7.widget.SearchView sv=(android.support.v7.widget.SearchView) searchItem.getActionView();
            sv.setIconifiedByDefault(false);
            View fv=findViewById(R.id.filters_frame);
            sv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                final MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                filtersMenuItem.setVisible(false);
                //editMenuItem.setVisible(true);
                if (isFiltersView) {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;

                }
                findViewById(R.id.floatingActionButton).setVisibility(View.VISIBLE);

            }


            @Override
            public void onViewAttachedToWindow(View arg0) {
                filtersMenuItem.setVisible(true);
                //editMenuItem.setVisible(false);
                findViewById(R.id.floatingActionButton).setVisibility(View.INVISIBLE);

            }
        });

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (isFiltersView) {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;
                }
                mFilterFragment.StartSearchActivity(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        Button advS = findViewById(R.id.searchButton);
        advS.setOnClickListener(view -> {
            String s = sv.getQuery().toString();
            mFilterFragment.StartSearchActivity(s);
            TranslateAnimation animate = new TranslateAnimation(
                    0,
                    0,
                    0,
                    -(fv.getHeight()));
            animate.setDuration(500);
            //animate.setFillAfter(false);
            fv.startAnimation(animate);
            fv.setVisibility(View.INVISIBLE);
            isFiltersView = false;
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.btn_advanced_search:
                View fv = findViewById(R.id.filters_frame);
                if (!isFiltersView) {
                    fv.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -(fv.getHeight()),
                            0);
                    animate.setDuration(500);
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate);
                    isFiltersView = true;
                } else {
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView = false;

                }
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //END SEARCH CLASSES

    public void OpenReviewView(View v){
        Intent ReviewsIntent = new Intent(getApplicationContext(), ShowReviewsActivity.class);
        ReviewsIntent.putExtra(ShowReviewsActivity.USERID_KEY, user.getUserId());
        startActivity(ReviewsIntent);
    }

}
