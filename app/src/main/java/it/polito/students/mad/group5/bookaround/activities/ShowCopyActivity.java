package it.polito.students.mad.group5.bookaround.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.models.IUser;

import java.util.List;
import java.util.Objects;

import at.blogc.android.views.ExpandableTextView;
import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;
import io.reactivex.disposables.Disposable;
import it.polito.students.mad.group5.bookaround.R;
import it.polito.students.mad.group5.bookaround.fragments.FragmentFilterDrawer;
import it.polito.students.mad.group5.bookaround.fragments.FragmentSendMessageDialog;
import it.polito.students.mad.group5.bookaround.models.Chat;
import it.polito.students.mad.group5.bookaround.models.Copy;
import it.polito.students.mad.group5.bookaround.models.LendingTransaction;
import it.polito.students.mad.group5.bookaround.models.TransactionStatus;
import it.polito.students.mad.group5.bookaround.models.User;
import it.polito.students.mad.group5.bookaround.utils.CreateTransactionResult;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtils;
import it.polito.students.mad.group5.bookaround.utils.FirebaseUtilsKt;
import it.polito.students.mad.group5.bookaround.utils.TransactionUtilsKt;

public class ShowCopyActivity   extends AppCompatActivity
                                implements FragmentSendMessageDialog.SendMessageDialogListener {
    private Copy copy;
    private static final int REQUEST_EDIT_COPY = 1;
    public static final String COPY_KEY = "copy";
    private boolean isFiltersView=false;
    private FragmentFilterDrawer mFilterFragment;
    private boolean leavingForEdit = false;
    private ValueEventListener mConnectedListener;
    private DatabaseReference copyRef;

    private Disposable chatDisposable = null;
    private Disposable bookDisposable = null;
    private String copyOwnerChatId = null;
    private Disposable transactionDisposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_copy);

        //setUpDrawer();
        setUpToolbar();
        setTitle(getString(R.string.title_activity_show_copy));

        findViewById(R.id.show_details).setOnClickListener(view -> ((ExpandableTextView)view).toggle());

        mFilterFragment = (FragmentFilterDrawer) getSupportFragmentManager().findFragmentById(R.id.filter_drwr_fragment);

        //Documentation: https://github.com/yavski/fab-speed-dial
        FabSpeedDial fabSpeedDial = findViewById(R.id.fab_speed_dial);
        fabSpeedDial.setMenuListener(new SimpleMenuListenerAdapter(){
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                String uid = FirebaseAuth.getInstance().getUid();
                if(uid != null && copy.getOwnerId().equals(uid)) {
                    @SuppressLint("RestrictedApi") MenuItem item = navigationMenu.findItem(R.id.mi_label_contact_user);
                    item.setVisible(false);
                    // The delete copy and the edit copy items are shown only if the current user is the owner of the copy
                    @SuppressLint("RestrictedApi") MenuItem deleteItem = navigationMenu.findItem(R.id.mi_delete_copy);
                    deleteItem.setVisible(true);
                    @SuppressLint("RestrictedApi") MenuItem editItem = navigationMenu.findItem(R.id.mi_edit_copy);
                    editItem.setVisible(true);
                    @SuppressLint("RestrictedApi") MenuItem askCopy = navigationMenu.findItem(R.id.mi_start_transaction);
                    askCopy.setVisible(false);
                }
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem){
                return onOptionsItemSelected(menuItem);
            }
        });
        ViewCompat.setTranslationZ(findViewById(R.id.fab_speed_frame), 10.0f);


        Intent callee = getIntent();
        if (callee != null) {
            copy = (Copy)callee.getSerializableExtra(COPY_KEY);
            updateFields();
        }
        else { finish(); }
    }

    /*************************************************************
     * This part of code is used to check if the current user has already a chat with the copy's owner user.
     */
    @Override
    protected void onStart() {
        super.onStart();
        copyRef = FirebaseDatabase.getInstance()
                .getReference(getString(R.string.DBKEY_COPIES))
                .child(copy.getCopyId());
        mConnectedListener = copyRef.addValueEventListener(new ValueEventListener() {
            @Override
            //Updating the view every time the data in the remote db are updated
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("callback", "onDataChange");
                copy = dataSnapshot.getValue(Copy.class);
                if(copy != null)
                    updateFields();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "Something went wrong with the remote database",
                        Toast.LENGTH_LONG).show();
            }

        });
        startListeningForDialogs();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (chatDisposable != null) {
            chatDisposable.dispose();
            chatDisposable = null;
        }
        if (bookDisposable != null) {
            bookDisposable.dispose();
            bookDisposable = null;
        }
    }

    private void startListeningForDialogs() {
        if (chatDisposable != null) {
            chatDisposable.dispose();
            chatDisposable = null;
        }
        chatDisposable =  FirebaseUtils.downloadUserChats()
                .doOnNext(chats -> {
                    // Filter out all empty chats
                    for (Chat chat : chats) {
                        for(IUser user : chat.getUsers()){
                            //For every chat of this user, check if the copy owner has a chat with the current user
                            if(copy.getOwnerId().equals(user.getId())){
                                copyOwnerChatId=chat.getId();
                                chatDisposable.dispose();
                                return;
                            }
                        }
                    }
                })
                .doOnError(this::onError)
                .subscribe();
    }

    private void onError(Throwable t) {
        Toast.makeText(getApplicationContext(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
        Log.e("ShowCopyActivity", t.getMessage());
        finish();
    }
    /********************************************************************/
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Copy previousCopy = (Copy)savedInstanceState.getSerializable("copy");
        if (previousCopy != null) { copy = previousCopy; updateFields(); }
    }


    private void ShowBookPage() {
        Intent bookPageIntent = new Intent(getApplicationContext(), BookDetailsActivity.class);
        bookPageIntent.putExtra("googleApiId", copy.getGoogleApiId());
        startActivity(bookPageIntent);
    }

    private void DeleteCopy() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.confirm_delete_title)
                .setMessage(R.string.confirm_delete_text)
                .setIcon(R.drawable.ic_delete_black)
                .setPositiveButton(R.string.yes, (dialog, button) -> {
                    copyRef.removeValue();
                    finish();
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void ShowProfilePage(){
        DatabaseReference ref = FirebaseDatabase.getInstance()
                                                .getReference(getString(R.string.DBKEY_USERS))
                                                .child(copy.getOwnerId());
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Intent userProfileIntent = new Intent(getApplicationContext(),ShowProfileActivity.class);
                userProfileIntent.putExtra(getString(R.string.show_user_key),user);
                startActivity(userProfileIntent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void StartEditCopy() {
        Intent EditCopyIntent = new Intent(getApplicationContext(), EditCopyActivity.class);
        EditCopyIntent.putExtra(COPY_KEY, copy);

        leavingForEdit = true;
        startActivityForResult(EditCopyIntent, REQUEST_EDIT_COPY);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (leavingForEdit) { leavingForEdit = false; }
        else { outState.putSerializable(COPY_KEY, copy); }
    }

    private void updateFields() {
        if (bookDisposable != null) {
            bookDisposable.dispose();
            bookDisposable = null;
        }

        TextView availabilityView = findViewById(R.id.availability_show);
        TextView isbnView = findViewById(R.id.show_isbn);
        TextView editionView = findViewById(R.id.show_edition);
        TextView townView = findViewById(R.id.show_town_view);
        TextView detailsView = findViewById(R.id.show_details);
        TextView conditionView = findViewById(R.id.show_condition);
        TextView titleView = findViewById(R.id.show_title);
        TextView authorView = findViewById(R.id.show_author);

        availabilityView.setText(copy.getStatus().localizedId);
        availabilityView.setTextColor(ContextCompat.getColor(this, copy.getStatus().colorId));
        isbnView.setText(copy.getIsbn());
        townView.setText(copy.getTown());
        detailsView.setText(copy.getDetails());
        conditionView.setText(copy.getCondition().getLocalizedVersion(this));

        bookDisposable = copy.getBookData().subscribe(book -> {
                editionView.setText(book.getEditionYear());
                titleView.setText(book.getTitle());
                if (book.getAuthors() != null && book.getAuthors().size() > 0) { authorView.setText(book.getAuthors().get(0)); }
            },
            error -> {
                Toast.makeText(getApplicationContext(), R.string.error_network_old_data, Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        );
        List<String> copyPictureUrls = copy.getPictureList();
        if (copyPictureUrls != null) {
            LinearLayout ll = findViewById(R.id.pictures_gallery);
            ll.removeAllViews();
            // The pictures gallery is populated
            for(String copyPictureUrl:copyPictureUrls) {
                ImageView imageView = new ImageView(getApplicationContext());
                Picasso.get().load(copyPictureUrl).placeholder(R.mipmap.not_found).into(imageView);
                LinearLayout.LayoutParams imageViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                float density = getResources().getDisplayMetrics().density;
                imageViewParams.setMargins(
                        (int) (density * 8),
                        (int) (density * 8),
                        (int) (density * 8),
                        (int) (density * 8)
                );
                imageView.setAdjustViewBounds(true);
                ll.addView(imageView,imageViewParams);
                imageView.setOnClickListener(view -> {
                    //set the image in dialog popup
                    //below code fullfil the requirement of xml layout file for dialog popup
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                    LinearLayout layout = new LinearLayout(getApplicationContext());
                    layout.setOrientation(LinearLayout.VERTICAL);
                    layout.setGravity(Gravity.CENTER);

                    imageViewParams.setMargins(
                            (int) (density * 80),
                            (int) (density * 80),
                            (int) (density * 80),
                            (int) (density * 80)
                    );

                    ImageView iv = new ImageView(getApplicationContext());
                    iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    iv.setAdjustViewBounds(true);
                    layout.addView(iv, imageViewParams);

                    Dialog builder = new Dialog(ShowCopyActivity.this);
                    builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    Objects.requireNonNull(builder.getWindow()).setBackgroundDrawable(
                            new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    builder.setOnDismissListener(dialogInterface -> {
                        //nothing;
                    });
                    builder.addContentView(layout, layoutParams);
                    layout.setOnClickListener(v -> builder.dismiss());
                    builder.show();
                    builder.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    Picasso.get().load(copyPictureUrl).fit().centerInside().into(iv);
                });
            }
        }
    }

    private void setUpToolbar(){
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.arrow_left);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.colorIcons), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        getMenuInflater().inflate(R.menu.menu_show_copy, menu);

        String uid = FirebaseAuth.getInstance().getUid();
        if(uid != null && copy.getOwnerId().equals(uid)) {
            MenuItem item = menu.findItem(R.id.mi_label_contact_user);
            item.setVisible(false);
        }

        MenuItem searchItem = menu.findItem(R.id.mi_search);
        android.support.v7.widget.SearchView sv=(android.support.v7.widget.SearchView) searchItem.getActionView();
        sv.setIconifiedByDefault(false);
        View fv=findViewById(R.id.filters_frame);
        sv.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            final MenuItem filtersMenuItem = menu.findItem(R.id.btn_advanced_search);
            @Override
            public void onViewDetachedFromWindow(View arg0) {
                filtersMenuItem.setVisible(false);
                if(isFiltersView){
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
            }


            @Override
            public void onViewAttachedToWindow(View arg0) {
                filtersMenuItem.setVisible(true);
            }
        });

        sv.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(isFiltersView){
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
                mFilterFragment.StartSearchActivity(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        Button advS = findViewById(R.id.searchButton);
        advS.setOnClickListener(view -> {
            String s=sv.getQuery().toString();
            mFilterFragment.StartSearchActivity(s);
            TranslateAnimation animate = new TranslateAnimation(
                    0,
                    0,
                    0,
                    -(fv.getHeight()));
            animate.setDuration(500);
            //animate.setFillAfter(false);
            fv.startAnimation(animate);
            fv.setVisibility(View.INVISIBLE);
            isFiltersView=false;
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.btn_advanced_search:
                View fv=findViewById(R.id.filters_frame);
                if(!isFiltersView){
                    fv.setVisibility(View.VISIBLE);
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            -(fv.getHeight()),
                            0);
                    animate.setDuration(500);
                    //animate.setFillAfter(true);
                    fv.startAnimation(animate);
                    isFiltersView=true;
                }
                else{
                    TranslateAnimation animate = new TranslateAnimation(
                            0,
                            0,
                            0,
                            -(fv.getHeight()));
                    animate.setDuration(500);
                    //animate.setFillAfter(false);
                    fv.startAnimation(animate);
                    fv.setVisibility(View.INVISIBLE);
                    isFiltersView=false;
                }
                break;
            case R.id.mi_book_details:
                ShowBookPage();
                break;
            case R.id.mi_delete_copy:
                DeleteCopy();
                break;
            case R.id.mi_edit_copy:
                StartEditCopy();
                break;
            case R.id.mi_label_show_profile:
                ShowProfilePage();
                break;
            case R.id.mi_label_contact_user:
                if(copyOwnerChatId!= null){
                    Intent startChatIntent = new Intent(this, ChatActivity.class);
                    startChatIntent.putExtra(ChatActivity.CHAT_ID_KEY, copyOwnerChatId);
                    startActivity(startChatIntent);
                }
                else{
                    chatDisposable.dispose();
                    FragmentSendMessageDialog newFragment = FragmentSendMessageDialog.newInstance(R.string.title_chat, copy.getOwnerId());
                    newFragment.show(getSupportFragmentManager(),"FragmentSendMessageDialog");
                }
                break;

            case R.id.mi_start_transaction:
                String uid = FirebaseAuth.getInstance().getUid();
                if (uid == null) { throw new SecurityException("User is not authenticated!"); }

                // To avoid impatient people
                if (transactionDisposable != null) { transactionDisposable.dispose(); }
                transactionDisposable = FirebaseUtilsKt.downloadUserTransactions(uid,false).subscribe(
                        transactionList -> {
                            if (transactionDisposable != null) {
                                transactionDisposable.dispose();
                                transactionDisposable = null;
                            }

                            LendingTransaction transaction = null;
                            for(LendingTransaction lt : transactionList){
                                if (    lt.getStatus()!= TransactionStatus.CLOSED &&
                                        lt.getStatus()!=TransactionStatus.REJECTED &&
                                        lt.lenderId.equals(copy.getOwnerId()) &&
                                        lt.copyId.equals(copy.getCopyId())
                                ){
                                    transaction = lt;
                                    break;
                                }
                            }

                            if (transaction != null) {
                                Intent startShowLoanActivity = new Intent(this, ShowTransaction.class);
                                startShowLoanActivity.putExtra(ShowTransaction.Companion.getTRANSACTION_KEY(), transaction.getStripped());
                                startActivity(startShowLoanActivity);
                            }
                            else if (!copy.getStatus().equals(Copy.Status.NOT_BORROWED)) {
                                Toast.makeText(getApplicationContext(), R.string.copy_not_available, Toast.LENGTH_LONG).show();
                            }
                            else {
                                CreateTransactionResult result = TransactionUtilsKt.createTransaction(copy.getOwnerId(),copy.getCopyId());
                                Objects.requireNonNull(result.getTask()).addOnCompleteListener(task -> {
                                    Intent startShowLoanActivity = new Intent(this, ShowTransaction.class);
                                    startShowLoanActivity.putExtra(ShowTransaction.Companion.getTRANSACTION_KEY(),result.getTransaction());
                                    startActivity(startShowLoanActivity);
                                });
                            }
                        },
                        Throwable::printStackTrace
                );

                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //END SEARCH CLASSES

    @Override
    public void onCancelClicked() {
        startListeningForDialogs();
    }

    @Override
    public void onSendClicked(String newChatId) {
        Intent startChatIntent = new Intent(getApplicationContext(), ChatActivity.class);
        startChatIntent.putExtra(ChatActivity.CHAT_ID_KEY, newChatId);
        startActivity(startChatIntent);
    }
}
