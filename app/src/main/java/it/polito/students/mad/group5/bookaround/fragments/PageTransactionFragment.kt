package it.polito.students.mad.group5.bookaround.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import it.polito.students.mad.group5.bookaround.R
import it.polito.students.mad.group5.bookaround.activities.ShowTransaction
import it.polito.students.mad.group5.bookaround.models.LendingTransaction
import it.polito.students.mad.group5.bookaround.utils.OpenClosedTransactionMap
import it.polito.students.mad.group5.bookaround.utils.downloadOpenClosedTransactions
import it.polito.students.mad.group5.bookaround.utils.toLiveData
import java.util.*

private const val ARG_PAGE = "ARG_PAGE"

/**
 * A simple [Fragment] subclass.
 * Use the [PageTransactionFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PageTransactionFragment : Fragment() {

    private var mPage: Int = 0
    private var transactionObservable: Observable<OpenClosedTransactionMap>? = null

    companion object {
        @JvmStatic
        fun newInstance(page: Int, transactionObservable: Observable<OpenClosedTransactionMap>) =
                PageTransactionFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_PAGE,page)
                    }
                    this.transactionObservable = transactionObservable
                    retainInstance=true
                }
    }

    inner class TransactionViewHolder(val view:View):RecyclerView.ViewHolder(view){
        private lateinit var transaction: LendingTransaction

        private val photoView = view.findViewById<ImageView>(R.id.transaction_item_thumb_photo)
        private val statusText = view.findViewById<TextView>(R.id.transaction_item_status)
        private val descText = view.findViewById<TextView>(R.id.transaction_item_desc)

        fun setTransaction(transaction: LendingTransaction){
            this.transaction = transaction
            val loader = transaction.copy?.pictureList?.let {
                if ( it.size > 0 ) {
                Picasso.get().load(it[0])
                } else null
            } ?: Picasso.get().load(R.mipmap.not_found)
            loader.centerCrop().fit().into(photoView)

            statusText.text=getString(transaction.status.localizedId)
            transaction.copy?.bookData?.subscribeBy(
                    onSuccess = {descText.text = it?.title},
                    onError = Throwable::printStackTrace
            )
        }

        fun getTransaction(): LendingTransaction {
            return transaction
        }
    }


    inner class TransactionListAdapter:RecyclerView.Adapter<TransactionViewHolder>(){
        val items = LinkedHashMap<String,LendingTransaction>()
        val itemList = ArrayList<LendingTransaction>()

        val VIEW_TYPE_OWNER = 1
        val VIEW_TYPE_BORROWER = 0

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
            val view = layoutInflater.inflate(
                    if (viewType == VIEW_TYPE_OWNER) R.layout.transaction_borrower_list_item
                            else R.layout.transaction_owner_list_item,
                    parent,
                    false)
            val viewHolder = TransactionViewHolder(view)

            view.setOnClickListener {
                val intent = Intent(activity,ShowTransaction::class.java)
                intent.putExtra(ShowTransaction.TRANSACTION_KEY,viewHolder.getTransaction().getStripped())
                startActivity(intent)
            }

            return viewHolder
        }

        override fun getItemViewType(position: Int): Int {
            return if (itemList[position].borrowerId == FirebaseAuth.getInstance().uid)
                VIEW_TYPE_BORROWER else VIEW_TYPE_OWNER
        }

        override fun getItemCount(): Int {
            return itemList.size
        }

        override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
            holder.setTransaction(itemList[position])
        }

        fun notifyListUpdated(newList: List<LendingTransaction>){
            if (newList.isEmpty()) {
                itemList.clear()
                items.clear()
                notifyDataSetChanged()
                return
            }
            newList.reversed().forEachIndexed { i, trans ->
                while (i < itemList.size && trans.transactionId != itemList[i].transactionId) {
                    // Item removed
                    items.remove(itemList[i].transactionId)
                    itemList.removeAt(i)
                    notifyItemRemoved(i)
                }

                if (i < itemList.size) {
                    // Item updated
                    itemList[i] = trans
                    items[trans.transactionId] = trans
                    notifyItemChanged(i)
                } else {
                    // Item inserted
                    itemList.add(trans)
                    items[trans.transactionId] = trans
                    notifyItemInserted(i)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.apply {
            mPage= this!!.getInt(ARG_PAGE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return RecyclerView(activity).apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            itemAnimator = DefaultItemAnimator()
            val adapter = TransactionListAdapter().apply {adapter = this}

            (transactionObservable ?: downloadOpenClosedTransactions(FirebaseAuth.getInstance().uid!!))
            .toLiveData().observe(
                    { lifecycle },
                    {
                        it?.value?.let {
                            adapter.notifyListUpdated(it[mPage == 0] ?: Collections.emptyList() )
                        }
                        it?.exception?.printStackTrace()
                    }
            )
        }

    }
}
